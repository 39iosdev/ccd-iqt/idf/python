# Python

![](https://39iosdev.gitlab.io/ccd-iqt/idf/python/assets/python-logo-master-v3-tm.png)

**The Full Python MDBook is available [here](https://39iosdev.gitlab.io/ccd-iqt/idf/python/)**

**Python** is a high-level, object-oriented programming language that emphasizes readability, allowing programmers to read and maintain code more easily, focus on key functionality using less code and makes Python easier to learn.

According to the [TIOBE](https://www.tiobe.com/tiobe-index/), Python is the most popular programming language in the world.

## Chapters and Labs
1. [Introduction](https://39iosdev.gitlab.io/ccd-iqt/idf/python/python_features/index.html)
   1.  [Setup (Lab 1A)](https://39iosdev.gitlab.io/ccd-iqt/idf/python/python_features/lab1a.html)
2. [Data Types](https://39iosdev.gitlab.io/ccd-iqt/idf/python/Data_Types/index.html)
   1. [Variables (Lab 2A)](https://39iosdev.gitlab.io/ccd-iqt/idf/python/Data_Types/lab2a.html)
   2. [Numbers (Labs 2B & 2C)](https://39iosdev.gitlab.io/ccd-iqt/idf/python/Data_Types/lab2b_c.html)
   3. [Strings (Labs 2D & 2E)](https://39iosdev.gitlab.io/ccd-iqt/idf/python/Data_Types/lab2d_e.html)
   4. [Lists (Lab 2F)](https://39iosdev.gitlab.io/ccd-iqt/idf/python/Data_Types/lab2d_e.html)
   5. [Bytes and Bytearray (Lab 2G)](https://gitlab.com/39iosdev/ccd-iqt/idf/python/-/blob/master/mdbook/src/Data_Types/performance_labs/lab2G/lab2g.py)
3. [Control Flow](https://39iosdev.gitlab.io/ccd-iqt/idf/python/Flow_Control/index.html)
   1. [Print (Lab 3A)](https://39iosdev.gitlab.io/ccd-iqt/idf/python/Flow_Control/lab3a.html)
   2. [File I/O (Lab 3B)](https://39iosdev.gitlab.io/ccd-iqt/idf/python/Flow_Control/lab3b.html)
   3. [If, Elif, Else (Lab 3C)](https://39iosdev.gitlab.io/ccd-iqt/idf/python/Flow_Control/lab3c.html)
   4. [While Loops (Lab 3D)](https://39iosdev.gitlab.io/ccd-iqt/idf/python/Flow_Control/lab3d.html)
   5. [For Loops (Lab 3F)](https://39iosdev.gitlab.io/ccd-iqt/idf/python/Flow_Control/lab3e.html)
   6. [Break and Continue (Lab 3G)](https://39iosdev.gitlab.io/ccd-iqt/idf/python/Flow_Control/lab3f.html)
4. [Functions](https://39iosdev.gitlab.io/ccd-iqt/idf/python/functions/index.html)
   1. [Lambda Functions (Labs 4A & 4B)](https://39iosdev.gitlab.io/ccd-iqt/idf/python/functions/lab4a.html)
5. [Objects](https://39iosdev.gitlab.io/ccd-iqt/idf/python/oop/index.html)
   1. [Classes, Instances, Getters/Setters (Lab 5A)](https://39iosdev.gitlab.io/ccd-iqt/idf/python/oop/lab5a.html)
   2. [Inheritance (Lab 5B)](https://39iosdev.gitlab.io/ccd-iqt/idf/python/oop/lab5b.html)
   3. [Composition (Lab 5C)](https://39iosdev.gitlab.io/ccd-iqt/idf/python/oop/lab5c.html)
   4. [Packages (Lab 5D)](https://39iosdev.gitlab.io/ccd-iqt/idf/python/oop/lab5d.html)
6. [Algorithms](https://39iosdev.gitlab.io/ccd-iqt/idf/python/Algorithms/index.html)
   1. [Big O Notation (Lab 6A)](https://39iosdev.gitlab.io/ccd-iqt/idf/python/Algorithms/Big_0_Analysis_Perf_Labs.html)
   2. [Doubly Linked List (Lab 6B)](https://39iosdev.gitlab.io/ccd-iqt/idf/python/Algorithms/Linked_List_Perf_Lab.html)
   3. [Singly Linked List (Lab 6C)](https://39iosdev.gitlab.io/ccd-iqt/idf/python/Algorithms/Singly_Linked_List_Lab.html)
   4. [Queue (Lab 6D)](https://39iosdev.gitlab.io/ccd-iqt/idf/python/Algorithms/Queue_Perf_Lab.html)
   5. [Trees (Lab 6E)](https://39iosdev.gitlab.io/ccd-iqt/idf/python/Algorithms/Trees_Perf_Labs.html)
7. [Advanced](https://39iosdev.gitlab.io/ccd-iqt/idf/python/advanced/index.html)
