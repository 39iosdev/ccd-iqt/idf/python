# Introduction

![](./assets/python-logo-master-v3-tm.png)

**Python** is a high-level, object-oriented programming language that emphasizes readability, allowing programmers to read and maintain code more easily, focus on key functionality using less code and makes Python easier to learn.

According to the [TIOBE](https://www.tiobe.com/tiobe-index/), Python is the most popular programming language in the world.

## Chapters

* [**Features**](python_features/index.html)
* [**Data Types**](Data_Types/index.html)
* [**Control Flow**](Flow_Control/index.html)
* [**Functions**](functions/index.html)
* [**Object Oriented Programming**](oop/index.html)
* [**Algorithms**](Algorithms/index.html)
* [**Data Structures**](Data_Structures/index.html)
* [**Advanced Concepts**](advanced/index.html)

---
