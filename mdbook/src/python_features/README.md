## Python Features

### **Introduction:**
This is an introduction to the structure and use of Python. Here you will learn how to use Python, basic functionality, and how to use the built-in Python documentation.

### **Topics Covered:**

* [**Python Introduction**](python_intro.html)
* [**Python Usage**](python_intro.html#python-usage)
* [**PyDocs**](pydocs_pep8.html#pydocs)
* [**Styling and PEP8**](pydocs_pep8.html#styling-and-pep8)
* [**Introduction to Objects**](objects.html#objects)
* [**Differences between version 2.x and 3.x**](py2_py3.html#py2-vs-py3-differences)
* [**Python Interpretor**](running_python.html#python-interpreter)
* [**Running Python**](running_python.html#running-python)


#### To access the Python Features slides please click [here](slides)

