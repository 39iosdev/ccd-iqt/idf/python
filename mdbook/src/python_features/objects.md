
## Objects

Python has no unboxed or primitive types, no machine values. Instead, **everything in Python is an Object!** Objects involve an abstract way of thinking about programming. Down to the core, an object is just a struct; an encapsulation of variables and functions into a single entity. But on the surface, objects provide inheritance and other powerful uses.

* **Inheritance** means that objects can gain attributes and features from other objects.  This topic will be expanded upon when we get to Object-Oriented Programming.
* Unlike C, where an integer (for example) is a machine primitive, a single unstructured chunk of memory; a Python integer will be an object, a large block of structured memory, where different parts of that structure are used internally to the interpreter. **Why does this matter?** Thanks to everything being an object, our types have more features. Meaning... we can do more things to them. This also means they are larger than the machine primitive types we are used to. 

---
**Setting up your Python environment (lab 1A)**

Please complete lab 1A setting up your Python environment.


