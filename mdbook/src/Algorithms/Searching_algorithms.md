# Searching 

Searching plays a very important role when it comes to working with data.  A **search algorithm** solves the problem of retrieving information stored within some data structure. 

Examples of such structures include, but are not limited to;   **linked lists**, **array data structures**, and **search trees**. The appropriate **search** algorithm often depends, not just on the data structure being searched, but also on any priori knowledge about the data. Searching also encompasses algorithms that query a data structure, such as the **SQL SELECT** command.

---

## Linear Search Algorithm

A **linear Search** algorithm is a simple algorithm that starts at the first item in a list and does a check on each item until the item that is being looked for is found or the end of the list is reached. 

Example:

```python
def linear_search(item, my_list):
    found = False
    position = 0
    while position < len(my_list) and not found:
        if my_list[position] == item:
            found = True
        position = position + 1
    return found  
```
    
Driver code for the linear_search function:

```python
bag = ['book', 'pencil', 'pen', 'note book', 'sharpener', 'eraser']

item = input('What item do you want to check the bag for? ')

item_found = linear_search(item, bag)

if item_found:    
    print('Yes, the item is in the bag')
else:
    print('Oops, the item is not in the bag')
```

- If the word pencil is inputted, the following output should be generated:

    **"Yes, the item is in the bag"**

     This is because the function returns **True** and the item_found variable will evaluate to be True in the condition statement.


- If the word ruler is inputted, the following output should be generated:

    **"Oops, the item is not in the bag"**

    This is because the function returns **False** and the item_found variable will evaluate to be false in the condition statement.

The time complexity of above algorithm is **O(n)**.

For more linear code:  [**linear search**](http://quiz.geeksforgeeks.org/linear-search/). 

---
## Binary Search Algorithm
Another approach to perform the same task is using **binary search**.

Search a **sorted** array by repeatedly dividing the search interval in half. 

- Begin with an interval covering the whole array.

- If the value of the search key is less than the item in the middle of the interval, narrow the interval to the lower half.

- Otherwise narrow it to the upper half.

- Repeatedly check until the value is found or the interval is empty.

 ![](Assets/BinSearch.png)

Example:

```python

def binary_search(target, sorted_list):   
    # first index in list
    left = 0
    # last index in list
    right = len(sorted_list) - 1

    while left <= right:
        # approximate middle index
        midpoint = (left + right) // 2
        if target == sorted_list[midpoint]:
            return midpoint
        elif target < sorted_list[midpoint]:
            # move right "pointer" to the midpoint - 1
            right = midpoint - 1
        else:
            # move left "pointer" to the midpoint + 1
            left = midpoint + 1
    return -1

```
Driver code for binary_search function:

```python
target = 3
sorted_list = [1,2,3,4,5,6,7,8,9]

response = binary_search(target, sorted_list)

if response == -1:
    print("Item not in list")
else:
    print(f"Item found at index {response}")
```

The idea of **binary search** is to use the information that the array is **sorted** and reduce the time complexity to **O(lg n)**.

We basically ignore half of the elements just after one comparison.

1. Compare x with the middle element.
2. If x matches with middle element, we return the mid index.
3. Else If x is greater than the mid element, then x can only lie in right half subarray after the mid element. So we repeat our previous actions for right half.
4. Else (x is smaller) repeat our previous actions for left half.


For a list of size **n** we perform the reduction **n/2/2/2/2**, until we get **1** element

Let **k** be the number of times we divide **n** by 2, then we get:
```text
    (n/2)^k = 1
    n = 2^k
    k = log base 2 of n
```

Big O is **O(lg n)**

It's important to note that there are more than just these types of search functions.



