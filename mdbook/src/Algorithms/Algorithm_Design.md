 # Algorithm Design

An algorithm is a step-by-step procedure that defines a set of instructions that are to be executed in a certain order to get the desired output. Algorithms are generally created independently of an underlying language, i.e. an algorithm can be implemented in more than one programming language.

From the data structure point of view, the following are some important categories of algorithms:

- **Search**  − Algorithm to search for an item in a data structure.
- **Sort**  − Algorithm to sort items in a certain order in a data structure.
- **Insert**  − Algorithm to insert an item in a data structure.
- **Update**  − Algorithm to update an existing item in a data structure.
- **Delete**  − Algorithm to delete an existing item from a data structure.

---
## Characteristics of an Algorithm


Not all procedures can be called an algorithm. An algorithm should have the following characteristics:

- **Unambiguous**: An algorithm should be clear and unambiguous. Each of the steps/phases as well as their inputs/outputs should be clear and must lead to only one meaning.
- **Input**: An algorithm should have 0 or more well-defined inputs.
- **Output**: An algorithm should have 1 or more well-defined outputs that should match the desired output.
- **Finiteness**: Algorithms must terminate after a finite number of steps.
- **Feasibility**: Should be feasible with the available resources.
- **Independent**: An algorithm should have step-by-step directions which should be independent of any programming code.

---
## How to Write an Algorithm?

There are no well-defined standards for writing algorithms. Algorithms are problem and resource dependent and are never written to support a particular programming code.

All programming languages share basic code constructs like loops (do, for, while), flow-control (if-else), etc. These common constructs can be used to write an algorithm.

Algorithms are written in a step-by-step manner, but that is not always the case. Algorithm writing is a process that is executed **after** the problem domain has been well-defined. Hence, we should know the problem domain for which we are designing a solution.

---
### Problem: Design an algorithm to add two numbers and display the result.


**Algorithm-writing example 1:**


**Step 1 - START\
Step 2 − declare three integers a, b & c\
Step 3 − define values of a & b\
Step 4 − add values of a & b\
Step 5 − store output of step 4 to c\
Step 6 − print c\
Step 7 − STOP**



Algorithms tell the programmer how to code the program.
    
Alternatively, the algorithm can be written in other methods.


**Algorithm-writing example 2:**

**Step 1 - START ADD  
Step 2 - get values of a & b\
Step 3 -  c &larr; a + b\
Step 4 - display c\
Step 5 - STOP**


When implementing design and analysis of an algorithm, usually the later method is used to describe an algorithm. Analyzing is made easier by ignoring all unwanted definitions. An analyst can observe what operations are being used and how the process is flowing.

Writing  **step numbers**, is optional.

Algorithms are designed to get a solution to a given problem. A problem can be solved in more than one way.

![](Assets/Problem1.png)

Many solutions for an algorithm can be derived for a given problem. 

The next step is to analyze the proposed algorithm solutions and implement the best suited solution for the problem domain.


---



## **References:**
Data Structures - Algorithms Basics. Tutorial's Point. (n.d.). https://www.tutorialspoint.com/data_structures_algorithms/algorithms_basics.htm.