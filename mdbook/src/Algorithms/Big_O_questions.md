# Big O Questions

## Question about big O terms:
    Does O(100n^2 ) = O(n^2 )?
    Does O(1/4 n^3 ) = O(n^3 )?
    Does O(n) + O(n) = O(n)?


The answers to all of these is Yes! Why? 

Big O notation is concerned with the long-term or the limiting behavior of functions/algorithms. 

If you’re familiar with limits, this will make sense - recall that:

    lim x^2 = lim 100x^2 = ∞
    x→∞       x→∞



Go out far enough on a graph and distinctions can't be made between **100x^2** and **x^2**. 

When talking about big O notation, always drop coefficient multipliers because they don’t make a difference in the long run. 
When analyzing an algorithm/function and it comes out to **O(n) + O(n)**, it doesn’t equal **O(2n)**, we simply say it is **O(n)**.

---

## Constant terms
 Constant terms, no matter how huge, are always dropped if a variable term is present. 
 
 So, **O(800 lg n + 73891) = O(lg n)**, while **O(73891**) by itself with no variable terms present is **O(1)**. 


---
## More big O questions

    Does O(100n^2 + 1\4 n^3 ) = O(n^3 )? 

Again, the answer to this is Yes! 

Big O is only concerned with how an algorithm behaves for very large values of **n**, when **n** is big enough, the **n^3** term will always dominate the **n^2** term, regardless of the coefficient on either of them.


In general, a function is big O of its largest factor. If an algorithm works out to **O(n^2 + n lg n + 100 )** we say it is **O(n^2)**. 

The biggest term dominates the smaller terms.

---
## Big O considerations

-Math operations: Consider all mathematical operations to be constant time **O(1)**.

-**for loops**: Functions containing for loops that go through the whole input are generally **O(n)**

-**if statements/conditionals**: The complexity of conditionals (ie. **if** statements) depends on what the condition is. The complexity of the condition can be constant **O(1)**, linear **O(n)**, or even worse.

-**While loops**: Combine the analysis of a conditional with one of a **for loop**.

-**Nested for loops**: Work from the inside out. Figure out the complexity of the innermost loop, then go out a level and multiply.

-**Recursion**: Can be tricky; think of recursion like a tree. If the tree has lots of branches, it will be more complex than one that has very few branches

---


## **References:**

MITOPENCOURSEWARE. (2011, April 13). 6.00 Notes On Big-O Notation. 
