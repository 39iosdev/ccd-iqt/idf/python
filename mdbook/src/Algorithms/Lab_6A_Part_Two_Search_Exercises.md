# **Lab 6A Part Part Two - Search Exercises**

---

### **Exercise 1**

 Suppose that a list contains the values 20, 44, 48, 55, 62, 66, 74, 88, 93, 99
at index positions 0 through 9.

-Explain the trace of searching for the target value of 90
within the sorted list by using the variables named left, right, and
midpoint in a binary search.

-Repeat for the target value 44.

### **Exercise 2**

The method that’s usually used to look up an entry in a phone book is not
exactly the same as a binary search because, when using a phone book, you don’t always go to the midpoint of the sublist being searched. Instead, you estimate the position of the target based on the alphabetical position of the first letter of the person’s last name.

For example, when you are looking up a number for “Smith,” you look towards the middle of
the second half of the phone book first, instead of in the middle of the entire book.

-Suggest a modification of the binary search algorithm that emulates this strategy for a
list of names. 
-Is its computational complexity any better than that of the standard
binary search?
