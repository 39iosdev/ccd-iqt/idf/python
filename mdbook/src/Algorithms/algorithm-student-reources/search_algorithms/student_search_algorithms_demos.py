"""
Different searching Algorithms

"""

"""
*******************SEARCH FOR THE MINIMUM ***************
This function mimics Python's min function.  Use this function to study the complexity
of this algorithm by returning the index of the minimum item.
This algorithm assumes tha the list is not empty and that the items are NOT in a specified order
This algorithm starts by treating the first position as that of the minimum item.  It then searches 
the right for a similar item, and if found, resets the position of the min to the current position
When it reaches the end of the list it returns the position of the min index.

"""
# complexity O(n)
def index_of_min(my_list):
    min_index = 0
    current_index = 1
    *** INSERT CODE HERE ***
    return min_index

the_list = [2, 6, 5, 1, 3, 4, 9]
print(index_of_min(the_list))

"""
*******************SEQUENTIAL SEARCH ***************
This function returns the position of the target item if found, or -1 otherwise
This function mimics the "in" operator for Python
"""

def seq_search(target, my_list):
    position = 0
     *** INSERT CODE HERE ***
    return -1
the_list = [4, 6, 9, 1, 3, 4, 2]
print(seq_search(9, the_list))

# best case - ? 
# average case - ?
# worst case - ?

"""
*********** BINARY SEARCH OF A SORTED LIST*******************
Used to search an ordered list for a particular value
Divide and conquer approach
Compare target value with middle value, then half of the list
is "discarded", repeat this process until the target is found
Very efficient for large sorted lists
"""
def binary_search(target, sorted_list):   
  
    # first index in list
    left = 0
    # last index in list
    right = len(sorted_list) - 1

    while left <= right:
        # approximate middle index
         *** INSERT CODE HERE ***
        if  *** INSERT CODE HERE ***
            return midpoint
        elif  *** INSERT CODE HERE ***
            # move right "pointer" to the midpoint - 1
            *** INSERT CODE HERE ***
        else:
            # move left "pointer" to the midpoint + 1
            *** INSERT CODE HERE ***
    return -1

my_list = [1,2,3,4,5,6,7,8,9,10]
print(binary_search(3, my_list))

"""
For  a list of size n we perform the reduction n/2/2/2/2... until we get 1 element

Let k be the number of times we divide n by 2, then we get:
    n/2^k = 1
    n= 2^k
    k = log base 2 of n

    Big O(lg n)
"""

