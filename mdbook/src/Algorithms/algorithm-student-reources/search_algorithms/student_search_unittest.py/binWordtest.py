import unittest

# Python3 program to implement Binary
# Search for strings

# Returns index of x if it is present
# in arr[], else return -1
def binary_search(arr, x):
    l = 0
    r = len(arr)
    while (l <= r):
        m = l + ((r - l) // 2)

        res = (x == arr[m])

        # Check if x is present at mid
        if (res == 0):
            return m - 1

        # If x greater, ignore left half
        if (res > 0):
            l = m + 1

        # If x is smaller, ignore right half
        else:
            r = m - 1

    return -1




class TestBinarySearch(unittest.TestCase):

    def test_binary_search(self):
        self.assertEqual(binary_search(["contribute", "persist","excel", "practice"], "excel"), 2)
        self.assertEqual(binary_search(["hawaii", "ohio","arizona", "texas"], "ohio"), 1)




if __name__ == '__main__':
    unittest.main()