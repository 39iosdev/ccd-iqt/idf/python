import unittest

def binary_search(itemlist, index):
    l = 0
    h = len(itemlist) - 1

    while (l <= h):
        m = (l + h) // 2
        if (itemlist[m] == index):
            return m
        elif (itemlist[m] < index):
            l = m + 1
        else:
            h = m - 1

    return -1


class TestBinarySearch(unittest.TestCase):

    def test_binary_search(self):
        self.assertEqual(binary_search([], 3), -1)
        self.assertEqual(binary_search([1, 2], 3), -1)
        self.assertEqual(binary_search([10, 20, 30, 40], 30), 2)
        self.assertEqual(binary_search([11, 22, 33, 45, 55], 33), 2)
        self.assertEqual(binary_search([14, 22, 37, 42], 37), 2)
        self.assertEqual(binary_search([10, 22, 38, 47], 10), 0)
        self.assertEqual(binary_search([17, 23, 34, 42], 42), 3)


if __name__ == '__main__':
    unittest.main()