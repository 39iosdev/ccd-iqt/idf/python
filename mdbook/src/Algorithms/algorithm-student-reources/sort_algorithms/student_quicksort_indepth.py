
def quicksort(lyst):
    print("Here in quicksort")
    print("Sending the lyst and the left index which is the start of the list and the right index which is the end of the list to quicksort helper")
    quicksortHelper(lyst, 0, len(lyst) - 1)
    
def quicksortHelper(lyst, left, right):
    print("\n")
    print("In quicksort helper")
    print(f"This is left: {left}")
    print(f"This is right: {right}")
    if left < right:
        print(f"Left is less than right: left is {left} right is {right}")
        print("Getting the pivot from the partition function:")
        pivotLocation = partition(lyst, left, right)
        print("\n")
        print("Returned from the partition function with the pivot location")
        print(f"This is the pivot {pivotLocation}")
        print(f"Sorting the left side recursively by sending in the lyst which is {lyst}, left which is {left} and right which is the pivot minus one which is {pivotLocation - 1}: ")
        quicksortHelper(lyst, left, pivotLocation - 1)
        print(f"Sorting the right side recursively by send in the lyst which is {lyst} the left which is going to be the pivot + 1 which is {pivotLocation + 1} and right which is {right} ")
        quicksortHelper(lyst, pivotLocation + 1, right)
    else:
        print(f"left of {left} is not less than right of {right} done sorting")
        
def partition(lyst, left, right):
    print("\n")
    print(f"In the partition function the lyst is {lyst} the left is {left} the right is {right}")
    print("Finding the middle index by floor dividing left plus right by 2")
    # Find the pivot and exchange it with the last item
    middle = (left + right) // 2
    print(f"The middle index is {middle}")
    pivot = lyst[middle]
    print(f"The pivot is the element at the index of the middle of the list which is {pivot}")
    print(f"Exchanging the element {lyst[right]} at the ending index of {right } with the middle element {lyst[middle]} at index of {middle}")
    lyst[middle] = lyst[right]
    print(f"This is the lyst now: {lyst}")
    print(f"Assigning the value of pivot which is {pivot} to the element at the end of the list which is {lyst[right]} at index {right}")
    lyst[right] = pivot
    print(f"This is the lyst now: {lyst}")
    # Set boundary point to first position
    print(f"Setting a boundary by assigning the left variable of {left} to the variable boundary")
    boundary = left
    print(f"This is the boundary now: {boundary}")
    # Move items less than pivot to the left
    print(f"Now entering the for loop and moving items less than pivot of {pivot} to the left")
    for index in range(left, right):
        if lyst[index] < pivot:
            print(f"The element {lyst[index]} is less than pivot of {pivot} so they will be swapped")
            swap(lyst, index, boundary)
            print("Done swapping")
            print(f"This is the lyst now {lyst}")
            print("Now the boundary is being increased by one and starting the next iteration")
            boundary += 1
    # Exchange the pivot item and the boundary item
    swap (lyst, right, boundary)
    print(f"This is the end of the for loop and all the swapping is done so this is the lyst: {lyst}")
    print(f"And this is the new right: {right} and this is the new left {left}")
    print(f"This is the boundary that is being returned {boundary}")
    return boundary
    
# Earlier definition of the swap function goes here
def swap(my_list, i, j):
    print("\n")
    print(f"Swapping the element of {my_list[i]} at index {i} with the element {my_list[j]} at the boundary {j}\n")
    """ Exchgange the items at positions i and j"""
    # print("i is : ", my_list[i])
    # print("j is : ", my_list[j])
    # # exchanges the positions of i and j
    temp = my_list[i]
    my_list[i] = my_list[j]
    my_list[j] = temp
    # print("i is : ", my_list[i])
    # print("j is : ", my_list[j])


import random

def main(size = 5, sort = quicksort):
    print("\n")
    # lyst = [9, 4, 5]
    lyst = []
    for count in range(size):
        lyst.append(random.randint(1, size + 1))
    
    print(f"This is the list {lyst}")
    print("Sending to quicksort")
    sort(lyst)

    print(f"This is the sorted lyst: {lyst}")
    
if __name__ == "__main__":
    main()