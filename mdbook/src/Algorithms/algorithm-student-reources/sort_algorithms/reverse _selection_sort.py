def swap(my_list, i, j):
    """ Exchgange the items at positions i and j"""
    # print("i is : ", my_list[i])
    # print("j is : ", my_list[j])
    # exchanges the positions of i and j
    temp = my_list[i]
    my_list[i] = my_list[j]
    my_list[j] = temp
    # print("i is : ", my_list[i])
    # print("j is : ", my_list[j])

def reverse_select_sort(my_list):
    i = 0
    #do n-1 searches for the smallest
    while i < len(my_list) - 1:
        #print(my_list)
        max_index = i
        j = i + 1
        #start a search
        while j < len(my_list):
            #changed the less than to a greater than
            if my_list[j] > my_list[max_index]:
                max_index = j
            j += 1
        #exchange if needed
        if max_index != i:
            swap(my_list, max_index, i)
        i += 1

l = [19, 2, 31, 45, 30, 11, 119, 7, 20]
reverse_select_sort(l)
print(l)

