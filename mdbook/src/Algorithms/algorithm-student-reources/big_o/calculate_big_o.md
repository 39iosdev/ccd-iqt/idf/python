## How to Calculate Big-O of an algorithm
*Quantify how quickly runtime will grow when an algorithm/function runs based on size of input.
	1 - Break algorithm/function into individual operations.
	2 - Calculate Big-O of each operation.
	3 - Add up Big-O of each operation together.
	4 - Remove constants.
	5 - Find the highest order term.

### Example 1:
__________________
	
	Code: 
		def addstuff(num1, num2):
		total = num1 + num2
		return total
	
	#Made of 4 operations
	1: Look up num1 = O(1)
	2: Look up num2 = O(1)
	3: Assign sum of 2 numbers to variable total =  O(1)
	4: Return total = O(1)
	
		The time complexity of each operation is O(1) or constant time because
		the operations only happen once and do not depend on the size of the input.
		The operations will take the same amount of time to run no matter what the input is. 
		
		*Mathematical operations are considered to have constant time:
			Adding 1 + 2 takes the same time as adding 1203442 + 234493.

	Add O(1) + O(1) + O(1) + O(1) = O(4)
	O(4) = O(1 * 4)
	The 4 is surperflous and is a constant so we can strip it, so 1 is now the highest order term.
	
	This algorithm/function is not dependent on the size of the input data, the
	time required to run is the same every single time.
	The Big-O:
		O(1) or Constant time.
	
### Example 2:
____________________
	
	Code:
		for i in len(examplearray):
			print(i)

	For loops usually have a linear time complexity.
	This for loop will also have a linear time complexity.
	The number of operations increases linearly with the size of the input, n is the size of our input.
	In this case the input is the size of the examplearray variable.
	This will run 100 times if there are 100 items in the array or 10 times if there are ten items in the array.
	
	The Big-O:
	 O(n) or linear time

### Example 3:
__________________

	Code:
		for i in len(examplearray): # O(n)
			for j in len(examplearray): # O(n)
				print(i + " " + j) # O(1) This will no longer matter in the scheme of things
	
	Perform a linear time operation for each value in an input,
	not just for the input itself.
	Since this is a nested for loop each for loop is O(n).
	Runtime of this will reference each item or input twice.
	Multiply the dependencies against each other:
		O(n) * O(n) = O(n^2)


	The logic will continue to apply if the for loops continue to nest.
	Every time the number of elements increases the number of operators will increase quadratically.
	If there were three nested loops it would be O(n^3).
	If there were four nested loops it would be O(n^4).
	Since Big-O is looking for the tightest bounds, it would be safe to say that those extra nested loops would also be O(n^2)

	The Big-O: 
		O(n^2)/O(n*n) or Quadratic time.

	
### Example 4:
__________________

	Code:
		n = 100

		# start at 0, go to 2 times n, step 2 
		for i in range(0, 2*n, 2): # O(n)
			j = n # O(1), will not matter
			for j in range(j, i, -1): # O(n)
				print("this is i: {}, and this is j: {}".format(i, j)) # O(1), will not matter
		
	This one is a bit trickier.
	This is a nested for loop and we have said most nested loops are O(n^2) or quadratic time,
	this one is still O(n^2).

	Remember that with nested loops we work from the inside out.
	The print statement in the inner for loop can be considered constant time:
		print("this is i: {}, and this is j: {}".format(i, j)) = O(1)

	The inner for loop is dependent on whatever the outer for loop i is doing.
	So we go look at the outer for loop:
		for i in range(0, 2*n, 2)

	This seems a bit tricky. The statement is going from 0 to 2*n and stepping two spaces.
	This means that the first for loop gets executed n steps and we need to divide the count by two because we skip every other one since the step is 2, O(n/2).
	However, since we drop the constants, it becomes just O(n).
	Now we can go back and look at the inner for loop again.
	As was said, it depends on i in the outer for loop.  Index i goes from 0 to 2*n,
	this means the second for loop gets executed n times the first iteration, n-2 the second iteration,
	n-4 the third up to n/2 where the second for loop will not be executed.
	It becomes n/2 + 1
	We drop the constants and it becomes O(n)
	We multiply.
		The inner print statement which is O(1).
		The inner for loop is O(n).
		The outer for loop is O(n).
		O(1) * O(n) * O(n)
		becomes
		O(n*n) or O(n^2)
	
	
### Example 5:
__________________

	Recursion for Fibonacci sequence.
	Fibonacci definition:
		Each number is the sum of the two preceding ones starting from 0.
		0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144 etc.
		0+1=1
		1+1=2
		1+2=3
		2+3=5
		3+5=8
		5+8=13
		8+13=21
		13+21=34
		21+34=55
		34+55=89
		55+89=144


	Code:
		def fib(n):
		if n<=1:
			return n
		else:
			return(fib(n-1) + fib(n-2))
			
		nterms = 6
		for i in range(nterms):
			print(fib(i))

	
	


	Diagram of the Fibonacci recursion tree:
					          f(6)
						  /    	    \
					f(5)                f(4)
					/   \              /    \
			   f(4)      f(3)        f(3)     f(2)
		       /  \      /  \       /  \      
			 f(3) f(2)  f(2) f(1) f(2) f(1) 
			 /  \
		   f(2) f(1)


		   
	To calculate the Big-O of a recursive function, calculate the number of calls in the recursion call tree.

	Often times the number of calls is O(b^d):
		b is the branching factor:
			Worst case number of recursive calls for one exectuion of the function.
		d is the depth of the tree:
			Longest path from the top of the tree to the base case.
	The branching factor for this is 2.
	The depth is n, where n is the number on which the function is called.
	Big-O:
		O(b^d) = O(2^n)

____________________________________fibonacci with elapsed time_____________

		import time

def fib(n):
    
    if n<=1:
        return n
    else:
        return(fib(n-1) + fib(n-2))
			
nterms = 45

for i in range(nterms):
    start = time.time()
    print(f"\nIteration {i + 1}")
    print(fib(i))
    elapsed = time.time() - start
    print(f"Elapsed is {elapsed}")

