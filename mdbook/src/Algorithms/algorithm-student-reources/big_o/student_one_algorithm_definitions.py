# """  
# Algorithm Efficiency:
#     Efficiency can be analyzed at 2 different stages

#     Stage 1: **Insert Here** Analysis (Before implementation):
#               -Theoretical analysis of an algorithm.  
#               -Efficiency of an algorithm is measured by assuming that all other factors, 
    #               such as processor speed are constant
    #               and have no effect on the implementation
    
#     Stage 2: **Insert Here** Analysis (after implementation): 
#               -Empirical analysis of an algorithm.  
    #           -The alogrithm is implemented using a programming language and then 
    #                 executed on a target machine.  
    #           -Acutal statistics like running time and space required are collected

# Algorithm Complexity:
#     Suppose X is an algorithm and n is the size of the input data.  The time and space used by algorithm X are the two main factors which decide the efficiency of X.

#     Time Factor:  Time is measured by counting the number of key operations such as comparisons in a sorting algorithm

#     Space Factor: Space is measured by counting the maximum memory space required by the algorithm.   

# Space Complexity:  *** Insert Here ***

# 		 The space required by an algorithm is equal to the sum of the following two components

#                   Fixed part:  *** Definition Here ***
#                    Example: 
#                        -simple variablea
#                        -constants
#                        -program size

#                   Variable part:  *** Definition Here ***
#                      Example:
#                       -dynamic memory allocation
#                       -recursion stack space
                  
# Time Complexity: 
#     *** Definition Here ***
#      Time requirements can be defined as a numberical function T(n)
#     T(n) can be measured as the number of steps provided each step consumes constant time

#     Example:
#         Addition of two n-bit integers takes n steps
#         The total computational time is T(n) = c * n
#         Time taken for addition of two bits = c
#         In this instance, T(n) grows linearly as the input size increases 


# Algorithm Design:
#     *** Definition Here ***
# 	Generally created independent of underlying language, can be implemented in more than one programming language

#     Categories of Algorithms:
#         Search - Search for an item in a data structure
#         Sort - Sort items in a certain order
#         Insert - Insert item in a data structure
#         Update - Update an existing item in a data structure
#         Delete - Delete an existing item from a data structure

# Characteristics of Algorithm:
#     Unambiguous -  ?
#     Input -  ?
#     Output -  ?
#     Finiteness -  ?
#     Feasibility -  ?
#     Independent -  ?

# How to write an Algorithm:
#      -Problem and resource dependent.  
#     -A process that is executed after the problem domain is well defined before designing the solution.
#     -Algorithm tells the programmer how to code the program

#     Example:
#         Algorithm design to add two numbers and display the results
#             Step 1 - START ADD
#             Step 2 - get values of x and y
#             Step 3 - z <- x + y
#             Step 4 - display z
#             Step 5 - STOP


