"""
-----------------------------------------Complexity Analysis--------------------------------------------
Counting Operations:
    Assume the following take constant time:
        - Mathematical Operations
        - Comparisons
        - Assignments
        - Accessing objects in memory
        - Returning something from a function

Complexity Analysis:
    A method of determining the efficiency of algorithms that
    allows them to be rated independently of
    platform-dependent timings or impractical instruction counts


Order of Complexity: The difference in perofrmance of your algorithms

   -CONSTANT: O(1) Constant despite input, looks like a straight line

  -LOGARITHMIC: O(lg n) Proportional to the log base 2 of the problem size

  -LINEAR: O(n) Grows in direct proportion to the size of the problem

  -LOG-LINEAR: O(n lg n) Fastest time for sorting currently achievable 

  -QUADRATIC: O(n^2)or O(n*n)  Grows as a function of the square of the problem size

  -EXPONENTIAL: O(k^n) or O(2^n) A growth rate of 2 raised to the 'n'.  *NOTE: These are impractical to run with large problem sizes.


------------------------BIG - O Notation-------------------------------------------------


BIG-O aka Landau's symbol:

 -Used to describe the asymptotic behavior of a function.
    -Tells you how fast a function grows or declines
    -Named after German Number theoretician Edmund Landau who invented the notation.
    The letter O is used because the rate of growth of a function is also called its order

BIG-O notations is used in the analysis of algorithms to describe an algorithm's usage 
of computational resources in a way that is independent of computer architecture or clock rate.

The worst case running time, or memory useage, of an algorithm is often 
expressed as a function of the length of its input using big O notation

An expression of big O notatiation is expressed as a capital letter 'O' followed by a function(generally)
in terms of the variable 'n', which is understood to be the size of the input to the function you are analyzing
O(n)


Dominant: 
    The term with an amount of work in an algorithm that becomes so large that you can ingor the amount of work represented by the the other terms
	
    (1/2)n^2 - (20)n

        The closer you get to infinity, the more you can ignore anything but n
        The dominant term will be n^2

        Represented with Big-O notation
        O(n^2)
    

Asymptotic Analysis:
     A method of describing limiting behavior while looking at large data sets, normally to infinity


O(1) = CONSTANT:
    -Time taken is independent of the amount of data
    Example:
      Stack: push, pop, peek
      Queue: dequeue and enqueue
      Hash tables: inserting, searching and deleting
      Instert nodes to a linked list

O(log n) = LOGARITHMIC aka inverse of exponentiation:
    -Time taken is proportional to the logarithm of the amount of data
    Example:
     -Binary searching a sorted list
     Searching a binary tree
     Divide and conquer algorithm approaches
     Something that reduces the problem in half each time

O(n) = LINEAR:
    -Time taken is directly proportional to the amount of data
    Example:
         Linear search
         Counting items in a list
         comparing a pair of strings
         simple iterative or recursive programs


O(n log n) = LOG-LINEAR(Linearithmic):
     -Time taken is proportional to the logarithm of the amount of data multiplied
      by the amount of data
    Example:
        Merge sort
        Heap sort
        Quick sort

O(n^2) = QUADRATIC:
    -Time taken is proportional to the amount of data squared, twice as much data takes 4 times as long to process.
    -Poor scalability.
    Example:
        Bubble sort
        Selection sort
        Insertion sort
        Traversing a 2d array or nested loops

O(n^k) = POLYNOMIAL
    - Time taken is proportional to the amount of data raised to the power of a constant
    -Example:
    Nested loops depending on the input
    Recursive calls

O(k^n) or O(2^n) = EXPONENTIAL:
    -Let 'k' be a constant and let 'n' be the amount of data
    -Time taken is proportional to a constant raised to the power of the amount of data
    -Very poor scalability almost immediately
    -If constant k is 10, then one extra item of data will slow it down by ten times
    Example:
        Towers of Hanoi

Logarithms - The inverse of exponentiation

    Generally
    x^z = y         log base x of y = z

    2^0 = 1         log base 2 of 1 = 0
    2^1 = 2         log base 2 of 2 = 1
    2^2 = 4         log base 2 of 4 = 2
    2^3 = 8         log base 2 of 8 = 3
    2^4 = 16        log base 2 of 16 = 4

    10^4 = 10000    log base 10 10000 = 4

    notice that each number that we're calculating the log of is twice as much
    as the previous number, but each log is only 1 bigger than the previous value



"""