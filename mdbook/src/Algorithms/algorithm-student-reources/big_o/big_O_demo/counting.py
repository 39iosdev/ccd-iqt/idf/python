"""

This program uses counting to estimate the efficiency of an algorithm
by counting the instructions executed with different problem sizes

Counting provides a good predictor of the amount of abstract work performed by an algorithm
no matter what platform you are running on because you are counting the instructions in the 
high-level code, not the instructions in the machine language.


"""

problemSize = 1000
range_size = 5
print("Count  Problem Size     Iterations")

for count in range(range_size): # O(5n)
    iterations = 0
    #start of algorithm
    work = 1
    for j in range(problemSize):  # O(n)
        for k in range(problemSize):
            iterations += 1 # O(2)
            work += 1 # O(2)
            work -= 1 # O(2)
    #end algorithm
    print(count+1   , end="")
    print("%12d%15d" % (problemSize, iterations))
    problemSize *= 2

"""
In this program, the number of iterations is the square of the problem size

Big O(n^2)

"""