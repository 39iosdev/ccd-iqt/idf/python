"""
Prints the running times for problem sizes that double 
using a single loop

The following program implements an algorithm that counts from 1 to a given number.
Thus the problem size is the number.  We start with the number 10,000,000
We time the algorithm and output the running time to the terminal
We then double the size of this number and repeat the process.
After five increases there is a set of results from which you can generalize.


"""


import time

problemSize = 10000000
range_size = 5
print()
print("Count  Problem Size   Seconds")
work = 1
for count in range(range_size): #O(5n) = O(n) because of limits
    start = time.time()         #O(1)  get the starting time
    #the start of the algorithm
    work = 1 #O(1)
    for x in range(problemSize): #O(n) some work for the problem to do, add then subtract
        work += 1 #O(2) = O(1) because of limits
        work -= 1 #O(2) = O(1) because of limits
        # print(work)
        
    #the end of the algorithm
    elapsed = time.time() - start #get the elapsed time
    print(count+1     , end='') #print the count plus one because we start at 0 in the count
    print("%12d%16.3f" % (problemSize, elapsed)) #format the spacing for the problem size and sedonds to complete
    problemSize *= 2 #double the problem size

    """

    The time module tracks the running time and returns the number of seconds 
    that have elapsed between the current time
    on the computer's clock and
    January 1, 1970

    The difference between the results of two calls of time.time()
    represents the elapsed time in seconds.

    The program does a constant amount of work in the form of two
    assignment statements on each pass through the loop where we add
    one then subtract one to simulate work being done by an algorithm.

    The work consumes enough time on each iteration
    so that the total running time is significant
    but has no other impact on the results

    The Running time more or less doubles when the size of the problem doubles

    In the above code we are determining the big O of the inner **for** loop.

    Combine all the complexities and drop the lesser terms 
    BIG-O

    O(n) + O(1) + (1) -> O(n)

   

    To determine the time complexity for the program with both 
    We can holistically analyze the operation as a whole
    Add, Multiply where needed then drop the lesser terms
    4*O(1) + O(n) * O(n) = O(n^2)
    """

