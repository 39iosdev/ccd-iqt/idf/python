"""
This is a version of timing1.py
The extended assignments have been moved into a nested loop
The loop iterates through the size of the problem within another loop that
also iterates through the size of the problem.

Instead of having the problem size be 10,000,000
We will use 1000.  If we used 10,000,000 the program will not finish
before the lesson is over

"""

import time

problemSize = 1000

for count in range(5): #O(5n)
    start = time.time() #O(1)
    #start the algorithm
    work = 1
    for j in range(problemSize): # O(n)
        for k in range(problemSize):
            work += 1 #O(2)
            work -= 1 #O(2)
    #end of algorithm
    elapsed = time.time() - start

    print(f'{count+1}  {problemSize} - {elapsed}')
    problemSize *= 2


    # """

    # There are many factors that can effect the running time 
    # of an algorithm on different computers
    
    # Predictions of performance generated from hardware timing
    # or software platforms generally cannot be used to predict 
    # the performance on other platforms.

    # It's impractical to determine the running time of large data sets this way.
    # Some algorithms it is impractical to run with large data sets on any computer.




    # """