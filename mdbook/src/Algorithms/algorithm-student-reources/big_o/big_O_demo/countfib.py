"""
This program tracks the number of calls of a recursive
Fibonacci function.
It Demonstrates the problem with sizes that double

The Collections module in Python are containers that are used to store collections of data,
Python collections module was introduced to improve the functionalities of the built-in 
collection containers

Counter is a subclass of dictionary object. The Counter() function in collections module
takes an iterable or a mapping as the argument and returns a Dictionary.
In this dictionary, a key is an element in the iterable or the mapping and value is 
the number of times that element exists in the iterable or the mapping.

Fibonnaci sequence: each number is the sum of the two preceding ones.

"""

from collections import Counter

def fib(n, counter):
    #count the number of calls of fib function
    counter[n] += 1
    if n < 3:
        return 1 #base case
    else:
        return fib(n-1, counter) + fib(n-2, counter)

problemSize = 2
for count in range(5):
    counter = Counter()
    #start of the algorithm
    fib(problemSize, counter)
    #end algorithm
    print(f'{problemSize}   {counter}')
    problemSize *= 2

    """
    As the problem size doubles
    the count(number of recursive calls grows slowly and then rapidly.
    
    """

# #*******************************************************with a class************************
# class Counter(object):
# # """ tracks a count """

#     def __init__(self):
#         self._number = 0

#     def increment(self):
#         self._number += 1
    
#     def __str__(self):
#         return str(self._number)

# def fib(n, counter):
#     #count number of calls of the Fib function
#     counter.increment()
#     if n < 3:
#         return 1 #base case
#     else:
#         return fib(n-1, counter) + fib(n-2, counter)

# problemSize = 2
# print("Problem size     Calls")
# for count in range(5):
#     counter = Counter()
#     #start of the algorithm
#     fib(problemSize, counter)
#     #end algorithm
#     print('%12s%15s' % (problemSize,  counter))
#     problemSize *= 2

#__________________________________without collections import ________________
# def recur_fib(n):
#     if n <= 1:
#         return n
#     else:
#         return(recur_fib(n-1) + recur_fib(n-2))

# nterms = 10

# for i in range(nterms):
#     print(recur_fib(i))