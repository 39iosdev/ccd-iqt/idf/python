# Big O bounds and examples



Now you should understand the **What** and the **Why** of big O notation, as well as how to describe something in big O terms. 

How do we get the bounds in the first place??

Let’s go through some examples.

---

**Example 1:**

**Constant Time**

 We consider all mathematical operations to be constant time (**O(1)**) operations. So the following functions are all considered to be **O(1)** in complexity:

```python

def inc(x):
    return x + 1
```

```python

def mul(x, y):
    return x * y
```

```python

def foo(x):
    y = x * 77.3
    return x / 8.2
```

```python

def bar(x, y):
    z = x + y
    w = x * y
    q = (w**z) % 870

    return 9 * q

```



 Functions that contain **for** loops that iterate over all items in the input are generally **O(n)**.  For example, above we defined a function **mul** that is **O(1)** (constant-time) because it used the
built-in Python operator **"*"**. 
    
If we define our own multiplication function that doesn’t
use **"*"** then it will **NOT** be **O(1)** (constant-time) anymore:

---

**Example 2:**

**For loops**

 The below function is **O(y)**. The way it is defined makes it dependent on the size of the input **y** because the **for** loop executes **y** times.  Then each iteration through the **for** loop executes a constant-time operation.
 
  Note that **O(y)** is equivalent to **O(n)** the variable **y** is representing the size input as is the **n**.  **y** just happens to be the name of the variable in this instance that represents the input size.

```python

def mul2(x, y):
    result = 0

    for i in range(y):
        result += x
    return result

```

 
---


**Example 3:**

 **What is the big-O bound on factorial?**

```python

def factorial(n):
    result = 1

    for num in range(1, n+1):
        result *= num
    return num

```

---


**Example 4:**

 **What is the big-O bound on factorial2?**

```python

def factorial2(n):
    result = 1
    count = 0

    for num in range(1, n+1):
        result *= num
        count += 1
    return num

```

---

**Example 5:**

**Conditionals**

The complexity of conditionals depends on what the condition is. The complexity of the condition can be constant, linear, or even worse.

This below example uses an **if** statement/conditional. The analysis of the runtime of a conditional is highly dependent upon what the conditional’s condition actually is. Checking if one character is equal to another is a constant-time operation, so this example is linear with respect to the size of **a_str**. So, if we let **n = |a_str|**, this function is **O(n)**.

```python

def count_ts(a_str):
    count = 0
    for char in a_str:
        if char == 't':
            count += 1
    return count
```


Now consider the code below:

```python

def count_same_letters(a_str, b_str):
    count = 0

    for char in a_str:
        if char in b_str:
            count += 1
    return count

```

 The above code looks very similar to the function **count_ts**, but it is actually very different!  
 
 The conditional **if** statement checks if **char** is in **b_str**. This check requires that, in a worst case scenario, a check is done on every single character in **b_str**!
 
 Why do we care about the worst case? 
  
Because big O notation is an upper bound on the worst-case running time. Sometimes analysis becomes easier if you ask yourself, what input could I give this algorithm to achieve the maximum number of steps? 

For the conditional, the worst-case occurs when **char** is not in **b_str**. 


Then we have to look at every letter in **b_str** before we can return False. 

So, what is the complexity of this function? 

Let **n = |a_str|** and **m = |b_str|**. 

Then, the **for** loop is **O(n)**. 
Each iteration of the **for** loop executes a conditional check that is, in the worst case, **O(m)**. 

Since we execute an **O(m)** check **O(n)** times, we say this function is:  **O(nm)**.




---

**Example 6:**

 **While loops**
 
  When working with **while** loops you have to combine the analysis of a conditional with one of a **for** loop.
  What is the complexity of the below factorial3?

```python

def factorial3(n):
    result = 1
    while n > 0:
        result *= n
        n -= 1
    return result

```

**Note:** 
  * In Python, **len** is a constant-time operation as well as string indexing (this is because strings are immutable) and list appending.
 
  * CPython just means “Python written in the C language”. You are actually using CPython. 
  * If you are asked to find the worst-case complexity, you want to use the worst case bounds. 
  * Note that operations such as slicing and copying aren’t O(1) operations.  This is because when inserting or deleting or copying, everything must move and has a cost.

```python

def char_split(a_str):
    result = []
    index = 0
    while len(a_str) != len(result):
        result.append(a_str[index])
        index += 1
    return result

```




---
**Example 7:**

 **Nested for loops**

 When working with nested loops, work from the inside out. Figure out the complexity of the innermost loop, then go out a level and multiply (this is similar to the second piece of code in Example 5). So, what is the time complexity of this code fragment, if we let **n = |z|**?

```python

result = 0

for i in range(z):
    for j in range(z):
        result += (i*j)

```
---

**Example 8:**

 **Recursion**

  Recursion can be tricky to figure out; think of recursion like a tree. If the tree has lots of branches, it will be more complex than one that has very few branches.  Consider recursive factorial:

```python

def r_factorial(n):
    if n <= 0:
        return 1

    else:
        return n*r_factorial(n-1)

```

What is the time complexity of the above recursion? 

The time complexity of r_factorial will be dependent upon the number of times it is called. If we look at the recursive call, we notice that it is: r_factorial(n-1). This means that, every time we call r_factorial, we make a recursive call to a subproblem of size **n − 1**. Given an input of size **n**, make the recursive call to the subproblem of size **n − 1** , which makes a call to the subproblem of size **n − 2** , which makes a call to the subproblem of size **n − 3**,... see a pattern? We’ll have to do this until we make a call to **n − n = 0** before we hit the base case of **n** calls. So, r_factorial is **O(n)**. There is a direct correlation from this recursive call to the iterative loop **for i in range(n, 0, -1)**.
In general, we can say that any recursive function **g(x)** whose recursive call is on a subproblem of size **x − 1** will have a linear time bound, assuming that the rest of the recursive call is **O(1)** in complexity (this was the case here, because the **n** factor was **O(1)**).

How about this function?

```python

def foo(n):
    if n <= 1:
        return 1

    return foo(n/2) + 1

```

In the above problem, the recursive call is to a subproblem of size **n/2**. How can we visualize this? 
First we make a call to a problem of size **n**, which calls a subproblem of size **n/2**, which calls a subproblem of size **n/4**, which calls a subproblem of size **n/(2^3)**,...
See the pattern yet? We can make the intuition that we’ll need to make recursive calls until **n = 1**, which will happen when **n/2^x = 1**.

**So, to figure out how many steps this takes, simply solve for x in terms of n:**

```math
n/2^x = 1

n = 2^x
log 2 n = log 2 (2^x)
∴ x = log 2 n
```


It will take **log 2 n** steps to solve this recursive equation. In general, if a recursive function **g(x)** makes a recursive call to a subproblem of size **x/b**, the complexity of the function will be **logbn**. Again, this is assuming that the remainder
of the recursive function has complexity of **O(1)**.

What about the complexity of something like Fibonacci? The recursive call to Fibonacci is:

**fib(n) = fib(n − 1 ) + fib(n − 2 )**. 

This may initially seem linear, but it’s not. If you draw this in a tree fashion, you get something like:

![](Assets/fibonacci_branching_recursive.png)


                         
The depth of this tree (the number of levels it has) is **n**, and at each level we see a branching factor of two (every call to fib generates two more calls to fib). Thus, a loose bound on fib is **O(2^n)**. In fact, there exists a tighter bound on Fibonacci involving the Golden Ratio.

---
## **References:**

MITOPENCOURSEWARE. (2011, April 13). 6.00 Notes On Big-O Notation. 