 # Search Algorithms
 
 ### Search for min
 ---
 
**Example 1:**

```python

def index_of_min_item(input_list):
    """ Returns the minimum item and it's index."""
    minIndex = 0 
    currentIndex = 1
    while currentIndex < len(input_list):
        if input_list[currentIndex] < input_list[minIndex]:
            minIndex = currentIndex
        currentIndex += 1
    return input_list[minIndex], minIndex

input_list = [7,4,2,7,6,2,1,11]

mini = index_of_min_item(input_list)
print(f"\n The smallest item in the list is : {mini[0]} at index: {mini[1]}")
```

**Example 2:**
```python
""" Returns the minimum item in the list"""
def find_min(alist):
    overall_min = alist[0]
    for i in alist:
        is_smallest = True
        for j in alist:
            if i > j:
                is_smallest = False
        if is_smallest:
            overall_min = i
    return overall_min


print(find_min([5, 4, 2, 1]))
print(find_min([4, 7, 2, 5]))

```
**Example 3:**
```python
def find_min(alist):
    """ Returns the minimum item in the list"""
    min_so_far = alist[0]
    for item in alist:
        if item < min_so_far:
            min_so_far = item
    return min_so_far




print(find_min([5, 4, 2, 3, 7]))
print(find_min([4, 3, 5, 9, 7]))
```
---

### Sequential search
**Example 1:**

```python

def sequential_search(target, input_list):
    """ Returns the position of the target item if found, or -1 otherwise in unordered list."""
    position = 0
    while position < len(input_list):
        if target == input_list[position]:
            return position
        position += 1
    return -1

target = 1
input_list = [7,4,2,7,6,2,1,0,11]

position = sequential_search(target, input_list)

if position == -1:
    print("The item is not in the list")
else:
    print(f"The index of the item in the list is : {position}")

```

**Example 2:**

```python
""" Returns True if item is in list otherwise returns False in an ordered list"""
def ordered_sequential_search(alist, item):
    pos = 0
    found = False
    stop = False
    while pos < len(alist) and not found and not stop:
        if alist[pos] == item:
            found = True
        else:
            if alist[pos] > item:
                stop = True
            else:
                pos = pos+1

    return found

test_list = [0, 1, 2, 8, 13, 17, 19, 32, 42,]
print(ordered_sequential_search(test_list, 3))
print(ordered_sequential_search(test_list, 13))

```

---

### Binary Search
**Example 1:**


```python

def binary_search1(target, sorted_list):
    """ Returns the index of the item in the binary search """
    left = 0
    right = len(sorted_list) - 1
    
    while left <= right:
        midpoint = (left + right) // 2
        if target == sorted_list[midpoint]:
            #target is midpoint
            return midpoint
        elif target < sorted_list[midpoint]:
            # Target is less than midpoint
            right = midpoint - 1
        else:#target > sorted_list[midpoint]:
            # Target is greater than midpoint
            left = midpoint + 1
    return -1
     
    

target = 3
sorted_list = [1,3,4,5,7,8,9]

response = binary_search1(target, sorted_list)


if response == -1:
    print("Item not found")
    
else:
    print(f"Item is at index {response}")
```


**Example 2:**
```python

def binary_search2(alist, item):
    """ Returns true if item is in the list"""
    first = 0
    last = len(alist)-1
    found = False

    while first <= last and not found:
        midpoint = (first + last)//2
        if alist[midpoint] == item:
            found = True
        else:
            if item < alist[midpoint]:
                last = midpoint-1
            else:
                first = midpoint+1

    return found

test_list = [0, 1, 2, 8, 13, 17, 19, 32, 42,]
print(binary_search2(test_list, 3))
print(binary_search2(test_list, 13))
```
**Example 3:**

```python
def binary_search3(alist, item):
    """ Binary recursive search"""
    """ Returns true if found otherwise returns False"""
    if len(alist) == 0:
        return False
    else:
        midpoint = len(alist)//2
        if alist[midpoint]==item:
            return True
        else:
            if item<alist[midpoint]:
                return binary_search3(alist[:midpoint], item)
            else:
                return binary_search3(alist[midpoint+1:], item)


test_list = [0, 1, 2, 8, 13, 17, 19, 32, 42,]
print(binary_search3(test_list, 3))
print(binary_search3(test_list, 13))
```

