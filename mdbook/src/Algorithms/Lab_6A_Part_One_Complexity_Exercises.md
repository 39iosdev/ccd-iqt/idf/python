
# **Lab 6A Part One - Complexity Exercises**

---

### **Exercise 1**

Write a tester program that counts and displays the number of iterations of the following loop:

```python

while problemSize > 0:
    problemSize = problemSize // 2

```

### **Exercise 2**

Run the program you created in Exercise 1 using problem sizes, of 1000, 2000, 4000, 10,000, and 100,000.

-What happens to the number iterations as the problem size doubles or increases by a factor of 10?

### **Exercise 3**

The difference between the results of two calls of the functions **time.time()** is an elapsed time.
Because the operating system might use the CPU for part of this time, the elapsed time might not reflect the actual time that a Python code segment uses the CPU.

-Browse the Python documentation for an alternative way of recording the processing time, and describe how this would be done.

### **Exercise 4**

Assume that each of the following expressions indicates the number of operations performed by an algorithm for a problem size of **n**.
-Point out the dominant term of each algorithm and use big O notation to classify it.

```text

    A. 2^n - 4n^2 + 5n
    B. 3n^2 + 6
    C. n^3 + n^2 - n

```

### **Exercise 5**

For problem size **n**, algorithms A and B perform **n^2** and **(1/2)n^2 + (1/2)n** instructions, respectively.

- Which algorithm does more work?

- Are there particular problem sizes for which one algorithm performs significantly better than the
other?

- Are there particular problem sizes for which both algorithms perform approximately the same amount of work?

### **Exercises 6**

At what point does an **n^4** algorithm begin to perform better than a **2^n** algorithm?

---
