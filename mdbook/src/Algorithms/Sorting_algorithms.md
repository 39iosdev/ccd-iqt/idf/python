# Sorting Algorithms


Sorting algorithms specify the particular order to arrange data. 

The importance of sorting lies in the fact that searching for data can be optimized if the data is stored in a sorted manner. Sorting is also used to represent data in more readable formats.

* Bubble Sort
* Insertion Sort
* Shell Sort
* Selection Sort
* Merge Sort
* Quick Sort
---
## **Bubble Sort**
### **Big O Complexity of O(n^2)* 

Bubble Sort is a comparison-based algorithm in which each pair of adjacent elements is compared and then the elements are swapped if they are not in order.

According to Wikipedia "Bubble sort, sometimes referred to as sinking sort, is a simple sorting algorithm that repeatedly steps through the list to be sorted, compares each pair of adjacent items and swaps them if they are in the wrong order. The pass through the list is repeated until no swaps are needed, which indicates that the list is sorted. The algorithm, which is a comparison sort, is named for the way smaller or larger elements 'bubble' to the top of the list."

Although the algorithm is simple, it is slow and impractical for most problems even when compared to insertion sort.

The algorithm can be practical if the input is in some sorted order but may occasionally have some out-of-order elements nearly in position.

![](Assets/bubbleSorta.png)

##### *Bubble Sorting. (2021). [Illustration]. https://www.w3resource.com/php-exercises/searching-and-sorting-algorithm/searching-and-sorting-algorithm-exercise-6.php*

### Example Bubble Sort:
```python
def bubble_sort(input_list):
    """Swap the elements to arrange in order"""
    for iter_num in range(len(input_list) - 1, 0, -1):
        for idx in range(iter_num):
            if input_list[idx] > input_list[idx + 1]:
                temp = input_list[idx]
                input_list[idx] = input_list[idx + 1]
                input_list[idx + 1] = temp
    return input_list

input_list = [19,2,31,45,6,11,121,27]
bubble_sort(input_list)
print(input_list)
```
---



## **Insertion Sort**
### **Big O Complexity of O(n^2)*

Insertion sort involves finding the right place for a given element in a **sorted** list. 
Start by sorting the first two elements by comparing them and placing in order. Then pick the third element and find its proper position among the previous two sorted elements. Gradually go on adding more elements to the already sorted list by putting them in their proper position.


![](Assets/insertionsorta.png)
##### *Insertion Sort Algorithm. (2019). [Illustiration]. https://www.gadgetronicx.com/wp-content/uploads/2019/02/insertion-sort-algorithm.jpg*

### Example Insertion Sort:
```python  
def insertion_sort(input_list):
    for i in range(1, len(input_list)):
        j = i - 1
        nxt_element = input_list[i]
        """ Compare the current element with next one """
        while(input_list[j] > nxt_element) and (j >= 0):
            input_list[j + 1] = input_list[j]
            j = j - 1
        input_list[j + 1] = nxt_element

input_list = [19,2,31,45,30,11,121,27]
insertion_sort(input_list)
print(input_list)
```
---
## **Shell Sort**
### **Big O Complexity of O(n^2)*

The Shell sort involves breaking an original list into sublists.  Each sublist is then sorted using an insertion sort.  

The sublists are chosen using an incrementer **i** or a **gap**.  The sublist is made of items that are **i** or **gap** apart.

In the image below an incrementor of three is used to divide the list of 9 items into 3 sublists.
Each sublist is then sorted.
By sorting the items into sublists they become closer to where they belong in sorted order.

![](Assets/shellsorta.png)

##### *Shell Sort with increments of Three. (n.d.). [Illustration]. https://runestone.academy/runestone/books/published/pythonds/SortSearch/TheShellSort.html*

### Example Shell Sort:
```python 
def shellSort(input_list):
    gap = len(input_list) // 2
    while gap > 0:
        for i in range(gap, len(input_list)):
            temp = input_list[i]
            j = i
            """ Sort the sub list for this gap"""
            while j >= gap and input_list[j - gap] > temp:
                input_list[j] = input_list[j - gap]
                j = j - gap
            input_list[j] = temp
        """ Reduce the gap for the next element"""
        gap = gap //2
input_list = [19,2,31,45,30,11,121,27]
shellSort(input_list)
print(input_list)
```
---
## **Selection Sort**
### **Big O Complexity of O(n^2)*
In selection sort we start by finding the minimum value in a given list and move it to a sorted list. Then we repeat the process for each of the remaining elements in the unsorted list. The next element entering the sorted list is compared with the existing elements and placed at its correct position.
![](Assets/selectionsort.png)

##### *Selection Sort. (n.d.). [Illustration]. https://riptutorial.com/sorting/topic/6170/selection*

### Example Selection Sort:
```python  
def selection_sort(input_list):
    for idx in range(len(input_list)):
        min_idx = idx
        for j in range(idx + 1, len(input_list)):
            if input_list[min_idx] > input_list[j]:
                min_idx = j
        """Swap the minimum value with the compared value"""
        input_list[idx], input_list[min_idx] = input_list[min_idx], input_list[idx]

input_list = [19,2,31,45,30,11,121,27]
selection_sort(input_list)
print(input_list)
```

---
## **Merge Sort**
### **Big O Complexity of O(n lg n)*
**Divide and Conquer** - 
Merge sort divides a group of items into equal halves and then combines them in a sorted manner

![](Assets/mergesort.png)

##### *How MergeSort Algorithm Works Internally. (2021). [Illustration] . https://www.java67.com/2018/03/mergesort-in-java-algorithm-example-and.html*

### Example Merge Sort:

```python  
def merge_sort(unsorted_list):
    if len(unsorted_list) <= 1:
        return unsorted_list
    """Find the middle point and divide it"""
    middle = len(unsorted_list)// 2
    left_list = unsorted_list[:middle]
    right_list = unsorted_list[middle:]
    left_list = merge_sort(left_list)
    right_list = merge_sort(right_list)
    return list(merge(left_list, right_list))

"""Merge the sorted halves"""
def merge(left_half, right_half):
    res =[]
    while len(left_half)!= 0 and len(right_half)!= 0:
        if left_half[0] < right_half[0]:
            res.append(left_half[0])
            left_half.remove(left_half[0])
        else:
            res.append(right_half[0])
            right_half.remove(right_half[0])
    if len(left_half) == 0:
        res = res + right_half
    else:
        res = res + left_half
    return res

unsorted_list = [64,34,25,12,22,11,90]
print(merge_sort(unsorted_list))
```
---
## **Quick Sort**
### **Big O Complexity in an iterative function of O(n lg n) in best case and O(n^2) in worst case*

**Divide and Conquer** -
Divide and Conquer - Quick sort uses the idea of a pivot for sorting. Pivot is usually the midpoint of the items but doesn't have to be, although using the midpoint is often times more efficient.

The pivot is used to partition the list so that the items can be sorted to one side of the pivot or the other.
For example, all items that are less than pivot go to the left and all items greater than pivot go to the right. Divide and Conquer comes in to play because the processes is recursively reapplied to the sublists that are formed at the split caused by pivot. When there are fewer than two items in a list the process ends.

Quicksort has a best case complexity of O(n lg n), however the worst case O(n^2) and the worst case is rare. If Quicksort is implemented recursively then the memory usage for the call stack is considered as well. There are two recursive calls after each partition. The best case for space complexity using recursion would be O(log n) and the worst case would be O(n).

To mitigate the performance of quicksort it is best to choose the median as the pivot of the elements being sorted rather than using the first or last element as the pivot.


### Example Quick Sort:
* *More in depth of quicksort is in the Sort Faster Algorithm demos page.*
```python  
def partition(input_list, left, right):
    
    # Find the pivot and exchange it with the last item
    middle = (left + right) // 2
    pivot = input_list[middle]
    input_list[middle] = input_list[right]
    input_list[right] = pivot
    # Set boundary point to first position
    boundary = left
    # Move items less than pivot to the left
    for index in range(left, right):
        if input_list[index] < pivot:
            swap(input_list, index, boundary)
            boundary += 1
    # Exchange the pivot item and the boundary item
    swap(input_list, right, boundary)
    return boundary
```