# Notes On Big O Notation

## Big O notation is the analysis of algorithms 
* Used to describe an algorithm's usage of computational resources in a way that is independent of a computer's architecture or clock rate.

* The worst case running time, or memory usage of an algorithm, is often expressed as a function of the length of its input using big O notation
* Generally seek to analyze the worst-case running time. However. it is not unusual to see a big O analysis of memory usage. The worst case running time, or memory usage of an algorithm, is often expressed as a function of the length of its input using big O notation.

* An expression in big O notation is expressed as a capital letter **""O""**, which is generally followed by a function in terms of the variable **n**. The variable **n** is understood to be the size of the input to the function that is being analyzed.
* This looks like: **O(n)**.
* A statement such as: **f(x)** is **O(n)** is read as “f of x is big Oh of n”. It is understood that the number of steps to run **f(x)** is linear with respect to **|x|**, the size of the input **x**.
* A description of a function in terms of big O notation only provides an upper bound on the growth rate of the function.
    This means that a function that is **O(n)** is also, technically, **O(n^2)**, **O(n^3)**, etc.
* However, we generally seek to provide the tightest possible bound. If an algorithm is **O(n^3)**, but it is also **O(n^2)**, it is generally best to say **O(n^2)**.

---
## Why do we use big O notation? 
* Big O notation allows us to compare different approaches for solving problems and predict how long it might take to run an algorithm with very large input.
* Big O notation is particularly concerned with the scalability of a function/algorithm.
    Big O bounds may not reveal the fastest algorithm for small inputs (for example, remember that for **x < 0.5, x^3 < x^2**), but big O will accurately predict the long-term behavior of the algorithm.
* This is particularly important in the realm of scientific computing. Analyzing the human genome or data from the Hubble telescope involves input (arrays or lists) of sizes well into the tens of millions (base pairs, pixels, etc.).


* At this scale it becomes easy to see why big O notation is helpful. If a program is used to analyze base pairs and has two different implementations: one is **O(n log n)** and the other is **O(n^3 )**. Even without knowing how fast the computer is that is being used, it’s easy to see that the first algorithm will be **n^3 /(n lg n) = n^2 / lg n** faster than the second, which is a BIG difference in speed for input of large sizes.

---

## Common bounds in order from smallest to largest
    
* **O(1)** Constant time: **O(1) = O(10) = O(2^100)** - why? Even though the constants are huge, they are still constant. If an algorithm takes 2100 discreet steps, regardless of the size of the input, the algorithm is still **O(1)**, the algorithm runs in constant time; it is not dependent upon the size of the input.
    
* **O(lg n)** Logarithmic time: This is faster than linear time; **O(log 10 n) = O(ln n) = O(lg n)**. 
   Traditionally in Computer Science we are most concerned with **lg n**, which is the base-2 logarithm – why is this the case?

    This is the fastest time bound for **searching** a data structure. 
    
* **O(n)** Linear time: Usually results from algorithms that examine every single bit of input such as using a **for** loop.
    
* **O(n lg n)**: This is the fastest time bound that can be currently achieved for **sorting** a list of elements.
    
* **O(n^2 )** Quadratic time: Often this is the bound when we have **nested loops**.
    
* **O(2^n)**: **HUGE!** A number raised to the power of **n** is slower than **n** raised to any other power.

---
## **References:**

MITOPENCOURSEWARE. (2011, April 13). 6.00 Notes On Big-O Notation. 
