# Sort Faster Algorithm Demos
## Quick Sort & Merge Sort Algorithms


The sorting algorithms we looked at previously were easy but inefficient.
We are going to look at two sorting algorithms that have a much better big O complexity.

Merge Sort and Quick Sort
Both algorithms are considered a divide and conquer approach.
By dividing the data we are sorting we potentially reduce the worst case scenario of big O complexity to **O(n lg n)**

---
## **Merge sort**

**Merge sort repeatedly divides a list into individual parts**

![](Assets/leve5a.png)

##### *Lambert, K. A., &amp; Osborne, M. (2019). Fundamentals of Python: first programs. Course Technology/Cengage Learning.*

**Once the list is divided it sorts the parts and merges them in order**

![](Assets/leve6a.png)

##### *Lambert, K. A., &amp; Osborne, M. (2019). Fundamentals of Python: first programs. Course Technology/Cengage Learning.* 

We discussed Merge sort earlier but the following code shows a more in-depth use.

### Implementation of Merge Sort:
```python
"""
Divide and Conquer strategy
Compute the middle position of a list and recursively sort its left and right sublists
Merge the two sorted sublists back into a single sorted list
Stop the process when sublists can no longer be subdivided
"""
def merge_sort(my_list):
    # my_list is to be sorted
    # copyBuffer is temporary space needed during the merge
    copy_buffer = list(my_list)
    merge_sort_helper(my_list, copy_buffer, 0, len(my_list) - 1)


def merge_sort_helper(my_list, copy_buffer, low, high):
    # low, high = bounds of sublist
    # middle = midpoint of sublist
    if low < high:
        middle = (low + high) // 2
        # use recursive calls to continue sorting into sublists
        merge_sort_helper(my_list, copy_buffer, low, middle)
        merge_sort_helper(my_list, copy_buffer, middle + 1, high)
        merge(my_list, copy_buffer, low, middle, high)

"""
Initialize i1 and i2 to first items in each sublist.
The merge function combines two sorted sublists into a larger sorted sublist.
The first sublist lies between low and middle and the second between middle + 1 and high
Set up index pointers to the first items in each sublist; these are low and middle + 1
Starting with the first item in each sublist, repeatedly compare items. Copy the smaller item
from its sublist to the copy_buffer and advance to the next item in the sublist.
Copy the portion of copy_buffer between low and high back to corresponding positions in my_list.
"""
def merge(my_list, copy_buffer, low, middle, high):
    i1 = low
    i2 = middle + 1
    # put items from sublists into copy_buffer so order is maintained
    for i in range(low, high + 1):
        if i1 > middle:
            #first sublist exhausted
            copy_buffer[i] = my_list[i2]
            i2 += 1
        elif i2 > high:
            # second sublist exhausted
            copy_buffer[i] = my_list[i1]
            i1 +=  1
        elif my_list[i1] < my_list[i2]:
            copy_buffer[i] = my_list[i1]
            i1 += 1
        else:
            copy_buffer[i] = my_list[i2]
            i2 += 1
    # Copy sorted items back into proper position in the list
    for i in range(low, high + 1):
        my_list[i] = copy_buffer[i]


""" Main"""
import random

def main(size = 20, sort_this = merge_sort):
    """create a list of random numbers"""
    my_list = []
    for _ in range(size):
        my_list.append(random.randint(1, size + 1))
    print(my_list)
    sort_this(my_list)
    print(my_list)

main()

```
---

## **Quicksort:**

![](Assets/action4a.png)


**Implementation of Quicksort**

```python

def quick_sort(my_list):
    quick_sort_helper(my_list, 0, len(my_list) - 1)


#recursive function 

def quick_sort_helper(my_list, left, right):
    if left < right:
        pivot_location = partition(my_list, left, right)

        #recursively calls quick_sort_helper for the left of the partition
        quick_sort_helper(my_list, left, pivot_location - 1)

        #recursively calls quick sort helper for the right of the partition
        quick_sort_helper(my_list, pivot_location + 1, right)
    
def partition(my_list, left, right):
    #find the pivot and exchange it with the last item
    middle = (left + right) //2
    pivot = my_list[middle]
    my_list[middle] = my_list[right]
    my_list[right] = pivot 
    # set boundary point for first position
    boundary = left
    #moves items less than the pivot to the left
    for index in range(left, right):
        if my_list[index] < pivot:
            swap(my_list, index, boundary)
            boundary += 1
    #exchange the pivot item and boundary
    swap(my_list, right, boundary)
    return boundary



def swap(my_list, i, j):
    #exchanges the positions of i and j
    temp = my_list[i]
    my_list[i] = my_list[j]
    my_list[j] = temp

""" main """

import random

def main(size = 20, sort = quick_sort):
    my_list = []
    for count in range(size):
        my_list.append(random.randint(1, size + 1))
    print(my_list)
    sort(my_list)
    print(my_list)
    
if __name__ == "__main__":
    main()

```