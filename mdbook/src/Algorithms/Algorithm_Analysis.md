# Algorithm Analysis

---


**Efficiency of an algorithm can be analyzed at two different stages, before implementation and after implementation:**

* **Priori Analysis**  (Before Implementation) - Theoretical analysis of an algorithm: Efficiency of an algorithm is measured by assuming that all other factors, such as processor speed, are constant and have no effect on the implementation.​
* **Posterior Analysis**  (After Implementation) - Empirical analysis of an algorithm: The selected algorithm is implemented using a programming language which is then executed on a target machine. In this analysis statistics, such as running time and the space required by an algorithm, are collected.

---
## Algorithm Complexity

The time and space used by an algorithm are the main factors that decide an algorithms efficiency.

Suppose  **x**  is an algorithm and  **n**  is the size of the input data. The time and space used by algorithm **x** are the two main factors which decide the efficiency of **x**.

* **Time Factor**: Measured by counting the number of key operations, such as counting comparisons in a sorting algorithm.

* **Space Factor**: Measured by counting the maximum memory space required by the algorithm.

The complexity of an algorithm **f(n)** gives the running time and/or the storage space required by the algorithm in terms of **n** as the size of input data.

---
## Space Complexity

Space complexity of an algorithm represents the amount of memory space required by the algorithm in its life cycle. The space required by an algorithm is equal to the sum of the following two components:

* **Fixed**:  The space required to store certain data and variables that are independent of the size of the problem (simple variables and constants used, program size, etc.).
* **Variable**: The space required by variables whose size depends on the size of the problem (dynamic memory allocation, recursion stack space, etc.).

Space complexity **S(P)** of any algorithm **P** is **S(P) = C + SP(I)**, where **C** is the fixed part and **S(I)** is the variable part of the algorithm which depends on instance characteristic **I**. 

Space Complexity Concept Example:

**Algorithm: SUM(a, b)  
Step 1 - START    
Step 2 - c &larr; a + b + 10  
Step 3 - Stop**


Here we have three variables: **a**, **b**, and **c** and one constant of **10**. Hence **S(P) = 3 + 1**. 

Space depends on the data types of the given variables and the constant types and multiplying them accordingly.

---
## Time Complexity

Time complexity of an algorithm represents the amount of time required by the algorithm to run to completion. Time requirements can be defined as a numerical function **T(n)**. 

The function **T(n)** can be measured as the number of steps, provided each step consumes constant time.  



For example, addition of two **n**-bit integers takes **n** steps. Consequently, the total computational time is **T(n) = c ∗ n**, where **c** is the time taken for the addition of two bits. Observe that **T(n)** grows linearly as the input size increases.



---


  ## **References:**
  Data Structures - Algorithms Basics. Tutorial's Point. (n.d.). https://www.tutorialspoint.com/data_structures_algorithms/algorithms_basics.htm.