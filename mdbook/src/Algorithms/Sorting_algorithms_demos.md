# Sort Algorithm Demos

---

The algorithms examined here are easy to write but inefficient. Some of the algorithms we discuss here will utilize a list of integers and the
**swap** function which is defined below.

**swap function:** 
```python
def swap(my_list, i, j):
    """ Exchange the items at positions i and j"""
    print("i is : ", my_list[i])
    print("j is : ", my_list[j])
    # exchanges the positions of i and j
    temp = my_list[i]
    my_list[i] = my_list[j]
    my_list[j] = temp
    print("i is : ", my_list[i])
    print("j is : ", my_list[j])

# another option to swap elements in our list
# def swap(my_list, i, j):
#     my_list[i], my_list[j] = my_list[j], my_list[i]

my_list = [3, 5, 4]
print(f"This is the list before swapping: {my_list}")
i = 0
j = 1
swap(my_list, i, j)
print(f"This is the list after swapping: {my_list}")
```   
**Basic Sort Demo 1:** 
---

**Selection Sort Algorithm**

![](Assets/sort1a.png)

```python
""" 
Each pass through the main loop selects a single item to be moved
# This will search the list for the position of the smallest item
# If that position is not the first position it swaps the items at those positions
# It then finds the next smallest item and swaps the item and the second position and so on
"""
def select_sort(my_list):
    i = 0
    # do n-1 searches for the smallest
    while i < len(my_list) - 1:
        print(my_list)
        min_index = i
        j = i + 1
        # start a search
        while j < len(my_list):
            if my_list[j] < my_list[min_index]:
                min_index = j
            j += 1
        # exchange if needed
        if min_index != i:
            swap(my_list, min_index, i)
        i += 1

""" with for loops
def select_sort(my_list):
    for i in range(len(my_list) - 1):
        min_index = i
        for j in range(i + 1, len(my_list)):
            if(my_list[j] < my_list[min_index]):
                min_index = j
        if(min_index != i):
            my_list[i], my_list[min_index] = my_list[min_index], my_list[i]

    return my_list

"""
        

my_list = [3, 5, 23, 43, 12, 14, 7, 98, 24, 120, 87, 9, 41 ]
print(my_list)
select_sort(my_list)
print(my_list)


#Complexity O(n^2)
```
 
 ---
 
**Bubble Sort Algorithms**

![](Assets/sort2a.png)

```python
"""
The strategy is start at the beginning of the list and compare pairs of data
items as it moves down to the end. Each time the items in the pair are out of order,
the algorithm swaps them. The largest item will eventually 'bubble' out to the 
end of the list. The algorithm repeats this process until the list is sorted
from smallest to largest
"""

def bubble_sort(my_list):
    n = len(my_list)
    # do n - 1 searches
    while n > 1:
        print(my_list)
        i = 1
        # start each bubble
        while i < n:
            if my_list[i] < my_list[i-1]:
                # exchange if needed
                swap(my_list, i, i-1)
            i += 1
        n -= 1

my_list = [3, 5, 23, 43, 12, 14, 7, 98, 24, 120, 87, 9, 41]
print(my_list)
bubble_sort(my_list)
print(my_list)
# Complexity of O(n^2)
```        
        
---

**Modified Bubble Sort function:**

```python
""" 
We can update the code for bubble sort to have linear time complexity in the best case scenario where our list is already sorted

"""

def bubble_sort2(my_list):
    n = len(my_list)
    # do n - 1 searches
    while n > 1:
        print(my_list)
        swapped = False
        i = 1
        # start each bubble
        while i < n:
            if my_list[i] < my_list[i-1]:
                # exchange if needed
                swap(my_list, i, i-1)
                swapped = True
            i += 1
        if not swapped:
            return
        n -= 1

my_list = [3, 5, 23, 43, 12, 14, 7, 98, 24, 120, 87, 9, 41]
print(my_list)
bubble_sort2(my_list)
print(my_list)
```        

---

**Insertion Sort Algorithms**

![](Assets/sort3a.png)


```python
"""
On the ith pass through the list, where i ranges from 1 to n-1, the ith 
item should be inserted into its proper place among the first i items in the 
list. After the ith pass, the first i items should be in sorted order
This process is analogous to the way in which many people organize playing cards
in their hands. That is, if you hold the first i-1 cards in order, you pick
the ith card and compare it to these cards until its proper spot is found.
Insertion sort consists of two loops. The outer loop traverses the positions
from 1 to n-1. For each position i in this loop, you save the item and start 
the inner loop at position i-1. For each position j in this loop, you move
the item to position j+1 until you find the insertion point for the saved(ith) item
Insertion sort is good for partially sorted lists due to the inner loop

"""
def insertion_sort(my_list):
    i = 1
    while i < len(my_list):
        print(my_list)
        item_to_insert = my_list[i]
        j = i-1
        while j >= 0:
            if item_to_insert < my_list[j]:
                my_list[j+1] = my_list[j]
                j -= 1
            else:
                break
        my_list[j+1] = item_to_insert
        i += 1

my_list = [3, 5, 23, 43, 12, 14, 7, 98, 24, 120, 87, 9, 41]

print(my_list)
insertion_sort(my_list)
print(my_list)

#  complexity of O(n^2)
```       


---

### Quick Sort & Merge Sort Algorithms

---

**Quicksort:**

![](Assets/action4a.png)

---

**Implementation of Quicksort**

```python

def quicksort(my_list):
    quicksortHelper(my_list, 0, len(my_list) - 1)
    
def quicksortHelper(my_list, left, right):
    if left < right:
        pivotLocation = partition(my_list, left, right)
        quicksortHelper(my_list, left, pivotLocation - 1)
        quicksortHelper(my_list, pivotLocation + 1, right)
        
def partition(my_list, left, right):
    # Find the pivot and exchange it with the last item
    middle = (left + right) // 2
    pivot = my_list[middle]
    my_list[middle] = my_list[right]
    my_list[right] = pivot
    # Set boundary point to first position
    boundary = left
    # Move items less than pivot to the left
    for index in range(left, right):
        if my_list[index] < pivot:
            swap(my_list, index, boundary)
            boundary += 1
    # Exchange the pivot item and the boundary item
    swap(my_list, right, boundary)
    return boundary
    
# Earlier definition of the swap function goes here

import random

def main(size = 20, sort = quicksort):
    my_list = []
    for count in range(size):
        my_list.append(random.my_list(1, size + 1))
    print(my_list)
    sort(my_list)
    print(my_list)
    
if __name__ == "__main__":
    main()

```

---

**Merge Sort Examples**

```python

from arrays import Array

def mergeSort(my_list):
    # my_list     list being sorted
    # copyBuffer temporary space needed during merge
    copyBuffer = Array(len(my_list))
    mergeSortHelper(my_list, copyBuffer, 0, len(my_list) - 1)
```



```python

def mergeSortHelper(my_list, copyBuffer, low, high):
    # my_list          list being sorted
    # copyBuffer    temp space needed during merge
    # low, high     bounds of sublist
    # middle        midpoint of sublist
    if low < high:
        middle = (low + high) // 2
        mergeSortHelper(my_list, copyBuffer, low, middle)
        mergeSortHelper(my_list, copyBuffer, middle + 1, high)
        merge(my_list, copyBuffer, low, middle, high)
        
```

---
![](Assets/leve5a.png)

![](Assets/leve6a.png)

---

```python

def merge(my_list, copyBuffer, low, middle, high):
    # my_list           list that is being sorted
    # copyBuffer     temp space needed during the merge process
    # low            beginning of first sorted sublist
    # middle         end of first sorted sublist
    # middle + 1     beginning of second sorted sublist
    # high           end of second sorted sublist
    # Initialize     i1 and i2 to the first item of second sorted sublist
    i1 = low
    i2 = middle + 1
    # Interleave items from the sublists into the copyBuffer in such a way that order is maintained
    for i in range(low, high +1):
        if i1 > middle:
            copyBuffer[i] = my_list[i2]   # First sublist exhausted
            i2 += 1
        elif i2 > high:
            copyBuffer[i] = my_list[i1]   # Second sublist exhausted
            i1 +=  1
        elif my_list[i1] < my_list[i2] 
            copyBuffer[i] = my_list[i1]   # Item in first sublist <
            i1 += 1
        else:
            copyBuffer[i] = my_list[i2]   # Item in second sublist <
            i2 += 1
    for i in range (low, high + 1):    # Copy sorted items back to proper position in my_list
        my_list[i] = copyBuffer[i]        # proper position in my_list
        
```
