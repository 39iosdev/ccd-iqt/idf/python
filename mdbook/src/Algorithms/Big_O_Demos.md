
## Big O Demonstration Labs 

---

### Algorithms Demo 1:

***timing1.py***

Prints the running times for problem sizes that double by using a single **for** loop. 

The following program implements an algorithm that counts from 1 to a given number.

Thus the problem size is the number.  

This problem starts with the number 10,000,000 then it times the algorithm and outputs the running time to the terminal.  
Then the size of the problem_size is doubled and the process is repeated.
After five increases there is a set of results from which you can generalize.


```python
import time

problem_size = 10000000
range_size = 5
print()
print("Count  Problem Size   Seconds")
work = 1
for count in range(range_size): 
    start = time.time()          
   
    work = 1
    #the start of the algorithm to examine 
    for x in range(problem_size): #O(n)
        work += 1 #O(2) = O(1) because of limits
        work -= 1 #O(2) = O(1) because of limits
    #the end of the algorithm to examine
    elapsed = time.time() - start
z
    print(f'{count + 1}  {problemSize} - {elapsed}')
    problem_size *= 2
```
  

**Simulated Output:**

|COUNT|PROBLEM SIZE|SECONDS|
|   :---:    | :---: |:-----:|
|1 |10000000|3.8|
|2 |20000000|7.591|
|3 |40000000|15.352|
|4 |80000000|30.697|
|5 |160000000|61.631

***seconds will vary based on your system**

**Explanation timing1.py:**

In the above code we are determining the big O of the inner **for** loop.

Combine all the complexities and drop the lesser terms

    O(n) + O(1) + (1) -> O(n)
---

### Algorithms Demo 2

***timing2.py***

Prints the running times for problem sizes that double, using nested loops.

The extended assignments are moved to **nested for loops** from timing1.py.  
Because of the number of iterations the problems size is dropped to 1000 to save time.


```python

import time

problem_size = 1000

for count in range(5): 
    start = time.time() 
    work = 1
     #the start of the algorithm to examine 
    for j in range(problem_size): # O(n)
        for k in range(problem_size): # O(n)
            work += 1  #O(2) = O(1) because of limits
            work -= 1  #O(2) = O(1) because of limits
    #the end of the algorithm to examine
    elapsed = time.time() - start

    print(f'{count + 1}  {problem_size} - {elapsed}')
    problem_size *= 2

```

**Simulated Output:**

|COUNT|PROBLEM SIZE|SECONDS|
| :---: | :---: | :---: |
|1 |1000|0.387|
|2 |2000|1.581|
|3 |4000|6.463|
|4 |8000|25.702|
|5 |16000|102.666|

***seconds will vary based on your system**

**Explanation timing2.py:**

In the above code we are looking at the inner two **for** loops only to determine the big O.

Since this is a nested loop we start on the inside and work our way out and multiply.
Then combine all the complexities and drop the lesser terms.

    O(n) * O(n) + O(1) + (1) -> O(n^2)

---

### Algorithms Demo 3

***counting.py***

Prints the number of iterations for problem sizes
that double, using a **nested loop**.

This program uses counting to estimate the efficiency of an algorithm by counting the number of instructions executed with different problem sizes.

Counting provides a good predictor of the amount of abstract work performed by an algorithm
no matter what platform you are running on is because you are counting the instructions in the high-level code, not the instructions in the machine language.



```python

problem_size = 1000
range_size = 5
print("Count  Problem Size     Iterations")

for count in range(range_size): 

    iterations = 0
    work = 1
    
    for j in range(problem_size): 
        for k in range(problem_size):
            iterations += 1 
            work += 1 
            work -= 1 
   
    print(f'{count + 1}  {problem_size} - {elapsed}')
    problem_size *= 2
    
```
* As you can see from the results, the number of iterations is the square of the problem size

**Expected Output:**

|COUNT|PROBLEM SIZE|ITERATIONS|
| :--: | :-------: | :-------: |
|  1   | 1000       |1000000|
|  2   | 2000       |4000000|
|  3   | 4000       |16000000|
|  4   | 8000       |64000000|
|  5   | 16000      |256000000|




Again the big O for this is O(n^2)



---

### Algorithms Demo 4

***countfib.py***

Prints the number of calls of a **recursive** Fibonacci function with problem sizes that double.

This program tracks the number of calls of a recursive Fibonacci function. It demonstrates the problem with sizes that double.

The Collections module in Python are containers that are used to store collections of data,
Python collections module was introduced to improve the functionalities of the built-in 
collection containers

Counter is a subclass of the dictionary object. The Counter() function in the collections module
takes an iterable or a mapping as the argument and returns a Dictionary.
In this dictionary, a **key** is an element in the iterable or the mapping and **value** is 
the number of times that element exists in the iterable or the mapping.

We will use the sum of the values in the collection counter to determine the number of recursive 
calls that are made for each iteration of the range.

 

```python

from collections import Counter

def fib(n, counter):
    #count the number of calls of fib function
    counter[n] += 1
    # print(n)
    if n < 3:
        return 1 #base case
    else:
        return fib(n-1, counter) + fib(n-2, counter)

problem_size = 2
for count in range(5):
    calls = 0
    counter = Counter()
    #start of the algorithm
    fib(problem_size, counter)
    #sums the quantity of values in the collection
    #counter which will tell you the number of recursive calls made
    total = sum(counter.values())  
    #end algorithm
    print(f'Problems size: {problem_size}| Calls made: {total} Counter: {counter}')
    problem_size *= 2
    
```

**Expected Output:**

|PROBLEM SIZE|CALLS MADE|
| :---: | :---: |
|2|1|
|4|5|
|8|41|
|16|1973|
|32|4356617|
 
 ---
 
