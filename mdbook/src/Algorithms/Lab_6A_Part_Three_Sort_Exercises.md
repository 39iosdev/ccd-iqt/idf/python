# **Lab 6A Part Three - Sort Exercises** 

---

### **Exercise 1**

Which configuration of data in a list causes the smallest number of exchanges
in a selection sort? Which configuration of data causes the largest number of
exchanges?

### **Exercise 2**

Explain the role that the number of data exchanges plays in the analysis of
selection sort and bubble sort. What role, if any, does the size of the data
objects play?

### **Exercise 3**

Explain why the modified bubble sort still exhibits O(n^2) behavior on average.

### **Exercise 4**

Explain why insertion sort works well on partially sorted lists.

### **Exercise 5**

The list method **reverse** reverses the elements in the list. Define a function named reverse that
reverses the elements in its list argument (without using the method reverse). Try to make this
function as efficient as possible, and state its computational complexity using big-O notation.

```python
    def rev(my_list): 
        //Insert Code
```

### **Exercise 6**

Python’s **pow** function returns the result of raising a number to a given power. Define a function
**expo** that performs this task and state its computational complexity using big-O notation. The first
argument of this function is the number, and the second argument is the exponent (nonnegative numbers only).
You can use either a loop or a recursive function in your implementation, but **DO NOT** use Python’s  "**" operator
or "pow" function.  You may need to do some research to figure out.

```python
     def expo(num, exponent):  
        //Insert Code     
```

### **Exercise 7**

Write the code for selection_sort that returns the sorted itemsList that passes the unit tests.

```python
    import unittest

    def selection_sort( itemsList ): 
        //Insert Code
        return itemsList

    class TestSelectSort(unittest.TestCase):

    def test_selection_sort(self):
        self.assertEqual(selection_sort([21,6,9,33,3]), [3, 6, 9, 21, 33])
        self.assertEqual(selection_sort([120,50,1,12,75,34,89]), [1,12,34,50,75,89,120])

    if __name__ == '__main__':
        unittest.main()
```
