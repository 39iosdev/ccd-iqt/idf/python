# JSON Lab I

## Instructions

* Your principal has heard about this data format, JSON, and wants to use it.
* He has asked you, the Computer Science teacher, to build an application to read in student data and store it in a JSON file.  
* Each class has at least 3 students, and each will have at least 5 grades.
* Your application will also need to ask for the information, so the other teachers can use the application.
* Using the JSON module in Python, write the information to a file so that it can be recalled later.

## Requirements

* Adhere to [PEP8](https://www.python.org/dev/peps/pep-0008/)  (comments, formatting, style, etc)
* Make sure to include your student docstring at the top(name, lab, date)
* Follow instructions above
---