# JSON Lab II

## Instructions

* Your principal has taken a look at the JSON file and can't figure out how to read information from it (then why....?).
* He's asked you to build a report based on the information stored within the JSON file.
* Each student's average, the class's average, and the mode of the class's scores (because the principal likes to know what is the most common score for some reason) need to be displayed.* Additionally, a letter grade needs to be displayed for student and class average, using the following table:

| Min | Letter | Max |
|:---|:----:|:---|
|  90 |    A   | 100 |
|  80 |    B   | 89 |
|  70 |    C   | 79 |
|  0 |    D   | 69 |

## Requirements

* Adhere to [PEP8](https://www.python.org/dev/peps/pep-0008/)  (comments, formatting, style, etc)
* Make sure to include your student docstring at the top(name, lab, date)
* Follow instructions above
---