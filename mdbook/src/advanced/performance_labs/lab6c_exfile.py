'''
Complete the function below to meet the following requirements:
- Two arguments will be passed, the file extension (as '.xxx') and an integer
- Obtain a directory listing of all files in the current directory with the specified extension
- From all files found, grab the Xth word as defined by the integer parameter
- Compile the Xth word from each file into a single string separated by spaces.
- Newline characters should be removed from the string.
- Return a tuple of the string, the number of files of the specified type that existed, and the OS
- This should work in both Windows and linux systems
- if there are no files of the specified type then return the message "No files found."
- if any of the files do not have an Xth element then just grab the last word. 
'''
import os, subprocess

def getFiles(ext):
    return

def xthWordTxt(ext, num):
    return 