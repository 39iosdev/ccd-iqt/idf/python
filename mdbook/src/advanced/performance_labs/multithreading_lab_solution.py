"""
Write a function that would execute both the slow_welcome() and ackermann(3, 4) function
without waiting for the other to finish.

Hint: Your script is already in a thread
"""

import sys
import threading
import time

def slow_welcome():
    print("Welcome")
    time.sleep(2)
    print("To")
    time.sleep(2)
    print("My")
    time.sleep(2)
    print("Python")
    time.sleep(2)
    print("Script")

def ackermann(m, n):
    if m == 0:
        return n + 1
    elif n == 0:
        return ackermann(m - 1, 1)
    else:
        return ackermann(m - 1, ackermann(m, n - 1))


def main():
    sys.setrecursionlimit(100000)

    slow_welcome_thread = threading.Thread(target=slow_welcome, args=())


    print("Starting slow_welcome()")
    slow_welcome_thread.start()

    print("Starting ackermann(3, 4)")
    print(f"Ackerman results: {ackermann(3,4)}")

main()
