# Multithreading Lab

## Instructions

### Just Threads

*  Making use of the threading and time modules from the Python Standard Library, create a program that uses 4 threads to add values to a global list variable
* The global list variable represents a remote database that our threads are interfacing with
* At least two of the threads must add values from overlapping ranges (for example, one thread adds values from 3 to 7 and another adds values from 6 to 10)
* After a thread adds a value to the list, use the time module to delay the thread (a delay of 1 or 2 seconds is fine)
* The threads need to be added to a thread list variable, so that all threads complete their work
* Once all threads have added their values to the list, print out the values of the global list variable
* All threads must make use of the same function to add the values to the global list variable

### Locks

* Implement a lock for the function so that the lock is not released until the function finishes adding all values to the global list variable
* Implement a lock for the function so that the lock is acquire before a value is added and then immediately released, followed by the time delay
* Think about the differences between these two implementations and be prepared to talk about it

## Requirements

* Adhere to [PEP8](https://www.python.org/dev/peps/pep-0008/)  (comments, formatting, style, etc)
* Make sure to include your student docstring at the top(name, lab, date)
* Follow instructions above

## Bonus Material

* Add threading to a previous lab