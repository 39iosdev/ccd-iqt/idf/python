

Performance labs 2

## Lab #1
For this Lab, the output should look like this:
```
function_A--> starting 
function_B--> starting 
function_C--> starting 


function_A--> exiting 
function_B--> exiting 
function_C--> exiting
```

## Lab #2
```python 
# Explain the code below
# You are replacing a teammember who did not document his code. Write the documentation for his code.

# Explain the below program:

import time
import threading 

def Tfunc(i):
 print("Thread no.:%d" % (i+1))
 time.sleep(5)
 print("%d finished sleeping from thread\n" % i)

for i in range(3):
 t1 = threading.Thread(target=Tfunc, args=(i,))
 t1.start()
 

 c=t1.isAlive()

 c1=t1.getName()
 print('\n',c1,"is Alive:",c)

 count=threading.active_count()
 print("Total No of threads:",count)
 ```

 ## Lab 3

```python
# Explain the code below
# You are replacing a teammember who did not document his code. Write the documentation for his code.

# Explain the below program:

import threading

import os

 
#
def task1():
    #
    print("Task 1 assigned to thread: {}".format(threading.current_thread().name))
    #
    print("ID of process running task 1: {}".format(os.getpid()))

 
#
def task2():
    #
    print("Task 2 assigned to thread: {}".format(threading.current_thread().name))
    #
    print("ID of process running task 2: {}".format(os.getpid()))

 
#
if __name__ == "__main__":

 

    # 

    print("ID of process running main program: {}".format(os.getpid()))

 

    # 

    print("Main thread name: {}".format(threading.main_thread().name))

 

    # 

    t1 = threading.Thread(target=task1, name='t1')

    t2 = threading.Thread(target=task2, name='t2')   

 

    # 

    t1.start()

    t2.start()

```

## Lab 4
***Implementing Threads: Write a program that takes a person's pay of $15/hr to calculate a 40 hour work pay and then calculate the overtime pay. The variable pay = 0 and is where the final pay should be assigned.***


## Lab 5
***Take a Python Program you created and add Multithreading to it***

