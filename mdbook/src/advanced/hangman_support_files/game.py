import hangman
state = hangman.buildGrid(1, "g  ss", "guess") 
"""Builds the play space for the game Hangman.

    Parameters:
    wrongGuesses (int): 
        Number to indicate how many wrong guesses player has made.
    revealedAnswer (str): 
        Value used to show the letters the player has guessed correctly.
        Example: "g::ss" using : as a spacer
    actualAnswer (str): 
        The full answer.
        Example: "guess"

    Returns:
    str: Either END or CONTINUE will be returned.
"""
print(state)