# Hangman Exercise : Part I

**IMPORTANT: DO NOT READ PART II BEFORE COMPLETING PART I**  
You have been asked to develop the logic for a hangman game. Another developer has
been tasked to build a function that will automatically build the game grid for you.

You will be provided the hangman.pyc file, which will allow you to use the buildGrid() 
function. This function takes in a number and two strings.  
- The number will indicate how many wrong guesses the player has made
- The first string will consist of any letters that the player has correctly guessed 
- The second string will be the full answer.  

The function will return "END" when either wrongGuesses is maxed at 6 or 
if the player solves the puzzle. It will return "CONTINUE" if the game continues for 
another round of guesses.

Since you'll be developing the logic of the game, you'll need to know the rules.  
The host gives a word or phrase that the other player needs to solve. For this game, 
you can hardcode the puzzle, or allow for the host to input the puzzle via file or 
command line. You'll need to store the answer.  Once the puzzle has been saved, 
using the buildGrid() function from before, ask the player to a letter. If letter is 
contained within the answer, you should generate a string that contains the revealed 
letters. 

You will need to implement at least one function of your own (recommendation is to make
use of functions where appropriate).  Making a function to create the revealed Answer
would be a good start.

Note about the revealed answer string being passed to the buildGrid() function.  The 
function will compare the revealedAnswer string against the answer string at each 
position.  You will need to use some sort of spacer character for the letters that 
have not been revealed.

The docstring for the buildGrid() function has been provided within the game.py file. 
The game.py has been provided to allow for immediate testing of the .pyc file 
(run game.py to make sure that it works with your build).

## Requirements

* Adhere to [PEP8](https://www.python.org/dev/peps/pep-0008/)  (comments, formatting, style, etc)
* Make sure to include your student docstring at the top(name, lab, date)
* Follow instructions above
* Provided files located here: [game.py](hangman_support_files/game.py), [hangman.pyc](hangman_support_files/hangman.pyc)
---