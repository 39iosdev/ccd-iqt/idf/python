# Hangman Exercise : Part II

Unfortunately, we've lost the code that was in hangman.py and the developer has 
left the company, so we need you to reverse engineer a solution.  We think you can 
improve on the user interface.  

The game only allowed for a fix number of guesses, so maybe you can allow for that 
number to be changed.  The graphic used to change the stick man was a little basic, 
so perhaps that can be improved.  

At a minimum:
- The word HANGMAN needs to be displayed
- A basic stand with stick man needs to be displayed
- The stand needs to change as the wrong guesses increase 
- What the player has guessed of the answer needs to be displayed.  
- The game should exit if either the player wants to or the player reaches an end condition.

## Requirements

* Adhere to [PEP8](https://www.python.org/dev/peps/pep-0008/)  (comments, formatting, style, etc)
* Make sure to include your student docstring at the top(name, lab, date)
* Follow instructions above
* Provided files located here: [game.py](hangman_support_files/game.py), [hangman.pyc](hangman_support_files/hangman.pyc)
---