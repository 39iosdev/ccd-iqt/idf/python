## Lesson Objectives:
* **LO 1** Employ C-Types. (Proficiency Level: B)
    * **MSB 1.1** Describe the purpose of C-Types. (Proficiency Level: B)
    * **MSB 1.2** Differentiate between ctypes.CDLL and ctypes.PyDLL. (Proficiency Level: B)
    * **MSB 1.3** Describe the load library function. (Proficiency Level: B)
    * **MSB 1.4** Describe the of purpose of structures. (Proficiency Level: B)
* **LO 2** Employ the Regular Expression (re) library. (Proficiency Level: C)
    * **MSB 2.1** Describe regular expressions (Proficiency Level: B)
    * **MSB 2.2** Differentiate between Python re module methods. (Proficiency Level: B)
    * **MSB 2.3** Given a pattern, write the appropriate regular expression to match it. (Proficiency Level: C)
    * **MSB 2.4** Given a regular expression, predict the strings that will match. (Proficiency Level: C)
* **LO 3** Employ advanced Python libraries. (Proficiency Level: C)
    * **MSB 3.1** Describe the purpose of sys library. (Proficiency Level: B)
    * **MSB 3.2** Describe the purpose of os library. (Proficiency Level: B)
    * **MSB 3.3** Describe the purpose of subprocess library. (Proficiency Level: B)
    * **MSB 3.4** Given a scenario, select the best library to interact with the system. (Proficiency Level: C)
* **LO 4** Discuss the purpose of the hashlib library. (Proficiency Level: )
* **LO 5** Describe the purpose of the JSON library. (Proficiency Level: B)
* **LO 6** Describe the purpose of PIP (Python Package Installer). (Proficiency Level: B)
* **LO 7** Describe multi-threading. (Proficiency Level: B)
    * **MSB 7.1** Describe the purpose of multi-threading. (Proficiency Level: B)
    * **MSB 7.2** Describe the benefits of multi-threading. (Proficiency Level: B)
    * **MSB 7.3** Identify the Python library methods used for threading. (Proficiency Level: A)
    * **MSB 7.4** Identify how to lock a thread in Python (Proficiency Level: B)
* **LO 8** Discuss the purpose of unit testing.(Proficiency Level: )
* **LO 9** Discuss the unittest library.(Proficiency Level: )
* **LO 10** Discuss the doctest library. (Proficiency Level: )
* **LO 11** Understand metaclasses. (Proficiency Level: B)
    * **MSB 11.1** Describe the purpose of metaclasses. (Proficiency Level: B)
    * **MSB 11.2** Describe the benefits of metaclasses. (Proficiency Level: B)
    * **MSB 11.3** Describe the purpose of the super() method. (Proficiency Level: B)
    * **MSB 11.4** Describe the purpose of the new() method. (Proficiency Level: B)
* **LO 12** Describe the terms and fundamentals associated with object-oriented programming using Python. (Proficiency Level: B)
    * **MSB 12.1** Describe the behavior of a singleton design pattern. (Proficiency Level: A)
    * **MSB 12.2** Describe the behavior of an adapter design pattern. (Proficiency Level: A)
    * **MSB 12.3** Describe the behavior of a bridge design pattern. (Proficiency Level: A)
* **LO 13** Set up and replicate a Python Virtual Environment. (Proficiency Level: )
    * **MSB 13.1** Understand the purpose and benefits of Python Virtual Environments. (Proficiency Level: B)

## Performance Objectives (Proficiency Level: 3c)
* **Conditions:** Given access to (references, tools, etc.):
    * Access to specified remote virtual environment
    * Student Guide and Lab Guide
    * Student Notes


* **Performance/Behavior Tasks:**
    * Create a structure to pass to an imported CDLL.
    * Import a C library (.DLL/.SO) to utilize a C compiled function.
    * Use the Python 're' library to match a specified pattern.
    * Create a Python script that interacts with system processes.
    * Create a Python script that interacts with operating system functionality.
    * Create a Python script that interacts with system-level information.
    * Utilize the Thread library to implement a simple multithreaded program.
    * Design a unit test.
    * Implement a unit test.
    * Utilize unit test results to guide program debugging.
    * Given a set of requirements, implement a metaclass.
    * 
    * 


* **Standard(s)**
    * **Criteria:** Demonstration: Correctable to 100% in class
    * **Evaluation:** Students will have 4 hours to complete the timed evaluation consisting of both cognitive and performance components.
    * Minimum passing score is 80%