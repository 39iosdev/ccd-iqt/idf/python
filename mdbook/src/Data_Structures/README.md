## Intro to Data Structures in Python

### **Introduction:**
In this section, the students will learn to implement complex data structures using Python.

Recognizing more advanced data structures allow for more efficient storage, organization, and access of data. 

This section will introduce and review the use of more advanced data structures in Python. 
Recognizing more advanced methods for manipulating data will allow the creation of more effective and appropriately designed applications. 

### **Topics Covered:**


* [**List Pointer Structures**](Lists_Pointer_Structures.html)
* [**Linked Lists**](Singly_Linked_List.html)
* [**Stacks**](Stacks_Lesson.html)
* [**Queue**](Queue_Lesson.html)
* [**Trees**](Trees_Lessons.html)

#### To access the Data Structures' slides please click [here](slides/)
