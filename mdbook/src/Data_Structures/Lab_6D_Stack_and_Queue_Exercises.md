# Lab 6D Stack and Queue Exercises

### **Exercise 1**

Write a program that uses a stack to test input strings to determine whether they
are palindromes. A palindrome is a sequence of words that reads the same as the
sequence in reverse: for example, noon.

### **Exercise 2**

Use a stack to convert an integer to binary. Hint: use modulus

```text
134

134/2 = 67 -> 0
67/2 = 33  -> 1
33/2 = 16  -> 1
16/2 = 8   -> 0
8/2  = 4   -> 0
4/2  = 2   -> 0
2/2  = 1   -> 0
1/2  = 0   -> 1

134 = 10000110
```

Challenges:

### **Exercise 3**

Implement a stack using linked lists.

### **Exercise 4**

Implement a queue using linked lists.
