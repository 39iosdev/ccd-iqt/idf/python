# Lists and Pointer Structures

You may have been told that Python doesn't use arrays and that instead of arrays, Python uses lists. 

In the background of Python, lists are actually arrays.

You may have also been told that Python doesn't use pointers, really it’s the user that doesn't use pointers.
In the background, Python is using pointers.

If you remember, Python is built off of the C language, which uses pointers.  

Python, however, removes possible problems of memory allocation and misuse from the user by taking care of pointers in the background.

The Python lists we have been using are built off of C arrays and are very powerful and versatile but they have some disadvantages when it comes to memory usage.

We are going to be building lists without Python's built in list function.
This provides the benefit of using pointer like ability to create our own functionality based off search and sort algorithms.

We are going to do a quick ground up discussion of definitions to get us up to coding with a few explanations.

---

### Array vs List

**C Array**: We said in the background that Python is using the C array for creating lists.

An array stores data in memory sequentially.  This has some advantages and disadvantages.  

*Advantages*:

* Quick because data is stored in memory sequentially. 
* Items can also be accessed individually.

*Disadvantages*:  

* Array elements must all be of the same data type.
* If the arrays are large, then large sequences of memory must be used and large sequential memory addresses may not be available.
* C arrays have a fixed size and cannot grow and shrink dynamically as needed.  

**Python lists**: Python lists are C arrays in the background.

*Advantages*:

* List items do not need to be all of the same data type
* Items can be accessed individually
* Can grow and shrink dynamically

*Disadvantages*:

* When changes are made, a new array is made from a copy of the original array. This allows the lists to be dynamic, but sequential memory again must be used.

For both the array and the list, when you remove or add to the data, it requires movement of the items in memory.  This can be time consuming.

This is where pointers would be beneficial.

---

### What is a pointer?

Pointers are variables that hold the memory address of another variable.

Everything in Python is an object.

Those objects have memory addresses that your system uses to work with your code.

When you create an object in memory you continue to use it in other places throughout your code with the variables.

Using pointers means that we can refer to the address of the object to pass around between functions and do not require as much memory as would passing the whole object around between functions.

---

### So how does this relate to pointers and data structures?

Since Python doesn't use pointers, at least at the user level, and sometimes the need for a pointer is handy, especially when needing to link data that can’t be put into sequential memory due to possible space issues, we can see a need for creating our own pointer like structures.

We will be creating structures, starting with singly linked lists, that connect our memory addresses together with nodes, which we will explain more in-depth.

Linked Lists come in several different types.  We will be learning Singly linked lists, Doubly linked lists and Circularly linked lists as well as how to use what we have learned about algorithms to manipulate the Linked lists.
