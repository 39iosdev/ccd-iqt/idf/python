# Queue

---
**A special type of list is the queue data structure**

Queues are  a fundamental and important concept to grasp since many other data structures are built on them.
Queues are the same concept as standing in a line waiting to be served.

When people are standing in a queue waiting for their turn to be served, service is only rendered at the front of the queue.

To exit the queue they wait until they are served, which only occurs at the very front of the queue.

By strict definition, it is illegal for people to join the queue at the front where people are being served:

The acronym FIFO best explains this.

FIFO stands for first in, first out.

![image](Assets/60819122-a1828d80-a164-11e9-9ea0-14e59a50ab83.png)

The operation to add an element to a queue is enqueue.

The operation to remove an element from a queue is dequeue.

Anytime an element is enqueued, the length or size of the queue increases by one.

Conversely, dequeuing items reduce the number of elements in the queue by one.

To demonstrate the two operations, the following table shows the effect of adding and removing elements from a queue:

| Queue Operation | Size | Contents | Operation results |
| :---: | :---: | :---: | :---: |
|Queue()|0|[]|Queue object created|
|Enqueue "Mark" | 1 | ['mark'] | Mark added to queue |
|Enqueue "John" | 2 | ['mark','john'] | John added to queue |
|Size() | 2 | ['mark','john'] | Number of items in queue returned |
|Dequeue() | 1 | ['mark'] | John is dequeued and returned |
|Dequeue() | 0 | [] | Mark is dequeued and returned|

When visualizing a Queue it is easiest if you think of waiting in line at the bank with a "head" and "tail" on the line.
You enter the queue by entering the “tail” of the line.

Once you enter the bank line (queue) you wait, and as each person in front of you exits the line you get closer to exiting from the “head”.
Once you reach the end/head then you can exit.

A Queue is therefore similar to a DoubleLinkedList because you are working from both ends of the data structure.

**Note:** Many times you can find real-world examples of a data structure to help you visualize how it works.
You should take the time to draw these scenarios to help you better understand the concepts.
