# Doubly Linked List

**Python Doubly linked list.**

Doubly Linked List is a variation of the singly linked list.

One of the limitations of the singly linked list is that it can be traversed in only one direction, that is forward.

The doubly linked list has overcome this limitation by providing an additional pointer that points to the previous node. With the help of the previous pointer, the doubly linked list can be traversed in a backward direction thus making insertion and deletion operation easier to perform.

![](Assets/null.png)

The above picture represents a doubly linked list in which each node has two pointers.

* Previous/prev: points to the previous node 
* Next: points to the next node.

Here, node 1 represents the head of the list.

The previous pointer of the head node will always point to NULL.

Next pointer of node one will point to node 2.

Node 5 represents the tail of this list whose previous pointer will point to node 4. Node 5's next will point to NULL.

---

**Doubly linked node class:**

Define a Node class which represents a node in the doubly linked list. It will have three attributes:

* **data** - Will hold the data of the node
* **previous** -  Which will point to the previous node
* **next** - Which will point to the next node.

```python
class DoubleNode:
    def __init__(self, data, next = None, prev = None):
        self.data = data
        self.next = next
        self.prev = prev

```

Define another class for creating a doubly linked list, and it has two attributes: head and tail.

```python
class DoublyLinkedList:
    def __init__(self):
        self.head = None
        self.tail = None
```

Initially, head and tail will point to null.

The append() method will add a node to end of the list

```python
def append(self, data):
        new_node = DoubleNode(data)
        #two cases to consider
        #the doubly linked list empty
        if self.head is None:
            new_node.prev = None
            self.head = new_node
            self.tail = self.head
        #not an empty list
        else:
            self.tail.next = new_node
            new_node.prev = self.tail
            self.tail = self.tail.next

```

    Create a new node from the Double node class
    If the head is null(the list is empty)
        Set the New_node's previous to None
        Set the head equal to the new node
        Set the tail equal to the head
    Else the list isn't empty
        Set the tail's next to equal the new node.
        Set the new node's previous to equal the tail
        Set the tail to equal the tail's next

The print_linked_list() method will print all the nodes present in the list.

```python
     def print_linked_list(self):
        if(self.head == None):
            print("The list is empty")
        else:
            probe = self.head
            while probe != None:
                print(probe.data)
                probe = probe.next

```

    If the list is empty:
        Print out that the list is empty, 
    Else the list is not empty
    Create a variable named probe that is equal to the head.
    While the head is not None 
        Print the data of the probe
        Set the probe equal to the probes next to continue to traverse

---

**Program**

```python
#Represent a node of doubly linked list
class DoubleNode:
    def __init__(self, data, next = None, prev = None):
        self.data = data
        self.next = next
        self.prev = prev
    
# Represent the head and tail of the doubly linked list
class DoublyLinkedList:
    def __init__(self):
        self.head = None
        self.tail = None
        
    #add to the front of the doubly linked list
    def append(self, data):
        new_node = DoubleNode(data)
        #two cases to consider
        #the doubly linked list empty
        if self.head is None:
            #create the first node and links
            new_node.prev = None
            self.head = new_node
            self.tail = self.head
        #not an empty list
        else:
            #add the node and update the links
            self.tail.next = new_node
            new_node.prev = self.tail
            self.tail = self.tail.next
        
    def print_linked_list(self):
        if(self.head == None):
            print("The list is empty")
        else:
            probe = self.head
            while probe != None:
                print(probe.data)
                probe = probe.next
            
    
```

**Continue to Performance Lab 6C: Circular and Doubly Linked List**

