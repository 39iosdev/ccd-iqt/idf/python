<!-- This is a work in progress
Not for use until this comment is removed
 -->

# TRIE

Trie (pronounced **try**) is a special type of tree.
The word **trie** comes from the word re***trie***val.  
Tries are also known as digital trees and prefix trees, and are used in information retrieval by storing strings to support fast pattern and prefix matching.

<!-- where are trie's used?? -->

## What's so "special" about a trie?  

Consider a Binary Search Tree (BST).  The branching factor for a BST is **2** because each node in a BST can have up to **2** children.

In a trie, the maximum number of children that any node can have is equal to the number of symbols in the alphabet.

So, for English, a trie would have a branching factor of 26. Cyrillic languages would have a branching factor of 33, and Vietnamese would have a branching factor 29.

This structure allows for easy storage of words.

## Classes

### TrieNode

The class for a TrieNode follows the same basic format as the other Node classes we have worked with; the node will have ***data*** and ***references*** to other nodes.

* The data attribute will be called **endword** and will initially be set to **False**.
* The reference attributes will be contained in a list called **children**.  The list is the size of the alphabet, such that the first item in the list corresponds to the first letter in the alphabet, and so on.  Initially, all references are set to **None**.  
  So, for English, index **0** corresponds to **'A'**, and index **25** corresponds to **'Z'**.

```python
class TrieNode:
    def __init__(self):
        self.children = [None]*26
        self.endword = False
      
```

### Trie

The Trie class will create the root of the tree by calling the TrieNode class.  A trie must have at least a root node.

```python
class Trie:
    def __init__(self):
        self.root = TrieNode()
```

## Example

The following shows a trie with the words **APE**, **APES**, **APPLES**, **PIE** and **PIES**.

*Note: In the following trie the nodes set to **False** are omitted and nodes set to **True** are indicated with the letter **T**.

```text
             root
             /  \
            A    P
            |     \
            P      I
          /   \     \
         E/T   P     E/T
        /        \    \
       S/T        L    S/T
                   \
                    E/T
```

Notice that the nodes set to **True** are the end of a word.  Words can share a common prefix such as the words in the trie that start with **AP** (APE, APES, APPLE) and the words that start with the prefix **PI** (PIE and PIES).

## Building a Trie

### Insert

Start with a root node whose **endword** attribute is set **False**
and has a **children** attribute with 26 **None** references (0 - 25).

```python
Node         children           endword
______________________________________

root  -------------------------- False
```

Now insert the word **TOP**, one letter at a time.

Start by creating a new node for **T**. The root will now have references for all empty nodes except for the **T** element at index 19.

```python
Node         children           endword
______________________________________
root -------------------T------ False
                        |
   T -------------------------- False
```

The new **T** node will have 26 references just like the root.

The next letter in the word to build out is **O**.

So we will repeat the process.

```python
Node         children           endword
______________________________________
root -------------------T------ False
                        |
   T --------------O----------- False
                   |
   O -------------------------- False
```

We will then repeat the process for the last letter in the word which is **P**.

```python
Node         children           endword
______________________________________
root -------------------T------ False
                        |
   T --------------O----------- False
                   |
   O ---------------P---------- False
                    |
   P -------------------------- True
```

This is how the trie will look.

```python

     root/False
      |
      T/False
      |
      O/False
      |
      P/True
```

In the last node we set the **endword** attribute to **True** with the **children** attribute having 26 indices set to None.

The good thing about tries is that as they grow it is less work to add new words since the branches are already set.

### Search

To search our trie to determine if it contains a word, traverse the trie through each node corresponding with the letters in the word that you're searching for and check the **endword** attribute at the final node in the path. If it is **True** then the word is in the trie (also called a search hit).  If it is **False** then the word is not in the trie.  

Consider the word **TO** which we have *not* added to our trie thus far, but which is a prefix of the word **TOP**.

The search would traverse the path from the root to the **O** node.  The **endword** attribute for the **O** node is **False** so our algorithm would tell us that the word **TO** is not in the trie.  This is also known as a search miss.

To insert the word **TO** in this scenario, the **endword** attribute for the **O** node would need to be changed from **False** to **True**.

```python

     root/False
      |
      T/False
      |
      O/True
      |
      P/True
```

Consider searching for the word **TOPS**.

The search would traverse from the **root** to the **P** node.  The **children** attribute of the **P** node contains **None** at index 18 (the index for an **S**).  So the search would return **False**.

To insert the word **TOPS** the **children** attribute for the **P** node would be updated to contain a reference to a new node at index 18.  From there the **endword** attribute of the new node for the **S** would be set to **True**.

```python

     root/False
      |
      T/False
      |
      O/True
      |
      P/True
      |
      S/True
```

### Remove

Removing a word from a trie is more complex than inserting and searching.

Let's look at a three scenarios based off of the following trie that contains the words **APE**, **APES** and **APPLE**

```python
           root
            |     
            A      
            |       
            P       
          /   \      
         E/T   P    
        /        \     
       S/T        L    
                   \
                    E/T
```

#### Scenario 1 - Delete the word **APE**

* Traverse the path **A-P-E**.
* Set the **E** node's **endword** attribute to **False**.
* Check if the **E** node has any children.  
  In this case the **children** attribute has a reference to the letter **S** (meaning **APE** is a prefix to another word still in the trie).
* Because there **are** children of the **E** node, we can stop.
* We have successfully completed the task of removing the word **APE**.

The resulting trie looks like:

```python
           root
            |     
            A      
            |       
            P       
          /   \      
         E     P    
        /        \     
       S/T        L    
                   \
                    E/T
```

#### Scenario 2 - Delete the word **APES**

* Traverse the path **A-P-E-S**  .
* Set the **S** node's **endword** attribute to **False**.
* Check if the **S** node has any children.  
  In this case, there are none.
* Because there are no children, the **S** node has no reason to exist in the trie, and therefor should be deleted.
* Go up to the **E** node and remove the reference to the **S** node by setting the index in the **children** attribute for **S** to None.
* Now since the **E** node's **endword** attribute is **True**, we can stop.
* We have successfully completed the task of removing the word **APES**.

The resulting trie looks like:

```python
           root
            |     
            A      
            |       
            P       
          /   \      
         E/T   P    
                \     
                  L    
                   \
                    E/T
```

#### Scenario 3 - Delete the word **APPLE**

* Traverse the path **A-P-P-L-E**.
* Check if the **E** node has any children.  
  In this case, there are none.
* As in the previous example, move up to the **L** node and remove the reference to the **E** node.
* But this time, we are not done, because there are more nodes to be checked to make sure the word is removed properly.
* The **L** node's **endword** attribute is **False** and it has no children, so it must also be removed.
* Move up to the **P** node and remove the reference to the **L** node.
* Repeat the process until a node's **endword** attribute is **True** ***OR*** the node has children.
  * Move up to the second **P** node and remove the reference to the **P** node.
  * Stop because this second **P** node has children (the **E** for **APE** and **APES**).
* We have successfully completed the task of removing the word **APPLE**.

The resulting trie looks like:

```python
           root
            |     
            A      
            |       
            P       
           /       
          E/T      
         /            
        S/T                      
```

## Big O

The big O worst case run time for **creating** a trie is dependent on how many words the trie contains (n) and the average length of the words (m).
Thus the big O is O(n*m).

For inserting, searching, and deleting a single word from a trie, the big O is O(n).
