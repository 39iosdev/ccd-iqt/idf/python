# Why hash tables?

Let's say you are creating a phone book. To create the phone book you know that you want to eventually look up the number for a person based off of their name.
You could store the names and numbers in a list or an array. To retrieve the phone numbers you would have to iterate either through the whole list looking at each item to do a comparisons to find the phone number or you would have to know exactly at what index it was stored at.
If you have an extensive phone book then you can begin to see how time consuming it would be to do a comparison on every item or how difficult it would be to remember at what index a name was stored.

With the way we are using one value (a name) to look up another (a phone number) we can use a hash table to store the data and to make retrieval fast.

<!-- The insertions, look ups and deletions performed by iterating over every single item can end up with a worst case big O of O(n). -->

<!-- With the way we are using one value (a name) to look up another (a phone number) we can use a hash table to store the data. If we set up a hash table the big O worst case resolves to O(1) -->

<!-- ## How to reach a big O(1)?

In order for us to reach a big O(1) we will need a table of a constant size.  If it varies in size then it will be almost impossible to keep track of what information goes where. -->

<!-- We know that we will be searching for phone numbers by name.  In other words, we are using a key to search for a value.  In this instance the key is the name and the value is the phone number

We already know by working with lists and arrays that you can directly retrieve an element if you know it's index.  If we can manipulate the key so that it produces an index number we can easily place it in a table and retrieve it quickly as well.

In order for this to work we need to manipulate the key so that it's reproducible.  If we can't find the phone number after it's been placed in an index then creating an index number for it is no good.  The key must also fit within our table.  Our table is of a finite size so what ever key that is produced must fit the range of the table.  

Once we have produced a key we can then add it and it's value to the table.  This allows for faster searches, we can look at one area of the table to check for our key instead of searching the whole structure. -->

<!-- Essentially, with good planning, we can get our operations to end up with a worst case big O of O(1) -->

## Understanding Hash Tables

To understand the concept of a hash table it is easiest to go back to the earlier studies of dictionaries.

Dictionaries, in python, consist of key-value mappings.  In other programming languages the concept of dictionaries is sometimes called hashes or hashmaps.
Our hash tables will function much the same way as a dictionary does. This means that we will be working with key, value pairs.  

There are a few concepts to get to know: **hash function**, **hashing**, **hash**, and **hash table**.
<!-- 
**Hashing** is when we use algorithms and other techniques to produce a new key from an original key called a **hash**. The hash is a reproducible number that is used as an index in our table which is called a **hash table**. -->

The **hash function** is what contains the algorithms and techniques that performs work on input.
The input to a hash function is usually the key.  The inner algorithms will produce a **hash**.
The hash is a number of fixed length that is used as an index for the location to store the values in the **hash table**.
When the work is being performed on the input it is called **hashing**.  Hashing is just a verb to say that you used a function on some input to produce a result.

```code
    
    key: value     hash_function(key)     hash_table
    ________________________________________________
    key = name    key-->hashing-->hash    hash: value

```

<!-- Think of the hash function as a factory, the hashing as the work being done in the factory, the hash is the product of the work, and the hash table is the warehouse that stores the results. -->

Let's look at some of the nuances of working with hash functions and hash tables.

Let's continue with the phone book example.
To begin with we will need a table of a constant size to hold our phone numbers.  If the hash table varies in size then it will be almost impossible to keep track of what information goes where.  

We are using a key to search for a value.  In this instance the key is the name and the value is the phone number.

```python
    key = name
    value = phone number
```

Since our hash table is of a constant finite size, the hash that is used as the index must fit the range of the table.  If the table has a size of 1000 phone numbers and the hash is greater than 1000 then it will not work as an index in the hash table.

<!-- Once the value is placed in the hash table it must be able to be retrieved. -->

<!-- We know from working with lists and arrays that you can directly retrieve an element if you know it's index. We can also do this in our hash table by manipulating the key in our phone book so that it produces an index number/hash. Then we can easily place the value by index in the table.   -->
<!-- 
In order for this to work we need to manipulate the key so that the index produced is reproducible.  -->

Once the key value pairs are stored in the hash table we can search then for an item by putting the original key back through the hash function.  This means that our function must reproduce the same hash for the same key that is passed to it every single time, otherwise we cannot search and our algorithms are broken.

Hash functions should be one way.  Meaning that once an index is produced, you should not be able to figure out what the original data was.  If the original data is able to be reproduced then again, our algorithms are considered broken.  This becomes clearer when you realize that sometimes we want to store data that is sensitive.  Let's say instead of addresses and phone numbers we were storing credit card information or social security information.  We would not want our hash tables to be compromised because the algorithms were broken.

The general process, again with the phone book example, is that a key/name is sent into a hash function.  The hash function will produce a number called a hash.  That hash will be used as the index for the location to store the value/phone number within the hash table.

Let's look at an example of a hash function.  At this point we are just looking at a simple hash function and will work our way up to the full code.
**Code:**

```python

def my_hash(stryng):
    hashed_key = 0
    for ch in stryng:
        hashed_key += ord(ch)
    
    return hashed_key
    
    
for item in ('hello world', 'python'):
    print("{}:{}".format(item, my_hash(item)))
```

**Output:**

```code
    hello world:1116
    python:674
```

The above hash function is taking in a string and will return a hash.  To do this the hash function is simply iterating over the string and summing the ordinal value of each character.

**hello world**:

```code

chars    ->  h  |  e  |  l  |  l  |  o  |    |  w  |   o |  r  |  l  |  d
ordinals -> 104 + 101 + 108 + 108 + 111 + 32 + 119 + 111 + 114 + 108 + 100 = 116

```

The return values (hash) are **1116** for the string **'hello world'** and **674** for the string **'python'**.

However, if we send in the strings **'hello world'** and **'world hello'** they will both sum to the same value and both return **1116**.

This is where we begin our talk on collision.

## Collision

Collision is when hashed keys share the same position in the table.  A perfect hash function would make sure that this does not happen, however the time spent creating a perfect hash function would likely mean that our function would itself not have a very feasible worst case big O and would make the whole purpose of having a fast structure moot.
Thankfully there are ways to work with collisions.

We are going to adjust the above code and add a multiplier that will increase as it multiplies each element of the ordinal values.

**Code:**

```python
def my_hash(stryng):
    multiplier = 1
    hashed_key = 0
    for ch in stryng:
        hashed_key += multiplier * ord(ch)
        multiplier += 1
    return hashed_key
    
    
for item in ('hello world', 'world hello'):
    print("{}:{}".format(item, my_hash(item)))
```

**Output:**

```code
hello world:6736
world hello:6616
```

Now it looks as if the code will return different results for the two different strings, but if we replace the strings with **'ad'** and **'ga'** we will again receive two strings that return the same hashed key.

**Code:**

```python
def my_hash(stryng):
    multiplier = 1
    hashed_key = 0
    for ch in stryng:
        hashed_key += multiplier * ord(ch)
        multiplier += 1
    return hashed_key
    
    
for item in ('ad', 'ga'):
    print("{}:{}".format(item, myhash(item)))
```

**Output:**

```code
ad:297
ga:297
```

We've now determined that our function is pretty good but it can't be completely perfect.  We need other methods to handle collisions.

Let's look at what is happening in the hash table so we can discuss how to work with collisions.

Each item, in the form of a key, value pair, is stored in a slot/bucket/position/index produced by the hash function.  We will use the term bucket for the rest of the discussion.

Below is an image to show the process

```code
input          hash function   hash with value  
______________________________________________
hello world    6736%25=11      11 : hello world
python         2333%25=8        8 : python
world hello    6616%25=16      16 : world hello

```

```code
hash table
______________________________________________________
    8 : python | 11 : hello world | 16 : world hello
```

For this image we are inputting three strings into the hash function to come up with a hash.  The hash is used to determine the bucket to place the key value pair.

We can now use the function to do our insertion and searching and deleting.
This brings us back to collision, what do we do with the instances that have the same bucket.

There are two main categories to handle collision; open addressing and chaining.

**Open addressing**
Open addressing means we are going to find an open bucket in our hash table to place the key value pair.
One of the ways to do this is the idea of **linear probing**.

**Linear probing**: if the bucket is full then the code looks for the next available bucket by iterating over each position until an empty one is found.

The downside to linear probing is that if the hash table begins to fill up then the time to insert or search increases.  

There are other methods for open addressing
such as **quadratic probing** and **double hashing**.

In **quadratic probing** the i^2 position in the i'th iteration is looked for to handle collisions.

**Example:**

```code
    let hash(x) be the bucket index computed using hash function.  
    If bucket hash(x) % S is full, then we try (hash(x) + 1*1) % S
    If (hash(x) + 1*1) % S is also full, then we try (hash(x) + 2*2) % S
    If (hash(x) + 2*2) % S is also full, then we try (hash(x) + 3*3) % S
```

In **double hashing** another hash function is used in the event of a collision. This creates more widely separated indices instead of the items being clustered around a primary index.  

The disadvantage of this is there is poor cache performance and computation time.

**Example:**

```code
 let hash(x) be the slot index computed using hash function.  
    If slot hash(x) % S is full, then we try (hash(x) + 1*hash2(x)) % S
    If (hash(x) + 1*hash2(x)) % S is also full, then we try (hash(x) + 2*hash2(x)) % S
    If (hash(x) + 2*hash2(x)) % S is also full, then we try (hash(x) + 3*hash2(x)) % S
```

One of the challenges with probing is **clustering**.  There are two categories of clustering; **primary clustering** and **secondary clustering**.

* Primary Clustering
    Many consecutive elements form groups around an index and it begins to take time to find a free position to search.  The bigger the cluster, the faster it grows which then begins to impact performance.  This is very common with linear probing.

* Secondary Clustering
    Long runs of filled slots occur away from the hashed position.  This is common with quadratic probing, however it is less severe in terms of performance than primary clustering.

One of the ways to handle collisions is **chaining**
**Chaining** allows a single bucket to hold many items.
To accomplish this the buckets are initialized with empty lists.  When an item is added to the index it is appended to the list.
There is no limit on the number of items to store in a hash table with this method.

The downside is that it does become inefficient when a list grows at a particular index.
The retrieval becomes slow because the list must now be iterated through to find the item.
There are ways to remedy this.  One way is to create a binary search tree at a bucket instead of a list, however a binary search tree could end up being just as slow unless the time is taken to make sure that the tree is self balancing.  

Another way would be to have a criteria set up where the whole table would be **re-hashed**, meaning the size of the table would be increased at a specified threshold and all the items in the table would be put through the hash function again to be placed in a new location, hence the term "re-hash".  

Growing the hash table or re-hashing means that we need to compare the size of the table with the count of how many buckets are actually used in the table.
This is called the **load factor** and is determined by a simple fraction.
Which is the total number of buckets divided by the number of buckets actually used.

```code
              n #number of buckets used
load_factor = -
              k #total buckets
```

The load factor gives us a way to determine when to re-hash our table.
There are things to consider when determining when to grow.  You must decide at what point should the table grow and to what size.

One way to do this would be set the load factor to be .75.  When the threshold is reached then the table will be doubled by the current size and then all the elements would need to be re-hashed to their new location by traversing the old structure and inserting into the new table.

## Code Example

Let's look at an example of building a hash table with linear probing.

Like our other data structures we will be using classes.
This example uses two classes.

One for the items that are hashed **"class HashItem"**
and one for the hash table **"class HashTable"**.

```python
class HashItem:
    def __init__(self, key, value):
        self.key = key
        self.value = value
        
class HashTable:
    def __init__(self):
        self.size = 25
        self.bucket = [None for i in range (self.size)]
        self.count = 0
        
    def _hash(self, key):
        multiplier = 1
        hashed_key = 0
        for ch in key:
            hashed_key += multiplier * ord(ch)
            multiplier += 1
        return hashed_key % self.size
        
    def put(self, key, value):
        item = HashItem(key,value)
        h = self._hash(key)
        while self.bucket[h] is not None:
            if self.bucket[h].key is key:
                break
            h = (h+1)%self.size
        if self.bucket[h] is None:
            self.count += 1
        self.bucket[h] = item
    
    def get(self, key):
        h = self._hash(key)
        while self.bucket[h] is not None:
            if self.bucket[h].key is key:
                return self.bucket[h].value
            h = (h + 1)% self.size
        return None

#driver code

ht = HashTable()
ht.put("The car", "Ecto-1")
ht.put("The destructor", "Stay Puff Marshmallow man")
ht.put("The gatekeeper", "Dana Barrett")
ht.put("To test", "ad")
ht.put("Another test", "ga")
ht.put("The keymaster", "Louis Tully")

for key in ("The car", "The destructor", "The gatekeeper", "The god", "To test", "Another test"):
    v = ht.get(key)
    print(v)
    
print("The number of elements is:{}".format(ht.count))
```

In the above code the class HashItem creates two attributes; the key and the value.  

The class HashTable creates three attributes which are the size of the hash table (self.size), a list that is the length of the specified size (self.bucket) and a variable to keep track of how many items are in the hash table (self.count)

The HashTable class is made up of three functions.  A protected function that performs the hashing **_hash**, a **put** function that inserts into the hash table, and a **get** function that retrieves from the hash table.

The **_hash** function takes in the key and returns a hashed value that fits the size of the table by using the modulus operator.

The **put** function uses the **_hash** function to get the new key/hash for the hash table.  It checks to see if the bucket is empty, if it is it will insert it and if it isn't it will apply the open addressing technique of linear probing to check for another empty bucket by adding one to the sum of the ordinal values and then divide by the size of the hash table to create a new key.  It will continue to look until an empty bucket is found.

The **get** function is used to retrieve elements from the hash table.  It takes in the key and sends it through the **_hash** function. Then the function follows the same logic as the put function to find the matching key/hash within the hash table.

We talked above about it being important that the hash is reproducible.
The term for this is **deterministic**.  This means that the same key passed into the hash function will always produce the same hash. When our functions are deterministic we can go to the exact location within our hash table to add and retrieve data.  The hash function is basically creating an index based off of the key being passed to it.

In the event of open addressing, we still only need to search a few areas.  If the bucket was using the concept of chaining then we would only need to iterate over the list at one index.

Now that we've broken down the basics of how a hash table is created and why, there are three characteristics to remember that define a hash table.

* Fast computation
    This is accomplished by creating key and searching at a specified area of the table.  A good hash function will produce a worst case big O of O(1) for insertion, searching and delete operations.

* Deterministic
    The same string will produce the same hash every time.
    This is extremely important in order for our hash table to be easily searched.

* Fixed length
    All hash results will have the same length.
    (In our above code the hash length is the size of an int)

### Uses for hash functions
Hash functions/one way functions are often used for data integrity and authentication.  There are many different kinds of hash functions in use.

* MD5 (Message Digest Algorithm)
* SHA-1 (Secure Hash Algorithm 1)
* SHA-2 (Secure Hash Algorithm 2)
* NTLM (Windows Network New Technology LAN Manager)

However, it is important to know that ***not all hashing algorithms are secure***.
 Some have been compromised over time and are no longer recommended to be used. An example of this is the MD5 algorithm.  MD5 at one time was used for authenticating digital signatures but has been deprecated due to the algorithm being exploited and producing collisions.
MD5 is still sometimes used as a non-cryptographic checksum for data integrity and data corruption detection.
