# Lab 6C Circular and Doubly Linked List

---
*Note: Please use input validation in your code

### **Exercise 1**

Build out the Circularly and Doubly linked lists classes functionality by adding the following functions:
* **prepend**: Adds the new node to the beginning of the list
* **insert**: Inserts a new node 
* **delete**: Delete an existing node
* **print**: Prints all nodes

### **Exercise 2**

Implement a **reverse** method to the doubly linked list class

### **Exercise 4**

Create a new file and modify your code to have DoublyLinkedList inherit from your SinglyLinkedList class.

### **Exercise 5**

Define a function **makeDoubly** that expects a singly linked structure as its argument. The function builds and returns a doubly linked structure that contains the items in the singly linked structure.