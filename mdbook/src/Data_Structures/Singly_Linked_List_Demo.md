# Singly linked lists

A singly linked list is a list with only one pointer that points to the address of the next node between two successive nodes. It can only be traversed in a single direction, that is, you can go from the first node in the list to the last node, but you cannot move from the last node to the first node.

We can actually use the node class that we created earlier to implement a very simple singly linked list:

```python
>>> n1 = Node('D1')
>>> n2 = Node('D2')
>>> n3 = Node('D3')
```

Next we link the nodes together so that they form a chain:

```python
>>> n1.next = n2
>>> n2.next = n3
```

To traverse the list, you could do something like the following. We start by setting the variable current to the first item in the list:

```python
current = n1
while current:
    print(current.data)
    current = current.next 
```

In the while loop we print out the current element, after we set current to equal the next item in the list. We keep doing this until we have reached the end of the list.

There are, however, several problems with this simplistic list implementation:

* It requires too much manual work by the programmer.
* It is too error-prone (this is a consequence of the first point).
* Too much of the inner workings of the list is exposed.

We are going to address all these issues in the following sections.

---

**Singly linked list class**

A list is clearly a separate concept from a node. So we start by creating a very simple class to hold our list. 

We will start with a constructor that holds a reference to the very first node in the list. Since this list is initially empty, we will start by setting this reference to None:

```python
class SinglyLinkedList:
    def __init__(self):
        self.head = None 
```

**Append operation**

The first operation that we need to perform is to append items to the list. This operation is sometimes called an insert operation or add. Here we get a chance to hide away the Node class. The user of our list class should really never have to interact with Node objects. These are purely for internal use.

A first shot at an append() method may look like this:

```python
class Node:
    def __init__(self, data, next = None):
        #instantiate a Node with a default next of none
        self.data = data
        self.next = next

class SinglyLinkedList:
    def __init__(self):
            self.head = None

    def append(self, data):
        # Encapsulate the data in a Node
        new_node = Node(data)
        #is the linked list empty?
        if self.head is None:
            self.head = new_node
        #list is not empty so find the end and add to it
        else:
            probe = self.head  #sometimes probe is also called current
            #check if probe is at the end if not then until it is none
            while probe.next != None:
                probe = probe.next
            probe.next = new_node      
```

We encapsulate data in a node, so that it now has the next pointer attribute. 

From here we check if there are any existing nodes in the list (that is, does self.head point to a Node). 
If there is none, we make the new node the first node of the list, 
otherwise, find the insertion point by traversing the list to the last node then
updating the next pointer of the last node to the new node.

Take note of the convention being used. 
The point at which we append new nodes is through self.head but not at self.head. We will traverse from the head to the end. If looking from left to right the head starts on the left as if you were reading in English:
    
```text
        D1|next  ->  D2|next  ->  D3|next  ->  D4|next  ->  D5|next  ->  D6|None
        ^                                                                 
       head                                                              
```

We can append a few items:

```python
>>> numberList = SinglyLinkedList()
>>> numberList.append('D1') 
>>> numberList.append('D2')
>>> numberList.append('D3')
>>> numberList.append('D4')
>>> numberList.append('D5')
>>> numberList.append('D6')
```

List traversal will work more or less like before. You will get the first element of the list from the list itself by assigning probe to the head of the list:

```python
probe = numberList.head
while probe:
    print(probe.data) 
    probe = probe.next
```

**A faster append operation**

There is a big problem with the append method in the previous section.

It has to traverse the entire list to find the insertion point at the end of the list.

This may not be a problem when there are just a few items in the list, but wait until you need to add thousands of items.

Each append will be slightly slower than the previous one.

A **O(n)** goes to prove how slow our current implementation of the append method will actually be.

To fix this we can add another attribute to the SinglyLinkedList class, we will store, not only a reference to the first node in the list, but also a reference to the last node by adding a tail attribute.

That way, we can quickly append a new node at the end of the list.

The worst case running time of the append operation is now reduced from **O(n)** to **O(1)**.

All we have to do is make sure the previous last node points to the new node that is about to be appended to the list. Here is our updated code:

```python
class SinglyLinkedList:
    def __init__(self):
        #instantiate a head and tail Node with a default of none
        self.head = None #keeps track of the beginning of the list
        self.tail = None #keeps track of the end of the list

    #add to the end of the linked list
    def append(self, data):
        #instantiate a new Node
        new_node = Node(data)
        #if the list is not empty, add the node with the new links
        if self.tail != None: 
            self.tail.next = new_node
            self.tail = new_node
        #the list is empty, instantiate the head and tail to the new node
        else:
            self.head = new_node
            self.tail = new_node  
```

Take note of the convention being used. The point at which we append new nodes in this instance is through self.tail. If looking from left to right the head starts on the left and ends at the tail on the right.  The list would look like:
    
```text    
        D1|next  ->  D2|next  ->  D3|next  ->  D4|next  ->  D5|next  ->  D6|None
        ^                                                                 ^
       head                                                              tail
```

The head node will point to D1 and the tail node will point to D6.  

Note that the list is not sequential in memory.

Also the direction of the list is determined by where you want your tail and head to be.  

The names can be reversed if you want to think of them going the other direction.  

Some sources refer to the head as anything that is added newly to the list and the tail as the first item in the list.

We will continue with the method that is already established where the head is the first item added to the list and tail is the last item added to the list.

**Getting the size of the list**

We would like to be able to get the size of the list by counting the number of nodes. One way we could do this is by traversing the entire list and increasing a counter as we go along:

```python

def size(self):
    count = 0
    probe = self.head
    while probe:
        count += 1
        probe = probe.next
    return count 
```
This works, but list traversal is potentially an expensive operation that we should avoid if possible.

So instead, we shall opt for another rewrite.

We add a size member to the SinglyLinkedList class, initializing it to 0 in the constructor.

Then we increment size by one in the append method and any other methods that can influence the size of our list such as a deletion method.

```python

class SinglyLinkedList:
    def __init__(self):
        # ...
        self.size = 0

    def append(self, data):
        # ...
        self.size += 1 
```

Because we are now only reading the size attribute of the node object, and not using a loop to count the number of nodes in the list, we get to reduce the worst case running time of that algorithm from **O(n)** to **O(1)**.

Now we can easily keep track of the size of our singly linked list and we know how the append method adds to the end of our singly linked list.

---
What if we want to add to the beginning or "prepend" the list?

To add to the beginning of the list we don't need to traverse anything.
We will create new links with the head and the new node then add in the simple code to increase the size attribute as follows:

```python
def prepend(self, data):
    #instantiate a new Node object
    new_node = Node(data)
    #adjust the link by making new_node's next equal to the head
    new_node.next = self.head
    #Now make the head the new node
    self.head = new_node
    self.size += 1 #added code to keep track of size

```

Now that an attribute is made, we can go ahead and create a function to get the size if we need it other than using self.size

```python
#get the size of the list
def size_of_list(self):
    return self.size
```

---

**Improving list traversal**

If you notice when we traverse our list that one place is still exposed to the node class.

We need to use node.data to get the contents of the node and node.next to get the next node.
However, we mentioned earlier that client code should never need to interact with Node objects.

We can achieve this by creating a method that returns a generator.

Remember that a generator is a function that produces a sequence of values to iterate through rather than creating a single value.  

Yield is similar to return except that it produces a sequence of values.  

If Yield is in the body of the function then it is a generator.

Yield preserves the local state to return the generator object. It looks as follows:

```python

def iter(self):
    probe = self.head
    while probe:
        val = probe.data
        probe = probe.next
        yield val
 ```

Now list traversal is much simpler and looks a lot better as well.

We can completely ignore the fact that there is anything called a Node outside of the list:

```python
  
for numD in linked_list.iter():
    print(numD) 
 ```

Notice that since the iter() method yields the data member of the node, our client code doesn't need to worry about that at all.

Now that we have demonstrated how to traverse a list we can write a print method.
One way to do this is using the while loop as we did before in our initial traversal.

**Printing the list**

```python
def print_singly_linked_list(self):
    probe = self.head
    while probe != None:
        print(probe.data)
        probe = probe.next
```

Now that we have created the iter function that does generation we could re-write the print function as well:

```python
def print_singly_linked_list(self):
    #print calling the iter function
    for numD in self.iter():
        print(numD, end = ' ')
```

---
What happens if we want to add to our singly linked list at a specific location?

We can still do this.  

We will need to know the index and have the data that is to be inserted at the location.

From there we will reset the links to the nodes.

```python
#inserting a node within the linked list
def insert_node(self, index, data):
    #is the head empty or index less than or equal to 0?
    if self.head is None or index <= 0:
        #create the head with a new node
        self.head = Node(data, self.head)
        self.size += 1
    #find the position to insert
    else:
        #start at the head and continue to the end decrementing the index, when index reaches 0 then insert
        probe = self.head
        while index > 1 and probe.next != None:
            probe = probe.next
            index -= 1
        #insert new node after node at position index -1 or last position
        probe.next = Node(data, probe.next)
        self.size += 1
```

---

**Deleting nodes**

Another common operation that you would need to be able to do on a list is to delete nodes. This may seem simple, but we'd first have to decide how to select a node for deletion. Is it going to be by an index number or by the data the node contains? Here we will choose to delete a node by it's index.

The following is a figure of deleting a node from the list:

![](Assets/linkedlistdemo.png)

When we want to delete a node that is between two other nodes, we declare a variable named previous (prev) and make the node directly connected to the successor of the node that is being deleted the previous nodes next node.

That is, we simply cut the node to be deleted out of the chain as in the preceding image.

Here is the implementation of the delete() method may look like:

```python

def delete_node_by_index(self, index):
    #is this the only node or the list empty?
    if index <= 0 or self.head.next is None:
        removed_item = self.head.data
        self.head = self.head.next
        self.size -= 1
        return removed_item
        
    else: #the list isn't empty
        probe = self.head
        while index > 1 and probe.next.next != None:
            probe = probe.next
            index -= 1
        removed_item = probe.next.data
        probe.next = probe.next.next
        self.size -= 1
        return removed_item
```

 It should take a **O(n)** to delete a node.

**List search**

We also need a way to check whether a list contains an item. This method is fairly easy to implement thanks to the iter() method we previously wrote. Each pass of the loop compares the current data to the data being searched for. If a match is found, True is returned, or else False is returned:

```python

def search(self, data):
    for node in self.iter():
        if data == node:
            return True
    return False  
```

---

**Clearing a list**

We may want a quick way to clear a list. 

Fortunately for us, this is very simple. 

All we do is clear the pointers head and tail by setting them to None
Then set the size back to 0:

```python
def clear(self): 
    """ Clear the entire list. """ 
    self.tail = None 
    self.head = None 
    self.size = 0
```

In one fell swoop, we orphan all the nodes at the tail and head pointers of the list. This has a ripple effect of orphaning all the nodes in between.

Only do this if you are absolutely sure you want to clear all of the list

---
*Note: These functions can be difficult to follow, it is a good idea to draw them out if you need help understanding what your code is going to do.

**Continue to Performance Lab: 6B Singly Linked List**
