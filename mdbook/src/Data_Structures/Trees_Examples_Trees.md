# Types of trees

## General vs Binary

**General Tree** - A general tree is a tree where each node can have either zero or many child nodes. There is no limitation on the degree of nodes. There are many subtrees in a general tree and they are unordered because the nodes of a general tree can not be ordered by any specific criteria. It cannot be empty.

**Binary Tree** - A binary tree is a specialized tree in which a parent node can have a maximum of 2 child nodes which are left child and right child.
There are mainly two subtrees (right and left). Nodes can be ordered because of specific criteria.  A binary tree can be empty

![](Assets/general_vs_binary.png)

# Binary Tree Types

**Perfectly Balanced Binary Tree** - Includes a complete complement of nodes at each level but the last one. Complete and Full binary are special cases of perfectly balanced 

**Full Binary Tree** - A full binary tree (referred to as a proper or plane binary tree) is a tree where every node except the leaves have 2 children  
![](Assets/full_binary_trees.png)


**Complete Binary Tree** - Every level, except possibly the last, is completely filled and all nodes in the last level are as far left as possible

![](Assets/complete_binary_trees.png)


**Balanced Binary Tree** - The left and right subtrees of every node differ in height by no more than 1.  

![](Assets/balanced_binary_trees.png)


**Perfect Binary Tree** - All internal nodes have two children and all leaf nodes are at the same level. A perfect binary tree of height h has 2^h - 1 nodes.

![](Assets/perfect_binary_trees.png)         


**Degenerate Binary Tree** - Also called an unbalanced or pathological binary tree. Every internal node has one child.

![](Assets/degenerate_binary_trees.png)


# Building trees


The process of assembling a tree is similar to the process of assembling a linked list. 
Each constructor invocation builds a single node.

```python

class Tree:
    def _init_(self, data, left=None, right=None):
        self.data = data
        self.left = left
        self.right = right
        
```

The data can be any type, but the left and right parameters should be tree nodes. Left and right are optional; the default value is None.

One way to build a tree is from the bottom up.
Allocating the child nodes first
```python
left = Tree(2)
right = Tree(3)
```
Then create the parent node and link it to the children:

```python
tree = Tree(1, left, right)

```
We can write this code more concisely by nesting constructor invocations:

```python
tree = Tree(1, Tree(2), Tree(3))

```

# Traversing trees

The most natural way to traverse a tree is recursively.

The following code shows a function named total that returns the sum of all the nodes by using recursion.

```python

def total(tree):
    if tree == None: 
        return 0
    return total(tree.left) + total(tree.right) + tree.data

```
The base case is the empty tree, which contains no data, so the sum is 0. 

The recursive step makes two recursive calls to find the sum of the child trees. When the recursive calls complete, we add the data of the parent and return the total. 

There are different of ways to traverse a tree.

Depth first which consists of 3 different techniques(Pre Order, Post Order, In Order), and Breadth first(Level Order).


## Depth First Traversals:

**Pre Order Traversal** - (root  -> left  -> right )

```python

"""
Check if the current node is empty, if not empty continue.

Display the data part of the root or current node.

Traverse the left subtree by recursively calling preorder.

Traverse the right subtree by recursively calling preorder.
"""

```


![](Assets/Preorder_Traversal.png)

&nbsp;

**Post Order Traversal** - (left ->  right -> root ) 

```python
"""
Check if the root node is empty, if not empty continue.

Traverse the left subtree by recursively calling postorder.

Traverse the right subtree by recursively calling postorder.

Display the data part of the root.
"""

```


![](Assets/Postorder_Traversal.png)


**In Order Traversal** - (left ->  root -> right )

```python
"""
Check if the root is node is empty, if not empty continue.

Traverse the left subtree by recursively calling inorder.

Display the data part of the root.

Traverse the right subtree by recursively calling inorder.
"""

```

![](Assets/Inorder_Traversal.png)

## Breadth First Traversals:

**Level Order Traversal** - Visits each node at each level in left-to-right order



# Expression trees and traversal

A type of tree that demonstrates the types of traversals is an Expression tree.
Expression trees are used inside compilers to parse, optimize and translate programs.

An expression tree is a way to represent the structure of an expression.

It can represent the computations unambiguously.

The nodes of an expression tree can be operands or operators.  

Operands are leaf nodes

Operator nodes contain references to their operands 

All of the operators are binary, meaning they have exactly two operands.

For example, the infix expression 1 + 2 * 3 is ambiguous unless we know that the multiplication happens before the addition.  

To do this we use different notations to represent how to proceed.

### Infix - Postfix - Prefix
Infix, postfix and prefix are notations that are used for describing how to write expressions.
Understanding these notations will help in building an expressions tree

**Infix**

    X + Y
    Operators are written in between operands
    
**Postfix**

    X Y +
    Operators are written after their operands

**Prefix**

    + X Y
    Operators are written before their operands



Building the Expression tree for: 1 + 2 * 3

```python
tree = Tree('+', Tree(1), Tree('*', Tree(2), Tree(3)))
```
The order of operations on this tree build:

The multiplication happens first which then becomes the second operand for the addition.
 
 2 * 3 = 6

 1 + 6 = 7




**Traversing an expression Tree to print**

Prefix - In this format the operators appear before their operand

PreOrder traversal

This print statement prints the contents of the root, then prints the entire left subtree and then the entire right subtree.  The contents of the root appear before the contents of the children.

```python
def print_tree_preorder(tree):
    if tree == None: return 0
    #data -> left -> right
    print(tree.data)
    print_tree_preorder(tree.left)
    print_tree_preorder(tree.right)


```
The Prefix output
```python
>>> tree = Tree('+', Tree(1), Tree('*', Tree(2), Tree(3)))
>>> print_tree_preorder(tree)
+ 1 * 2 3

```
---

Postfix - In this format the operands appear before the operators

Post Order traversal

Print the subtrees first and then the root

```python
def print_tree_postorder(tree):
    # left -> right -> data
    if tree == None: return 0
    print_tree_postorder(tree.left)
    print_tree_postorder(tree.right)
    print(tree.data)

```
The Postfix output

```python
    >>> print_tree_Postorder(tree)
    >>> 1 2 3 * +
```

---

Infix - In this format the operators appear between the operators.
Sometimes when writing an expression in infix you have to use parentheses to preserve the order of operations.  So in order traversal is not quite sufficient to generate an infix expression

In Order Traversal

Print the left tree, then the root and then the right tree

```python
def print_tree_inorder(tree):
    # left -> data -> right
    if tree == None: return 0
    print_tree_inorder(tree.left)
    print(tree.data)
    print_tree_inorder(tree.right)

```
The Infix output

```python
    >>> print_tree_inorder(tree)
    >>> 1 + 2 * 3 
```
---

An inorder traversal to keep track of the tree levels to print a graphical representation of the tree

```python
def print_tree_indented(tree, level=0):
    if tree == None: return 0
    print_tree_indented(tree.right, level+1)
    print '  ' * level + str(tree.data)
    print_tree_indented(tree.left, level+1)

```
The parameter level keeps track of where we are in the tree. By default, it is initially 0. Each time we make a recursive call, we pass level+1 because the child’s level is always one greater than the parent’s. Each item is indented by two spaces per level. The result for the example tree is:

```text
      3
  *
    2
+
  1

```

# Binary Search Trees - BST

A Binary Search tree is a special binary tree where: 

The left child of a node has a value less than the parent

The right child has a value greater than the parent.

```python
"""
        3
       /
      2
     /
    1
"""
```

#### Searching 
When searching for element 1, we have to traverse all elements (in order 3, 2, 1). Therefore, searching in binary search tree has worst case complexity of O(n). In general, time complexity is O(h) where h is height of BST.

#### Insertion
For inserting element 0, it must be inserted as left child of 1. Therefore, we need to traverse all elements (in order 3, 2, 1) to insert 0 which has worst case complexity of O(n). In general, time complexity is O(h).

#### Deletion
For deletion of element 1, we have to traverse all elements to find 1 (in order 3, 2, 1). Therefore, deletion in binary tree has worst case complexity of O(n). In general, time complexity is O(h).

**Deleting from a BST**

Three cases to think about:

Case 1: Node to be deleted is a leaf node (No child). Directly delete the node from the tree.

Case 2: Node to be deleted is an internal node with one child. Copy the child to the node then delete the child

Case 3: Node to be deleted is an internal node with two children

Case 3 Two methods:
Method 1 - Find the minimum of the right side, copy and delete
Method 2 - Find the maximum of the left side, copy and delete




### Completely Sorted Binary Search Tree -AVL
    AVL = Named after Gregory Adelson-Velsky and Evgenii Landis

```python
"""
            5
           / \
          4   7
         /     \
        1       9
"""
```

#### Searching

For searching element 1, we have to traverse elements (in order 5, 4, 1) = 3 = log2n. Therefore, searching in AVL tree has worst case complexity of O(log2n).

#### Insertion

For inserting element 12, it must be inserted as right child of 9. Therefore, we need to traverse elements (in order 5, 7, 9) to insert 12 which has worst case complexity of O(log2n).

#### Deletion

For deletion of element 9, we have to traverse elements to find 9 (in order 5, 7, 9). Therefore, deletion in binary tree has worst case complexity of O(log2n)

