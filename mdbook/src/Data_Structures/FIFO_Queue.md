# FIFO Queue
The python Queue class implements a basic first-in, first-out  linear collection.

Elements are added to one end and removed from the other.

Queues are implemented in many ways, some are based on arrays, and some are based on linked structures.

* rear - Where additions are made to the queue
* front - Where removal is made to the queue

Queues have two fundamental operations:

* enqueue - adds to the rear
* dequeue - removes from the front

There is also a peek operation that can be employed as in a stack

* peek - see the item at the front of the queue

---
**Real World Example**

Priority queues:
Queues that schedule their elements using a rating scheme as well as FIFO
Example: Dijkstra's shortest path algorithm used in networking routing calculations.

* If two elements have the same rating then they are scheduled in FIFO order.
* Elements are ranked from largest to smallest according to some attribute.  
* Generally the smallest are removed first no matter when they are added to the queue.

---

**Doubly Linked list Node and Queue class**

```python
class Node(object):
    """ A Doubly-linked lists' node. """
    def __init__(self, data=None, next=None, prev=None):
        self.data = data
        self.next = next
        self.prev = prev


class Queue(object):
    """ A doubly-linked list. """
    def __init__(self):
        self.head = None
        self.tail = None
        self.count = 0

    def enqueue(self, data):
        """ Append an item to the list. """

        new_node = Node(data, None, None)
        if self.head is None:
            self.head = new_node
            self.tail = self.head
        else:
            new_node.prev = self.tail
            self.tail.next = new_node
            self.tail = new_node

        self.count += 1

    def dequeue(self):
        """ Remove elements from the front of the list"""
        current = self.head
        if self.count == 1:
            self.count -= 1
            self.head = None
            self.tail = None
        elif self.count > 1:
            self.head = self.head.next
            self.head.prev = None
            self.count -= 1
        return current

```

**Queue program with python list implementation**:

```python
class Queue:
    def __init__(self):
        self.queue = []
    
    def peek(self):
        if self.queue:
            print(self.queue[0])
    
    def view_q(self):
        for i in range(len(self.queue)):
            print(f'item{i} is {self.queue[i]}')
    
    def enqueue(self):
        item = input("what item would you like to push to the queue? ")
        self.queue.append(item)
    def dequeue(self):
        item = self.queue.pop(0)
        print(f"You dequeued {item}")


q = Queue()

while True:
    print('--------------------')
    print("Queue Options")
    print("1. View Queue")
    print("2. See next in Queue")
    print("3. Enqueue")
    print("4. Dequeue")
    print("---------------------")

    menu_choice = int(input("Choose your option: "))
    print()

    if menu_choice == 1:
        q.view_q()
    elif menu_choice == 2:
        q.peek()
    elif menu_choice == 3:
        q.enqueue()
    elif menu_choice == 4:
        q.dequeue()

```

