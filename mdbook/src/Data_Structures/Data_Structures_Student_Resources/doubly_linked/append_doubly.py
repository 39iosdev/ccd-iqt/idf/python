"""
Doubly linked list allows traversal of the list in two directions.
The Node class has an extra attribute of previous/prev.
Thus each node will have two pointers.
One to point to the next node
One to point to the previous node

The head's previous will point to null

The nodes between head and tail will have 
two pointers to the previous and next nodes respectively

The tails next will point to null
"""
#Double node is a variation of the original node class with an extra prev pointer
class DoubleNode:
    def __init__(self, data, next = None, prev = None):
        self.data = data
        self.next = next
        """Insert Code"""

#The DoublyLinkedList class is similar to the basic Linked list class
class DoublyLinkedList:
    def __init__(self):
        self.head = None
        self.tail = None

    #add to the end of the list
    def append(self, data):
            #create the node to hold the data
            new_node = DoubleNode(data)
            #two cases to consider
            #the doubly linked list empty set the new links for the first node
            if self.head is None:
                #the previous will be set to none because it is the beginning
                """Insert Code"""
                #the head is set to the new node
                """Insert Code"""
                #the tail is set to the head to remain circular
                """Insert Code"""
            #not an empty list
            else:
                #set the current tail's next to equal the new node
                """Insert Code"""
                #set the new nodes previous to equal the current tail
                """Insert Code"""
                #set the current tail to equal the current tail's next
                """Insert Code"""
    
    def print_linked_list(self):
        if(self.head == None):
            print("The list is empty")
        else:
            probe = self.head
            while probe != None:
                print(probe.data)
                probe = probe.next

double_linked = DoublyLinkedList()
print("\nTrying to print an empty list\n")
double_linked.print_linked_list()
print("\nAppending to the list")
double_linked.append("Tony")
double_linked.append("War Machine")
double_linked.append("Jarvis")
double_linked.append("Ironman")
print("\nPrinting the list\n")
double_linked.print_linked_list()


