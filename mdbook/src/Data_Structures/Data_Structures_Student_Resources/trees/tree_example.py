
"""
Preorder Traversal - check if the current node is empty
                     display the data part of the root or current node
                     traverse the left subtree by recursively calling preorder
                     traverse the right subtree by recursively calling preorder

Postorder Traversal - traverse the left subtree, 
                      traverse the right subtree, 
                      display the root

Inorder Traversal   - traverse the left subtree
                      display the root
                      traverse the right subtree

Levelorder Traversal - visits each node at each level in left-to-right order
"""


class Tree:
    def __init__(self, data, left = None, right = None):
        self.data = data
        self.left = left
        self.right = right
    
    def __str__(self):
        return str(self.data)

def total(tree):
    if tree == None: return 0
    return total(tree.left) + total(tree.right) + tree.data

def print_tree_preorder(tree):
    if tree == None: return 0
    #data -> left -> right
    print(tree.data)
    print_tree_preorder(tree.left)
    print_tree_preorder(tree.right)

def print_tree_postorder(tree):
    # left -> right -> data
    if tree == None: return 0
    print_tree_postorder(tree.left)
    print_tree_postorder(tree.right)
    print(tree.data)

def print_tree_inorder(tree):
    # left -> data -> right
    if tree == None: return 0
    print_tree_inorder(tree.left)
    print(tree.data)
    print_tree_inorder(tree.right)

def print_tree_indented(tree, level = 0):
    if tree == None: return 0
    print_tree_indented(tree.right, level + 1)
    print('    ' * level + str(tree.data))
    print_tree_indented(tree.left, level + 1)




# tree = Tree(1, Tree(2), Tree(3))
# print(total(tree))


tree = Tree('+', Tree(1), Tree('*', Tree(2), Tree(3)))


            #         +
            #     /       \
            # 1           *
            #           /   \
            #          2     3

print_tree_preorder(tree)
# print_tree_postorder(tree)
# print_tree_inorder(tree)
print_tree_indented(tree)
