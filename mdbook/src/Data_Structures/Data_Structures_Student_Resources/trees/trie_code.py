class Stack:
    def __init__(self):
        #create empty list
        self.items = []
    
    #accepts a tuple of the letter and the node
    def push(self, letter, node):
        #append new item to the list
        self.items.append((letter, node))
    
    def pop(self):
        #removes item
        return self.items.pop()
    
   
#class for the Trie Nodes
class TrieNode:
    def __init__(self):
        self.children = [None]*26
        self.endword = False
        #this limits having to iterate over every option if there are no children.
        self.haschildren = False
    
    def updateChildren(self):
        for c in self.children:
            if c != None:
                self.haschildren = True
                return True
        self.haschildren = False
        return False

#class for the Trie
class Trie:
    def __init__ (self):
        self.root = TrieNode()
    
    #insert into trie
    def insert(self, word):
        #create current variable to traverse 
        current = self.root
        #traverse the word 
        for letter in word:
            index = charToIndex(letter)
            #if not present in the children list insert it
            if not current.children[index]:
                #create the new trie node for the element at the index
                current.children[index] = TrieNode()
                #update haschildren
                if current.haschildren == False:
                    current.haschildren = True
            #update the current variable to the new element
            current = current.children[index]
        #mark the last node as a leaf/end of the word
        current.endword = True

    #search trie
    def search(self, word):
        # current variable set to root 
        current = self.root

        #iterate over the trie to look for the word
        for letter in word:
            #get the index from the charToIndex function
            index = charToIndex(letter)
            #look at the element at index in the children list, if it doesn't exists the word is not in the list
            if not current.children[index]:
                return False
            #update the current variable to the element at the last index
            current = current.children[index]
        #return the endword attribute (True or False) The word is in the list if it is set to True
        return  current.endword 

    #remove from the trie
    def remove(self, word):
        #create current variable to traverse 
        current = self.root
        #create stack object to hold nodes 
        stack = Stack()
        #iterate through trie 
        for letter in word:
            #push element onto the stack to keep track of the previous nodes
            stack.push(letter, current)
            #get the index of the letter of the word to remove
            index = charToIndex(letter)
            if not current.children[index]:
                #The word is not in the trie
                return False
            #make the current variable the element at the index of the children
            current = current.children[index]

        #reached the end of the word
        #check if the node is an end of a word

        if current.endword == False:
            return False
        else: # current.endword == True:
            #set the endword to false
            current.endword = False

        #check if there are children
        if current.haschildren:
            #may need to delete stack in other languages
            return True
        #if there are children we leave them
        #if there are no children, all references in the list are set to None
        #Python will clean up in garbage collection, we don't need to delete. In other languages 
        #this will have to be handled

        #if there are not children we can go up and clean up the previous nodes
        #and delete until we reach the prefix for another word
        
        while current.endword == False and current.haschildren == False:
            prevLetter, prevNode = stack.pop()
            #make the reference to the node None
            index = charToIndex(prevLetter)
            prevNode.children[index] = None
            #update prevNode's has children in case you deleted the last child
            prevNode.updateChildren()
            #make the prevNode the current
            current = prevNode

def charToIndex(ch):
    return ord(ch)-ord('a')

def main():
    #input keys a through z lower case
    keys = ["top", "tops", "topside"]

    #when the functions return, it will return either False(0) or True(1)
    output = ["Not present in Trie", "Present in Trie"]

    #Create a Trie object
    trie = Trie()

    #Make a trie with the keys
    for word in keys:
        trie.insert(word)
    
    print("Looking for a word not added to the trie")
    print("The : {}\n".format(output[trie.search("the")] ))
   

    print("Looking for a word that is in the trie")
    print("topside : {} \n".format(output[trie.search("topside")] ))

    print("Looking for a word that is in the trie")
    print("top : {}\n".format(output[trie.search("top")] ))

    print("Looking for a word that is in the trie")
    print("tops : {}\n".format(output[trie.search("tops")] ))
    print()

    print("Looking for a word not added to the trie")
    print("topper : {}\n".format(output[trie.search("topper")] )) 
    print()

    print("Attempting to Remove a word not added to the trie")
    trie.remove("cops")
    print("cops : {}\n".format(output[trie.search("cops")] ))

    print("Inserting a new word")
    trie.insert("taper")
    print("taper : {}\n".format(output[trie.search("taper")] ))

    print("Inserting a new word")
    trie.insert("tape")
    print("tape : {}\n".format(output[trie.search("tape")] ))

    print("Removing the word taper \n")
    trie.remove("taper")
    print("taper : {}\n".format(output[trie.search("taper")]))

    print("Searching to make sure tape still is in the tree after removing taper \n") 
    print("tape : {}\n".format(output[trie.search("tape")] ))

    print("Inserting the word taper again") 
    trie.insert("taper")
    print("taper : {}\n".format(output[trie.search("taper")] ))

    print("Inserting the word tapering")
    trie.insert("tapering")
    print("tapering : {}\n".format(output[trie.search("tapering")] ))

    print("Removing the word taper again after adding another word to it \n")
    trie.remove("taper")
    print("taper : {}\n".format(output[trie.search("taper")] ))

    print("Searching to make sure tapering still is in the tree after removing taper \n")
    print("tapering : {}\n".format(output[trie.search("tapering")] ))

    print("Inserting the word tapestry")
    trie.insert("tapestry")
    print("tapestry : {}\n".format(output[trie.search("tapestry")] ))

    print("Removing the word tape \n") 
    trie.remove("tape")
    print("tape : {}\n".format(output[trie.search("tape")] ))

    print("Checking to make sure tapering and tapestry are still in the try \n")
    print("tapering : {}".format(output[trie.search("tapering")] ))
    print("tapestry : {}".format(output[trie.search("tapestry")] ))

    print("Removing the word topside \n") 
    trie.remove("topside")
    print("topside : {}\n".format(output[trie.search("topside")] ))

    print("Looking to make sure tops is still in the trie")
    print("tops : {}\n".format(output[trie.search("tops")] ))
    print()

if __name__ == "__main__":
    main()