# Definition: Binary tree node.
class TreeNode(object):
    def __init__(self, data):
         self.val = data
         self.left = None
         self.right = None


def min_val_node(parent, node):
    if node.left:
        return min_val_node(parent, node.left)
    else:
        return parent, node

#note this doesn't completely work if you delete the root
def delete_Node(root, value):
    #we are at the node that needs to be deleted
    if root.val == value:
        #there are two children
        if root.right and root.left:
            #find the successor for the parent node
            parent, successor = min_val_node(root, root.right)

            if parent.left == successor:
                parent.left = successor.right
            else:
                parent.right = successor.right
            
            #reset the left and right children of the successor
            successor.left = root.left
            successor.right = root.right

            return successor
        
        else:
            if root.left:
                return root.left
            else:
                return root.right
    
    else:
        if root.val > value:
            # if root.left:
            root.left = delete_Node(root.left, value)
        else:
            # if root.right:
            root.right = delete_Node(root.right, value)

    return root

   

# def preOrder(node): 
#     if not node: 
#         return      
#     print(node.val)
#     preOrder(node.left) 
#     preOrder(node.right)  


def inOrder(node):
    if not node:
        return
    inOrder(node.left)
    print(node.val)
    inOrder(node.right)
 ####tree example1   
# root = TreeNode(5)  
# root.left = TreeNode(3)  
# root.right = TreeNode(6) 
# root.left.left = TreeNode(2)  
# root.left.right = TreeNode(4) 
# root.left.right.left = TreeNode(11)
# root.right.left = TreeNode(7)
# root.right.right = TreeNode(8) 
# # 

####tree example2
root = TreeNode(25)  
root.left = TreeNode(20)  
root.right = TreeNode(36) 
root.left.left = TreeNode(10)  
root.left.right = TreeNode(22) 
root.right.left = TreeNode(30)  
root.right.right = TreeNode(40)  
root.left.left.left = TreeNode(5) 
root.left.left.right = TreeNode(12)  
root.right.left.left = TreeNode(28)
root.right.right.left = TreeNode(38)
root.right.right.right = TreeNode(48) 
print("\nIn in_order order:")

inOrder(root)
# result = delete_Node(root, 48) 

result = delete_Node(root, 25)

print("\nAfter deleting specified node:")
inOrder(result)
