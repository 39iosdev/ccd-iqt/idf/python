class Queue:
    def __init__(self):
        self.queue = []

    def peek(self):
        if not self.is_empty():
            return self.queue[-1].data

    def view_q(self):
        for i in range(len(self.queue)):
            print(f'item {i} is {self.queue[i]}')
    
    def enqueue(self, item):
        self.queue.insert(0, item)

    def dequeue(self):
        if not self.is_empty():
            return self.queue.pop()
    
    def is_empty(self):
        return len(self.queue) == 0
    
    def __len__(self):
        return self.size()

    def size(self):
        return len(self.queue)
            