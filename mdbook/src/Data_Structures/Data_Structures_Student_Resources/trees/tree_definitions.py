"""
Term               Definition

Node               An item stored in a tree 

Root               The topmost node in a tree. It is the only node without a parent

Child              A node immediately below and directly connected to a given node.
                    A node can have more than one child, and its children are viewed as
                    organized in left-to-right order. The leftmost child is called the 
                    first child, and the rightmost child is the last child.

Parent             A node immediately above and directly connected to a given node. A node 
                    can only have one parent.

Siblings            The children of a common parent

Leaf                A node that has no children

Interior Node       A node that has at least one child

Edge/Branch/Link    The line that connects a parent to its child

Descendant          A node's children, its children's children, and so on, down to the leaves

Ancestor            A node's parent, its parent's parent, and so on, up to the root

Path                The sequence of edges that connect a node and one of its descendants

Path length         The number of edges in a path

Depth or level      The depth or level of a node equals the length of the path connecting it to
                        the root. Thus, the root depth or level of the root is 0. Its children
                        are at level 1, and so on.

Height              The length of the longest path in the tree; put differently, the maximum
                        level number among leaves in the tree

Subtree             The tree formed by considering a node and all its descendants



Binary Tree - A tree data structure in which each node has at most two children which
 are left child, and right child. The top node is the root

 A binary tree is a type of tree that can have a max of two child nodes.


Complete Binary Tree - Every level, except possibly the last, 
is completely filled and all nodes in the last level are as far left as possible

Full Binary Tree - A full binary tree (referred to as a proper or plane binary tree) 
is a tree where every node except the leaves have 2 children

Balanced Binary Tree - The left and right subtrees of every node differ 
in height by no more than 1

Perfect Binary Tree - All internal nodes have two children and all leaf nodes are at the 
same level as  perfect binary tree of height h has 2^h - 1 nodes

Perfectly Balanced Binary Tree - Includes a complete complement of nodes at each
 level but the last one. 

Complete and Full binary are special cases of perfectly balanced







Depth first traversal:
    Pre order (root -> left -> right)

        def print_tree_preorder(tree):
            if tree == None: return 0 #base case
            print tree.data #root
            print_tree(tree.left) #left
            print_tree(tree.right) #right


            tree = Tree ('+', Tree(1), Tree('*', Tree(2), Tree(3)))

    prefix = + X Y


    Post order (left -> right -> root)

        def print_tree_postorder(tree):
            if tree == None: return 0 #base
            print_tree_postorder(tree.left) #left
            print_tree_postorder(tree.right)#right
            print tree.data #root

        postfix = X Y +


    In order ( left -> root -> right)

        def print_tree_inorder(tree):
            if tree == None: return 0 #base case
            print_tree_inorder(tree.left)#left
            print tree.data #root
            print_tree_inorder(tree.right)#right


        def print_tree_indented(tree, level=0): #for graphical representation
            if tree == None: return 0
            print_tree_indented(tree.right, level+1)
            print '  ' * level + str(tree.data)
            print_tree_indented(tree.left, level+1)

        infix = X + Y




Breadth first traversal

    Levelorder Traversal - Visits each node at each level in left-to-right order

        def breadth_first_traversal(root_node): 
                list_of_nodes = [] 
                traversal_queue = deque([root_node]) 

                while len(traversal_queue) > 0:
                    node = traversal_queue.popleft() 
                    list_of_nodes.append(node.data) 
                    if node.left_child: 
                    traversal_queue.append(node.left_child) 
                    if node.right_child: 
                    traversal_queue.append(node.right_child) 
                return list_of_nodes 




Binary search trees

BST is a special type of binary tree in which left child of a node has value 
less than the parent and right child has value greater than parent


•	Searching: For searching element 1, we have to traverse all elements (in order 3, 2, 1). Therefore, searching in binary search tree has worst case complexity of O(n). In general, time complexity is O(h) where h is height of BST.
•	Insertion: For inserting element 0, it must be inserted as left child of 1. Therefore, we need to traverse all elements (in order 3, 2, 1) to insert 0 which has worst case complexity of O(n). In general, time complexity is O(h).
•	Deletion: For deletion of element 1, we have to traverse all elements to find 1 (in order 3, 2, 1). Therefore, deletion in binary tree has worst case complexity of O(n). In general, time complexity is O(h).


AVL completly sorted
    AVL = Named aver Gregory Adelson-Velsky and Evgnii Landis
•	Searching: For searching element 1, we have to traverse elements (in order 5, 4, 1) = 3 = log2n. Therefore, searching in AVL tree has worst case complexity of O(log2n).
•	Insertion: For inserting element 12, it must be inserted as right child of 9. Therefore, we need to traverse elements (in order 5, 7, 9) to insert 12 which has worst case complexity of O(log2n).
•	Deletion: For deletion of element 9, we have to traverse elements to find 9 (in order 5, 7, 9). Therefore, deletion in binary tree has worst case complexity of O(log2n)

The worst case time complexity for search, insert and delete operations in a general Binary Search Tree is O(n) for all
he worst case time complexities of searching in a height balanced tree binary tree, BST/ AVL ( Georgy Adelson-Velsky and Evgenii Landis, )


Deleting from a bst 
Three cases to think about:

Case 1: Node to be deleted is a leaf node (No child). Directly delete the node from the tree.

Case 2: Node to be deleted is an internal node with one child. Copy the child to the node then delete the child

Case 3: Node to be deleted is an internal node with two children
    Two methods:
    Find the minimum of the right side, copy and delete
    Find the maximum of the left side, copy and delete


"""
