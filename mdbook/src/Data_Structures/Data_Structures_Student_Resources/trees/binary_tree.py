"""
Binary Tree - a tree data structure in which each node has at most two children
              which are left child, and right child

              Top node is the root

Complete Binary Tree - Every level, except possibly the last, is completely filled and
                        all nodes in the last level are as far left as possible

Full Binary Tree     - a full binary tree (referred to as a proper or plane binary tree) is
                        a tree where every node except the leaves have 2 children  

Balanced Binary Tree - the left and right subtrees of every node differ in height
                        by no more than 1

Perfect Binary Tree  - all internal nodes have two children and all leaf nodes are at the same level
                        a perfect binary tree of height h has 2^h - 1 nodes.

Perfectly Balanced Binary Tree - includes a complete complement of nodes at each level but 
                                  the last one. Complete and Full binary are special cases
                                  of perfectly balanced  

Unlike linked lists, one-dimensional arrays etc, which are traversed in linear order, 
trees may be traversed in multiple ways        

Depth-first search - preorder, postorder, inorder
Breadth-first search - levelorder

Preorder Traversal - check if the current node is empty
                     display the data part of the root or current node
                     traverse the left subtree by recursively calling preorder
                     traverse the right subtree by recursively calling preorder

Postorder Traversal - traverse the left subtree, 
                      traverse the right subtree, 
                      display the root

Inorder Traversal   - traverse the left subtree
                      display the root
                      traverse the right subtree

Levelorder Traversal - visits each node at each level in left-to-right order
"""

from Queue import Queue
from Stack import Stack

class TreeNode:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

class BinaryTree:
    def __init__(self, root):
        # pass in a root data to start out tree
        self.root = TreeNode(root)

    def print_tree(self, traversal_type):
        if traversal_type == "preorder":
            # start with an empty string that will fill out
            return self.preorder_print(self.root, "")
        if traversal_type == "inorder":
            return self.inorder_print(self.root, "")
        if traversal_type == "postorder":
            return self.postorder_print(self.root, "")
        if traversal_type == "levelorder":
            return self.levelorder_print(self.root)
        else:
            print(f"Traversal type {traversal_type} is not supported. ")
            return False
    
    def preorder_print(self, start, traversal):
        # root --> left --> right
        if start:
            # add on each data to this traversal string
            traversal += (str(start.data) + "-")
            traversal = self.preorder_print(start.left, traversal)
            traversal = self.preorder_print(start.right, traversal)
        return traversal

    def inorder_print(self, start, traversal):
        # left --> root --> right
        if start:
            traversal = self.inorder_print(start.left, traversal)
            traversal += (str(start.data) + "-")
            traversal = self.inorder_print(start.right, traversal)
        return traversal

    def postorder_print(self, start, traversal):
        # left --> right --> root
        if start:
            traversal = self.postorder_print(start.left, traversal)
            traversal = self.postorder_print(start.right, traversal)
            traversal += (str(start.data) + "-")
        return traversal

    def levelorder_print(self, start):
        # check if node is null
        if start is None:
            return
        # instantiate queue object
        queue = Queue()
        queue.enqueue(start)
        traversal = ""
        while queue.size() > 0:
            traversal += str(queue.peek()) + "-"
            node = queue.dequeue()
            # check left and right children of node
            if node.left:
                queue.enqueue(node.left)
            if node.right:
                queue.enqueue(node.right)
        return traversal

    def size(self):
        if self.root is None:
            return 0
        #initialize stack object
        stack = Stack()
        stack.push(self.root)
        size = 1
        while stack:
            node = stack.pop()
            if node.left:
                size += 1
                stack.push(node.left)
            if node.right:
                size += 1
                stack.push(node.right)
        return f'Size of the tree is {size}'



bt = BinaryTree("D")
bt.root.left = TreeNode("B")
bt.root.right = TreeNode("F")
bt.root.left.left = TreeNode("A")
bt.root.left.right = TreeNode("C")
bt.root.right.left = TreeNode("E")
bt.root.right.right = TreeNode("G")

print(bt.print_tree("preorder"))
print()
print(bt.print_tree("inorder"))
print()
print(bt.print_tree("postorder"))
print()
print(bt.print_tree("levelorder"))
print(bt.size())


