"""
Prepending to the list means to add to the beginning.
When adding to the beginning of the list we do not need to traverse the list.
New links will need to be created with the head and the new node then we will
add in the code to increase the size attribute so that we can consistently know the 
size of our list
The big O will be O(1)

"""

class Node:
    def __init__(self, data, next = None):
        #instantiate a Node with a default next of None
        self.data = data
        self.next = next

class SinglyLinkedList:
    def __init__(self):
        #instantiate a head and tail Node with a default of none and size to 0
        self.head = None #keeps track of the beginning of the list
        self.tail = None #keeps track of the end of the list
        self.size = 0    #keep track of the size of the list

    #add to the beginning of the list
    def prepend(self, data):
       """Insert Code"""

    def size_of_list(self):
        return self.size

    #add to the end of the linked list
    def append(self, data):
        #instante a new Node           
            
        new_node = Node(data)
        #if the list is not empty, add the node and update the links
        if self.tail != None: 
            self.tail.next = new_node
            self.tail = new_node
            #increase the size of the list
            self.size += 1 
        #the list is empty, instantiate the head AND tail to the new node
        else:
            self.head = new_node
            self.tail = new_node 
            #increase the size of the list
            self.size += 1 

    #print the linked list
    def print_linked_list(self):
        #traverse the list and print the items on one line until the probe.next is None
        probe = self.head
        while probe != None:
            print(probe.data, end = " ")
            probe = probe.next

    
    
#create the list by adding to the end
linked_list = SinglyLinkedList()
linked_list.append("D1")
linked_list.append("D2")
linked_list.append("D3")
linked_list.append("D4")
linked_list.append("D5")
linked_list.append("D6")


#add to the beginning of the list
linked_list.prepend(53)
#print the list
linked_list.print_linked_list()
#print the size of the list
print(f"\nThe size of the list is: {linked_list.size_of_list()}\n")