"""The append operations is sometimes called an insert operation or add.
The Node class and the SinglyLinkedList class work together to make the linked list
With this append method we must traverse all the way to the end of the list to add to it.
The big O will be O(n) because the big O will be determined by the size of the input.

Because we want to see what is happening in our list we will also use a print_linked_list function
"""

#the Node class has two attributes, data and next
class Node:
    def __init__(self, data, next = None):
        #instantiate a Node with a default next of None
        """Insert Code"""

#the LinkedList class has one attribute
class SinglyLinkedList:
    def __init__(self):
        #instantiate the head with a default of None
        """Insert Code"""
    
    #add things to the end of the linked list
    def append(self, data):
        #instante a new Node
        new_node = Node(data)
        #if the linked list empty create a head from the new node
        if """Insert Code""":

            #list is not empty so find the end and add to it
        else:
            #initialize our probe pointer
            """Insert Code""
            #check if probe is at the end if not then advance until it is none
            while """Insert Code""":
                """Insert Code"""

    #print the linked list
    def print_linked_list(self):
        #traverse the list and print the data on one line until the probe.next is None
        """Insert Code"""

linked_list = SinglyLinkedList()
linked_list.append("D1")
linked_list.append("D2")
linked_list.append("D3")
linked_list.append("D4")
linked_list.append("D5")
linked_list.append("D6")
linked_list.print_linked_list()