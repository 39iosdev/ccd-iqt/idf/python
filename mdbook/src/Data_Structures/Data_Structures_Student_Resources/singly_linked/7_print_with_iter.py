"""
Now that we know how to use a generator function to return the
data of our list, we can make use of it by updating our print function

"""

class Node:
    def __init__(self, data, next = None):
        #instantiate a Node with a default next of None
        self.data = data
        self.next = next

class SinglyLinkedList:
    def __init__(self):
        #instantiate a head and tail Node with a default of none and size to 0
        self.head = None #keeps track of the beginning of the list
        self.tail = None #keeps track of the end of the list
        self.size = 0    #keep track of the size of the list

    #print the linked list with the help of the iter function
    def print_linked_list(self):
        #print calling the iter function
        """Insert Code"""

    #iterates over the list and returns the list
    def iter(self):
        probe = self.head
        while probe:
            val = probe.data
            probe = probe.next
            yield val

    #add to the beginning of the list
    def prepend(self, data):
        #instantiate a new Node object
        new_node = Node(data)
        #adjust the link by making new_node's next equal to the head
        new_node.next = self.head
        #Now make the head the new node
        self.head = new_node
        #increase the size of the list
        self.size += 1 

    def size_of_list(self):
        return self.size

    #add to the end of the linked list
    def append(self, data):
        #instante a new Node           
            
        new_node = Node(data)
        #if the list is not empty, add the node and update the links
        if self.tail != None: 
            self.tail.next = new_node
            self.tail = new_node
            #increase the size of the list
            self.size += 1 
        #the list is empty, instantiate the head AND tail to the new node
        else:
            self.head = new_node
            self.tail = new_node 
            #increase the size of the list
            self.size += 1 

   

    
    
#create the list by adding to the end
linked_list = SinglyLinkedList()
linked_list.append("D1")
linked_list.append("D2")
linked_list.append("D3")
linked_list.append("D4")
linked_list.append("D5")
linked_list.append("D6")


#add to the beginning of the list
linked_list.prepend(53)
#print the list
# linked_list.print_linked_list()
#print the size of the list
print(f"\nThe size of the list is: {linked_list.size_of_list()}\n")

#print the items in the list 
linked_list.print_linked_list()