"""
To delete a node we must know how to select a node to delete.
This delete_node function deletes by index.
Deleting a node between two other nodes means we will declare
a variable named previous (prev)  and make the node directly connected
to the successor of the node that is being deleted the previous nodes next node.

Big O of O(n)
"""

class Node:
    def __init__(self, data, next = None):
        #instantiate a Node with a default next of None
        self.data = data
        self.next = next

class SinglyLinkedList:
    def __init__(self):
        #instantiate a head and tail Node with a default of none and size to 0
        self.head = None #keeps track of the beginning of the list
        self.tail = None #keeps track of the end of the list
        self.size = 0    #keep track of the size of the list

    def delete_node_by_index(self, index):
        #is this the only node or the list empty?
        if """Insert Code""":
            
        else: #the list isn't empty
            """Insert Code"""


    #inserting a node within the linked list
    def insert_node(self, index, data):
    #is the head empty or index less than or equal to 0?
        if self.head is None or index <= 0:
            #create the head with a new node
            self.head = Node(data, self.head)
            #increase the size of the list
            self.size += 1
        #find the position to insert
        else:
            #start at the head and continue to the end decrementing the index, when index reaches 0 then insert
            probe = self.head
            while index > 1 and probe.next != None:
                probe = probe.next
                index -= 1
            #insert new node after node at position index -1 or last position
            probe.next = Node(data, probe.next)
            #increase the size of the list
            self.size += 1

    #print the linked list with the help of the iter function
    def print_linked_list(self):
         #print calling the iter function
        for numD in self.iter():
             print(numD, end = ' ')

    #iterates over the list and returns the list, generator function
    def iter(self):
        probe = self.head
        while probe:
            val = probe.data
            probe = probe.next
            yield val

    #add to the beginning of the list
    def prepend(self, data):
        #instantiate a new Node object
        new_node = Node(data)
        #adjust the link by making new_node's next equal to the head
        new_node.next = self.head
        #Now make the head the new node
        self.head = new_node
        #increase the size of the list
        self.size += 1 

    def size_of_list(self):
        return self.size

    #add to the end of the linked list
    def append(self, data):
        #instante a new Node           
            
        new_node = Node(data)
        #if the list is not empty, add the node and update the links
        if self.tail != None: 
            self.tail.next = new_node
            self.tail = new_node
            #increase the size of the list
            self.size += 1 
        #the list is empty, instantiate the head AND tail to the new node
        else:
            self.head = new_node
            self.tail = new_node 
            #increase the size of the list
            self.size += 1 

   

    
    
#create the list by adding to the end
linked_list = SinglyLinkedList()
linked_list.append("D1")
linked_list.append("D2")
linked_list.append("D3")
linked_list.append("D4")
linked_list.append("D5")
linked_list.append("D6")


#add to the beginning of the list
linked_list.prepend(53)

#add at a specific index
linked_list.insert_node(2, "python")

#print the size of the list
print(f"\nThe size of the list is: {linked_list.size_of_list()}\n")

#print the items in the list 
linked_list.print_linked_list()

print("\nDeleting at index 3")
linked_list.delete_node_by_index(3)
linked_list.print_linked_list()
print(f"\nThe size is: {linked_list.size_of_list()}\n")