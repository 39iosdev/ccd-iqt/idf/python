"""The size_of_list function returns the size of the linked list by adding
to the SinglyLinkedList class.  Whenever a change is made to the list
the size is changed.
This allows the big O to go from O(n) to O(1) 

"""

class Node:
    def __init__(self, data, next = None):
        #instantiate a Node with a default next of None
        self.data = data
        self.next = next

class SinglyLinkedList:
    def __init__(self):
        #instantiate a head and tail Node with a default of none and size to 0
        self.head = None #keeps track of the beginning of the list
        self.tail = None #keeps track of the end of the list
        """Insert Code"""    #keep track of the size of the list

    #return the size of the list by using the size attribute of the SinglyLinkedList class
    def size_of_list(self):
        """Insert Code"""

    #add to the end of the linked list
    def append(self, data):
        #instante a new Node           
            
        new_node = Node(data)
        #if the list is not empty, add the node and update the links
        if self.tail != None: 
            self.tail.next = new_node
            self.tail = new_node
            #increase the size of the list
            """Insert Code""" 
        #the list is empty, instantiate the head AND tail to the new node
        else:
            self.head = new_node
            self.tail = new_node 
            #increase the size of the list
            """Insert Code"""

    #print the linked list
    def print_linked_list(self):
        #traverse the list and print the items on one line until the probe.next is None
        probe = self.head
        while probe != None:
            print(probe.data, end = " ")
            probe = probe.next

    
    

linked_list = SinglyLinkedList()
linked_list.append("D1")
linked_list.append("D2")
linked_list.append("D3")
linked_list.append("D4")
linked_list.append("D5")
linked_list.append("D6")
linked_list.print_linked_list()

print(f"\nThe size of the list is: {linked_list.size_of_list()}\n")