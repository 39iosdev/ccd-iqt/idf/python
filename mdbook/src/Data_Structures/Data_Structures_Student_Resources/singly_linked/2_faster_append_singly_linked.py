"""The difference between the faster append function and the simple append function is a result
of the SinglyLinkedList class having another attribute.  Instead of one attribute of head, it now contains
a tail attribute.  This allows nodes to be added directly to the end of the list by changing the links
and thus reducing the big O to O(1).
"""

class Node:
    def __init__(self, data, next = None):
        #instantiate a Node with a default next of None
        self.data = data
        self.next = next

class SinglyLinkedList:
    def __init__(self):
        #instantiate a head and tail Node with a default of none
        self.head = None #keeps track of the beginning of the list
        """Insert Code"""

    #add to the end of the linked list
    def append(self, data):
        #instante a new Node
        new_node = Node(data)
        #if the list is not empty, add the node and update the links
        if """Insert Code""":
        #the list is empty, instantiate the head AND tail to the new node
        else:
            """Insert Code"""
            
    #print the linked list
    def print_linked_list(self):
        #traverse the list and print the items on one line until the probe.next is None
        probe = self.head
        while probe != None:
            print(probe.data, end = " ")
            probe = probe.next

linked_list = SinglyLinkedList()
linked_list.append("D1")
linked_list.append("D2")
linked_list.append("D3")
linked_list.append("D4")
linked_list.append("D5")
linked_list.append("D6")
linked_list.print_linked_list()