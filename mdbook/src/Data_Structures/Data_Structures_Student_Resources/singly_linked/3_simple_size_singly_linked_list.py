"""The size_of_list function returns the size of the linked list by traversing
every node and counting it as long as the next is not none.
This can be an expensive operation depending on the size of the list
The big O is O(n)

"""

class Node:
    def __init__(self, data, next = None):
        #instantiate a Node with a default next of None
        self.data = data
        self.next = next

class SinglyLinkedList:
    def __init__(self):
        #instantiate a head and tail Node with a default of none
        self.head = None #keeps track of the beginning of the list
        self.tail = None #keeps track of the end of the list

    #get the size of the list by traversing and counting each node
    def size_of_list(self):
         """Insert Code"""
         
    #add to the end of the linked list
    def append(self, data):
        #instante a new Node
        new_node = Node(data)
        #if the list is not empty, add the node and update the links
        if self.tail != None: 
            self.tail.next = new_node
            self.tail = new_node
        #the list is empty, instantiate the head AND tail to the new node
        else:
            self.head = new_node
            self.tail = new_node 

    #print the linked list
    def print_linked_list(self):
        #traverse the list and print the items on one line until the probe.next is None
        probe = self.head
        while probe != None:
            print(probe.data, end = " ")
            probe = probe.next

    
    

linked_list = SinglyLinkedList()
linked_list.append("D1")
linked_list.append("D2")
linked_list.append("D3")
linked_list.append("D4")
linked_list.append("D5")
linked_list.append("D6")
linked_list.print_linked_list()

print(f"\nThe size of the list is: {linked_list.size_of_list()}\n")