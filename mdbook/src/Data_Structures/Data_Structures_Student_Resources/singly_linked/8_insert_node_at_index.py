"""
To insert a node within a specific location of the list we need to know
where/the index and we need the data.
From there we will update the links to position the node correctly in the list
Then we will adjust the size of the list accordingly

Big O of O(n)
"""

class Node:
    def __init__(self, data, next = None):
        #instantiate a Node with a default next of None
        self.data = data
        self.next = next

class SinglyLinkedList:
    def __init__(self):
        #instantiate a head and tail Node with a default of none and size to 0
        self.head = None #keeps track of the beginning of the list
        self.tail = None #keeps track of the end of the list
        self.size = 0    #keep track of the size of the list


    #inserting a node within the linked list
    def insert_node(self, index, data):
    #is the head empty or index less than or equal to 0?
        if """Insert Code""":
        #find the position to insert
        else:
            #start at the head and continue to the end decrementing the index, when index reaches 0 then insert
            """Insert Code"""

    #print the linked list with the help of the iter function
    def print_linked_list(self):
         #print calling the iter function
        for numD in self.iter():
             print(numD, end = ' ')

    #iterates over the list and returns the list, generator function
    def iter(self):
        probe = self.head
        while probe:
            val = probe.data
            probe = probe.next
            yield val

    #add to the beginning of the list
    def prepend(self, data):
        #instantiate a new Node object
        new_node = Node(data)
        #adjust the link by making new_node's next equal to the head
        new_node.next = self.head
        #Now make the head the new node
        self.head = new_node
        #increase the size of the list
        self.size += 1 

    def size_of_list(self):
        return self.size

    #add to the end of the linked list
    def append(self, data):
        #instante a new Node           
            
        new_node = Node(data)
        #if the not empty, add the node and update the links
        if self.tail != None: 
            self.tail.next = new_node
            self.tail = new_node
            #increase the size of the list
            self.size += 1 
        # is empty, instantiate the head AND tail to the new node
        else:
            self.head = new_node
            self.tail = new_node 
            #increase the size of the list
            self.size += 1 

   

    
    
#create the list by adding to the end
linked_list = SinglyLinkedList()
linked_list.append("D1")
linked_list.append("D2")
linked_list.append("D3")
linked_list.append("D4")
linked_list.append("D5")
linked_list.append("D6")


#add to the beginning of the list
linked_list.prepend(53)

#add at a specific index
print("Inserting at index 2")
linked_list.insert_node(2, "python")

#print the size of the list
print(f"\nThe size of the list is: {linked_list.size_of_list()}\n")

#print the items in the list 
linked_list.print_linked_list()