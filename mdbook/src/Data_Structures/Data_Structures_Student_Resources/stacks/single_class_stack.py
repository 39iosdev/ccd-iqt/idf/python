"""

Stacks:
    Linear collections in which access is completley restricted to just one end called the top
    LIFO - Last in first out protocol
        Think of a stack of dishes at a buffet
    push - put items on the stack
    pop - remove the top item from the stack
    peek - examine the top element on the stack

    stack type is not built in to Python so we can use a list to emulate an array-based stack
    We use the methods append and pop
    It's possible to utilize other methods like insert, replace and remove,
    however that defeats the spirit of the stack

"""

class Stack:
    def __init__(self):
        #create empty list
        self.items = []
    
    def push(self, item):
        #append new item to the list
        self.items.append(item)
    
    def pop(self):
        #removes item
        return self.items.pop()
    
    def is_empty(self):
        #empty list
        return self.items == []
    
    def peek(self):
        #shows the top item without removing
        if not self.is_empty():
            return self.items[-1]
    
    def get_stack(self):
        #returns the stack of items
        return self.items


##################################################

# s = Stack()
# s.push('first in')
# s.push('second in')
# s.push('last in, first out')
# print(s.get_stack())
# print(s.pop())
# print(s.get_stack())

"""

Use a stack to check whether or not a string has balanced usage of parenthsis

"""

def is_match(p1, p2):
    if p1 == "(" and p2 == ")":
        return True
    elif p1 == "{" and p2 == "}":
        return True
    elif p1 == "[" and p2 == "]":
        return True
    else:
        return False

def is_paren_balanced(paren_string):
    s = Stack()
    is_balanced = True#keep track of where we are
    index = 0
    #loop through string
    while index < len(paren_string) and is_balanced:
        paren = paren_string[index]
        if paren in "([{":
            s.push(paren)
        #its a closed paren
        else:
            if s.is_empty():
                is_balanced = False
            else:
                top = s.pop()
                #the popped item and the current item we're on
                if not is_match(top, paren):
                    #not a match
                    is_balanced = False
        index += 1
    if s.is_empty() and is_balanced:
        return True
    else:
        return False


print(is_paren_balanced("()"))
print(is_paren_balanced("{{([{}])}"))
