class Node:
    def __init__(self, data = None):
        self.data = data
        self.next = None


class Stack:
    def __init__(self):
        self.top = None
        self.size = 0

    def push(self, data):
        node = Node(data)
        if self.top:
            node.next = self.top
            self.top = node
        else:
            self.top = node
        self.size += 1

    def pop(self):
        if self.top:
            data = self.top.data
            self.size -= 1
            if self.top.next:
                self.top = self.top.next
            else:
                self.top = None
            return data
        else:
            return None

    def peek(self):
        if self.top:
            return self.top.data
        else:
            return None

def check_brackets(statement):
    stack = Stack()
    #check each character
    for ch in statement:
        #if the character matches one of the three left parens push to stack
        if ch == '{' or ch == '['or ch == '(':
            stack.push(ch)
        #if the character matches one of the three right parens pop and hold in last variable
        if ch == '}' or ch == ']' or ch == ')':
            last = stack.pop()
            #check if the last variable matches most recent character
            if last == '{' and ch == '}':
                continue
            elif last == '[' and ch == ']':
                continue
            elif last == '(' and ch == ')':
                continue
            #doesn't match then return false
            else:
                return False
    #too many left in the stack should be empty if all match
    if stack.size > 0:
        return False
    else:
        return True

s1 = ( 
    "{(foo), (bar) } [hello] (((this) is) a) test",
    "{(foo), (bar) } [hello] (((this) is) atest",
    "{(foo), (bar) } [hello] (((this) is) a) test))"
)
for s in s1:
    m = check_brackets(s)
    print("{}: {}".format(s, m))