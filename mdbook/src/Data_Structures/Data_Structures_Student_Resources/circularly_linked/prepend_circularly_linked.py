"""
To prepend/add to the beginning of a circularly linked list we will
proceed as before with the singly linked list prepend with the exception
that we will need to carefully reconnect the links between the head and tail

"""
#The node class for circularly linked list is the same as for a singly
class Node:
    def __init__(self, data, next = None):
        #instantiate a Node with a defaults next of None
        self.data = data
        self.next = next

#The circularlyLinked class is the same as the basic singly linked
class CircularLinked:
    def __init__(self):
        self.head = None
    
    def prepend(self, data):
        #create a new node with data to add
        new_node = Node(data)
        #create a probe variable equal to the head
        """Insert Code"""
        #the new nodes next is set equal to head
        """Insert Code"""

        #no other elements in the list
        if not self.head:
            #probe's next is set equal to new node to begin list
            """Insert Code"""
        #at least one node in the list
        else:
            #while probe's next is not equal to head
            while """Insert Code""":
                #make the probe equal to the probe's next
                """Insert Code"""
            #set the probe's next to the new node when the next is equal to head
            """Insert Code"""
        #make the head equal the new node
        """Insert Code"""

    def append(self, data):
        #if list is empty
        if not self.head:
            #set head equal to new node
            self.head = Node(data)
            #point the heads next to it's own head
            self.head.next = self.head
        #at least one node in the list reset links and add node
        else:
            #create a new node with the data to add
            new_node = Node(data)
            #create a variable named probe to traverse the list
            probe = self.head
            #while probe's next is not equal to the head traverse until it is
            while probe.next != self.head:
                probe = probe.next
            #make the probe's next equal to the new node
            probe.next = new_node
            #make the new nodes next equal the head
            new_node.next = self.head
    
    def print_circ_linked_list(self):
        probe = self.head
        #while probe does not equal head print the data and move to the next 
        while probe:
            print(probe.data)
            probe = probe.next
            #breaking condition since this is circular and could go on forever
            if probe == self.head:
                break

circular_linked = CircularLinked()
circular_linked.append("first append")
circular_linked.append("second append")
circular_linked.append("third append")
circular_linked.append("fourth append")
print("Printing the list\n")
circular_linked.print_circ_linked_list()
print("\nPrepending the list \n")
circular_linked.prepend("first prepend")
print("Reprinting the list\n")
circular_linked.print_circ_linked_list()
