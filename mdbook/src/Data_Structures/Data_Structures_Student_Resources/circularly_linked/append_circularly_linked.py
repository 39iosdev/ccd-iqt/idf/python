"""
circularly Linked List :
    Special case of singly linked list
    Insertion and removal of the first node are special cases
    These are special because the head pointer must be reset.
    You can use circular linked lists with a dummy header node.
    Contains a link from the last node back to the first node in the structure

"""
#The node class for circularly linked list is the same as for a singly
class Node:
    def __init__(self, data, next = None):
        #instantiate a Node with a defaults next of None
        self.data = data
        self.next = next

#The circularlyLinked class is the same as the basic singly linked
class CircularLinked:
    def __init__(self):
        self.head = None
    
    def append(self, data):
        #if list is empty
        if not self.head:
            #set head equal to new node
            self.head = Node(data)
            #point the heads next to it's own head
            """Insert Code"""
        #at least one node in the list reset links and add node
        else:
            #create a new node with the data to add
            new_node = Node(data)
            #create a variable named probe to traverse the list
            probe = self.head
            #while probe's next is not equal to the head traverse until it is
            while """Insert Code""":
                """Insert Code"""
            #make the probe's next equal to the new node
            """Insert Code"""
            #make the new nodes next equal the head
            """Insert Code"""
    
    
    def print_circ_linked_list(self):
        probe = self.head
        #while probe does not equal the head print the data and move to the next 
        while probe:
            print(probe.data)
            probe = probe.next
            #breaking condition since this is circular and could go on forever
            if probe == self.head:
                break



circular_linked = CircularLinked()
circular_linked.append("first append")
circular_linked.append("second append")
circular_linked.append("third append")
circular_linked.append("fourth append")
print("Printing the list\n")
circular_linked.print_circ_linked_list()

