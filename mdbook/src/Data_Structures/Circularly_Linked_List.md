# Circularly Linked Lists

Circularly linked lists are a special case of a singly linked list.

![](Assets/circular_linked_list.png)

Insertion and removal of the first node are special cases because the head pointer must be reset.

A circularly linked list can be used with a dummy header node.

The dummy contains a link from the last node back to the first node in the structure.

---
We again use the node class and a class for our circularly linked list that looks very similar to what we used for the singly linked list.

```python

class Node:
    def __init__(self, data, next = None):
        #instantiate a Node with a defaults next of None
        self.data = data
        self.next = next

class CircularLinked:
    def __init__(self):
        self.head = None
    

```
We can append just like the singly linked list, however the code is slightly different

```python
def append(self, data):
    #no items in list
    if not self.head:
        self.head = Node(data)
        self.head.next = self.head
    #at least one node in the list, add to the end
    else:
        new_node = Node(data)
        probe = self.head
        while probe.next != self.head:
            probe = probe.next
        probe.next = new_node
        new_node.next = self.head

```
The **append** method checks for an empty list.  Below is some pseudocode to walk us through this:

```text
If the list is empty:
    Set the head equal to the new node instance.
    The head's next is then set to the head so it points to it's own head 
Else the list is not empty:
    Create a new node with the data
    Create a probe variable to traverse the list
    While the probe's next is not equal to the head
        Continue setting the probe equal to it's next, continue looking for the last entered node

    When we reach the last entered node the probe's next will equal the new node
    Then reset the new node's next to equal the head
```

All the links are made before the head is set to be linked to the new node's next.

---
We also can prepend to the list, as we did before, but again, the links between the head and tail need to be carefully re-connected.

```python
def prepend(self, data):
    new_node = Node(data)
    probe = self.head
    new_node.next = self.head

    #no other elements in the list
    if not self.head:
        new_node.next = new_node
    #at least one node in the list
    else:
        while probe.next != self.head:
            probe = probe.next
        probe.next = new_node
    self.head = new_node

```

In the prepend:

```text
Create a new node
Create a probe variable that is set to the head.
Then the new node's next is set to the head so that the links can be 
    kept track of while traversing
```

Then the the conditional checks
```text
If the list is empty
    Set the new node's next equal to the new node to begin the list

Else the list is not empty
    While the probe's next is not equal to the head
    The probe will be set equal to the probe's next to traverse the list.
When the probe's next is equal to the head then it is set to the new node

Then the head is also set to the new node.
```
    
All the connections are made before the new node is set to become the head.

---

Printing a circularly linked list

```python
 def print_circ_linked_list(self):
     # Create a probe variable and set it equal to the head.
    probe = self.head
    # While the probe variable is not none 
    while probe:
        # The data of the probe will be printed
        print(probe.data)
        # Then the probe will be set to the probe's next to continue traversing.
        probe = probe.next
        #breaking condition since this is circular and could go on forever
        if probe == self.head:
            # If the probe is equal to the head 
            break

```
    