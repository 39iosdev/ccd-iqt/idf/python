# Trees

## Nonlinear Data Structures

Trees are nonlinear data structures. Tree structures allow us to implement algorithms much faster than when using linear data structures, such as array-based lists or linked lists. Additionally, trees provide a natural organization for data, and have become structures in file systems, graphical user interfaces, databases, Web sites, and other computer systems.

The relationships in a tree are hierarchical, with some objects being “above” and some “below” others. 
The main terminology for tree data structures comes from family trees, with the terms “parent,” “child,” “ancestor,” and “descendant” being the most common words used to describe relationships. 

**Family Tree**

![](Assets/Tree1.png)

---

### TREE DEFINITIONS AND PROPERTIES

A tree is an abstract data type that stores elements hierarchically. 

We typically call the top element the root of the tree, but it is drawn as the highest element, with the other elements being connected below (Think of a tree being upside down).

With the exception of the top element, each element in a tree has a parent element and zero or more children elements. 

A tree is usually visualized by placing elements inside ovals or rectangles, and by drawing the connections between parents and children with straight lines. 

 
![](Assets/Tree2.png)

The image depicts a tree with 17 nodes representing the organization of a fictitious corporation. 

The root stores Electronics R'Us. 


The children of the root are R & D, Sales, Purchasing, and Manufacturing. 

The internal nodes, which are nodes that have child nodes, are Sales, International, Overseas, Electronics R'Us, and Manufacturing

**Formal Tree Definition**

Formally, a tree is defined as a set of nodes storing elements such that the nodes have a parent-child relationship that satisfies the following properties:

* If the tree is nonempty, it has a special node, called the root, that has no parent.
* Each node of the tree that is not the root has a unique parent node.
* Every node with parent is a child.

Note that according to our definition, a tree can be empty, meaning that it does not have any nodes, which allows us to define a tree recursively.  A tree is either empty or consists of a node, called the root of the tree, and a (possibly empty) set of subtrees whose roots are the children of root.

**Tree Definitions**

**Node**: An item stored in a tree

**Root**: The topmost node in a tree. It is the only node without a parent

**Child**: A node immediately below and directly connected to a given node. A node can have more than one child. The children are viewed as organized in left-to-right order. The leftmost child is called the first child, and the rightmost child is the last child.

**Parent**: A node immediately above and directly connected to a given node. A node can only have one parent.

**Siblings**: The children of a common parent

**Leaf**: A node that has no children

**Interior Node**: A node that has at least one child

**Edge/Branch/Link**: The line that connects a parent to its child

**Descendant**: A node's children, its children's children, and so on, down to the leaves

**Ancestor**: A node's parent, its parent's parent, and so on, up to the root

**Path**: The sequence of edges that connect a node and one of its descendants

**Path length**: The number of edges in a path

**Depth or level**: The depth or level of a node equals the length of the path connecting it to the root. Thus, the root depth or level of the root is 0. Its children are at level 1, and so on.

**Height**: The length of the longest path in the tree; put differently, the maximum level number among leaves in the tree

**Subtree**: The tree formed by considering a node and all its descendants


![](Assets/Tree_properties.png)


&nbsp;

# Tree Use Examples

**Example:**

There is a hierarchical relationship between files and directories in a computer's file system. In the Figure below, we see that the internal nodes of the tree are associated with directories and the leaves are associated with regular files.

In the UNIX and Linux operating systems, the root of the tree is appropriately called the “root directory” and is represented by the symbol “/.”

**Tree Representation of a File System**

![](Assets/Tree3.png)


**Example:** The inheritance  between classes in a Python program forms a tree when single inheritance is used. 

The BaseException class is the root of that hierarchy, while all user-defined exception classes should conventionally be declared as descendants of the more specific Exception class. 

![](Assets/Tree4.png)

**Python's hierarchy of Exception Types**

In Python, all classes are organized into a single hierarchy, as there exists a built-in class named object as the ultimate base class. It is a direct or indirect base class of all other types in Python (even if not declared as such when defining a new class). Therefore, the hierarchy pictured in Figure above is only a portion of Python's complete class hierarchy.

---
