
# Singly Linked Lists

To understand Linked lists we need to discuss nodes more and develop a node class.

---
**What is a node?**

Nodes are the building blocks of linked lists that do the linking and contain the data to link.

Nodes are created by implementing a class that has two parts.

* Data: Contains the data element.  Often times called **data**
* Address: Points to location of the next node.  Often times called **next**

Since Classes create objects, we can reuse the class to create a new object for each **node** of our list and connect the data with the address to the next node.

![](Assets/linklist.png)

In the image it looks as if the items are sequential but in memory they can be anywhere.

---

### Node Class

The **next** attribute points to the address of the next data in the list allowing them to be anywhere within the memory space.  Getters and setters could be used to set the values otherwise the default of the next node will be None, which means the last item in our list will point to None.

```python
#This class will represent a single linked list node
class Node:
    def __init__(self, data, next = None):
        #instantiate a Node with a default next of none
        self.data = data
        self.next = next
```

---

### LinkedList Class

The first item in our list will be kept tracked of by a **head** pointer.
The last node is sometimes refereed to as the tail node and has a null link.
This will be done in another class named "LinkedList".
The two classes, "Node" and "LinkedList" work together to create our list.
The LinkedList class, for now, only has one attribute and that is "head" and will point to none as default, meaning that the list is empty.
The rest of the nodes do not have a specified name and are referenced by the link of the preceding node.

*Note: Sometimes, as you will see in the demos, there can be another attribute named tail.

```python

#This class represents the linked list
class LinkedList:
    def __init__(self):
        self.head = None       
```

As stated before there are advantages and disadvantages to linked lists.

*Advantages*:

* Dynamic:  Insertion and deletion are simpler because the pointers can be updated instead of the elements being shuffled in memory
* Stack and Queue structures and be easily implemented with lists
* Linked lists require smaller memory allocations than arrays or lists.  They do not require the memory addresses to be sequential because they mimic pointers by connecting the memory addresses.

*Disadvantages*:

* Additional memory is used up by next pointers
* Random access is not possible.  The whole list must be traversed from the beginning to get to a particular node.  This is not always the most efficient in terms of big O
* Each node is going to take up the space of the data type being stored as well as additional space for the pointer to the next node
* Can only be traversed in one direction

Once the attributes are set we can add different functions for the classes to perform.

For now, with a linked list, we will add new nodes to the beginning of list and we will point the head at the newly added node.

If we would want to insert a node at the end of the list then we would want to keep track of the end of the list with another pointer.

---
