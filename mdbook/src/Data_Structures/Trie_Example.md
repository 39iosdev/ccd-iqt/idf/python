<!-- # This code is a work in progress.
# It is not intended for use until this comment is removed
# The document will eventually be in .md format instead of .py
# This is testing phase -->

# Trie Example

## Classes for a Trie

To implement this trie two classes are going to be used: TrieNode and Trie

### TrieNode Class

Typically a node class has two attributes, but for this example we add a third.  

The **haschildren** attribute is added to indicate whether or not a node has children to save a little bit of time instead of checking all the elements of the **children** list.

The associated **updateChildren** function is also added to ensure the **haschildren** attribute is updated if nodes are removed from the trie.

```python
#class for the Trie Node
class TrieNode:
    def __init__(self):
        self.children = [None]*26
        self.endword = False
        self.haschildren = False

    def updateChildren(self):
        for c in self.children:
            if c != None:
                self.haschildren = True
                return True
        self.haschildren = False
        return False
```

### Trie Class

This class has one attribute: the root TrieNode.

```python
#class for the Trie
class Trie:
    def __init__ (self):
        self.root = TrieNode()
```

## Trie class functions

### Converting char to index

**charToIndex** is a helper function that converts an English character to the associated index in the **children** array of the **TrieNode** class.  

Remember that since we are using the English alphabet that the letter **A** will correlate with the index of 0 through to the letter **Z** which will have an index of 25.

```python
def charToIndex(ch):
    return ord(ch)-ord('a')
```

### Inserting into a Trie

* Initialize a variable **current** starting at the root to move through the nodes of the trie
* Iterate through each *letter* of the word you are inserting
  * Create a child node for the *letter* if it doesn't exist for the **current** node
    * Update **haschildren** to **True** for the **current** node as required
  * Update **current** to the node for the *letter* of the word  
    (aka the node that already existed or the node that you just created)
* When done iterating through the letters of the word, update **endword** for the **current** node to **True**

```python
    #insert into trie
    def insert(self, word):
        #current to traverse through nodes
        current = self.root
        #iterate through the word 
        for letter in word:
            #get the index from the charToIndex function
            index = charToIndex(letter)
            #if not present in the children list insert it
            if not current.children[index]:
                #create the new trie node for the element at the index
                current.children[index] = TrieNode()
                #update haschildren
                if current.haschildren == False:
                    current.haschildren = True
            #update the current variable
            current = current.children[index]
        #mark the last node as the end of the word
        current.endword = True
```

### Searching a Trie

The search function is very similar to the insert function.  The primary difference is that you only traverse the trie on the paths that exist.  If a path does not exist for the word that you are searching for, then the word does not exist in the trie, and the function returns **False**.  If you can traverse the trie all the way to the end of the word, then the **endword** attribute for that final node is returned.

```python
    #search trie
    def search(self, word):
        #current to traverse through nodes
        current = self.root
        #iterate through the word 
        for letter in word:
            #get the index from the charToIndex function
            index = charToIndex(letter)
            #look at the element at index in the children list, if it doesn't exists the word is not in the list
            if not current.children[index]:
                return False
            #update the current variable to the element at the last index
            current = current.children[index]
        #return the endword attribute (True or False) The word is in the list if it is set to True
        return current.endword
```

### Removing from a Trie

Recall from the lesson that the process of removing a word from a trie is more complex because you sometimes have to work your way backwards toward the root of the tree removing nodes that are no longer needed.

In order to accomplish the process of moving up the tree (without altering the data structure) this remove function utilizes a Stack as a way of remembering the node traversal history.

* Initialize a variable **current** starting at the root to move through the nodes of the trie
* Initialize a stack
* Traverse the trie by iterating over the letters as we have done in the functions above with these considerations:
  * Push the **current** node and the next letter onto the stack at the top of loop
  * Return **False** if the path to the next node does not exist (just like in the search method above)
* When done iterating through the letters of the word
  * if **endword** is **False** then return **False** because the word was not in the trie
  * otherwise, update **endword** for the **current** node to **False** because we are removing it from the trie
* If the **current** node has children, return **True** because there is no other work to do.
* Start the process for working your way back up the branch, deleting nodes as required.
  * While the **current** node has no children and its **endword** is **False**
    * Pop the stack and store the result in **prevNode** and **prevLetter**
    * Set the child reference of **prevNode** for the **prevLetter** to **None**
    * Update **prevNode**'s **haschildren** attribute because you may have just deleted the last child
    * Update **current** to **prevNode**

***Note***: It is important to remember that python has garbage collection.  We do not need to clean up memory as we would in other languages.  If you implement a version of this structure in a language such as **C** then you must clean up memory for nodes that are no longer being used.

```python
class Stack:
    def __init__(self):
        #create empty list
        self.items = []
    
    #accepts a tuple of letter and node
    def push(self, letter, node):
        #append new item to the list
        self.items.append((letter, node))
    
    def pop(self):
        #removes item
        return self.items.pop()
```

```python
    #remove from the trie
    def remove(self, word):
        #create current variable to traverse 
        current = self.root
        #create stack object to hold nodes 
        stack = Stack()
        #iterate through trie 
        for letter in word:
            #push element onto the stack to keep track of the previous nodes
            stack.push(letter, current)
            #get the index of the letter of the word to remove
            index = charToIndex(letter)
            if not current.children[index]:
                #The word is not in the trie
                return False
            #make the current variable the element at the index of the children
            current = current.children[index]

        #reached the end of the word
        #check if the node is an end of a word

        if current.endword == False:
            return False
        else: # current.endword == True:
            #set the endword to false
            current.endword = False

        #check if there are children
        if current.haschildren:
            #may need to delete stack in other languages
            return True
        #if there are children we leave them
        #if there are no children, all references in the list are set to None
        #Python will clean up in garbage collection, we don't need to delete. #In other languages this will have to be handled

        #if there are not children we can go up and clean up the previous nodes
        #and delete until we reach the prefix for another word
        
        while current.endword == False and current.haschildren == False:
            prevLetter, prevNode = stack.pop()
            #make the reference to the node None
            index = charToIndex(prevLetter)
            prevNode.children[index] = None
            #update prevNode's has children in case you deleted the last child
            prevNode.updateChildren()
            #make the prevNode the current
            current = prevNode
```

## Full code with driver code

```python
class Stack:
    def __init__(self):
        #create empty list
        self.items = []
    
    #accepts a tuple of the letter and the node
    def push(self, letter, node):
        #append new item to the list
        self.items.append((letter, node))
    
    def pop(self):
        #removes item
        return self.items.pop()
    
   
#class for the Trie Nodes
class TrieNode:
    def __init__(self):
        self.children = [None]*26
        self.endword = False
        #this limits having to iterate over every option if there are no children.
        self.haschildren = False
    
    def updateChildren(self):
        for c in self.children:
            if c != None:
                self.haschildren = True
                return True
        self.haschildren = False
        return False

#class for the Trie
class Trie:
    def __init__ (self):
        self.root = TrieNode()
    
    #insert into trie
    def insert(self, word):
        #create current variable to traverse 
        current = self.root
        #traverse the word 
        for letter in word:
            index = charToIndex(letter)
            #if not present in the children list insert it
            if not current.children[index]:
                #create the new trie node for the element at the index
                current.children[index] = TrieNode()
                #update haschildren
                if current.haschildren == False:
                    current.haschildren = True
            #update the current variable to the new element
            current = current.children[index]
        #mark the last node as a leaf/end of the word
        current.endword = True

    #search trie
    def search(self, word):
        # current variable set to root 
        current = self.root

        #iterate over the trie to look for the word
        for letter in word:
            #get the index from the charToIndex function
            index = charToIndex(letter)
            #look at the element at index in the children list, if it doesn't exists the word is not in the list
            if not current.children[index]:
                return False
            #update the current variable to the element at the last index
            current = current.children[index]
        #return the endword attribute (True or False) The word is in the list if it is set to True
        return  current.endword 

    #remove from the trie
    def remove(self, word):
        #create current variable to traverse 
        current = self.root
        #create stack object to hold nodes 
        stack = Stack()
        #iterate through trie 
        for letter in word:
            #push element onto the stack to keep track of the previous nodes
            stack.push(letter, current)
            #get the index of the letter of the word to remove
            index = charToIndex(letter)
            if not current.children[index]:
                #The word is not in the trie
                return False
            #make the current variable the element at the index of the children
            current = current.children[index]

        #reached the end of the word
        #check if the node is an end of a word

        if current.endword == False:
            return False
        else: # current.endword == True:
            #set the endword to false
            current.endword = False

        #check if there are children
        if current.haschildren:
            #may need to delete stack in other languages
            return True
        #if there are children we leave them
        #if there are no children, all references in the list are set to None
        #Python will clean up in garbage collection, we don't need to delete. In other languages 
        #this will have to be handled

        #if there are not children we can go up and clean up the previous nodes
        #and delete until we reach the prefix for another word
        
        while current.endword == False and current.haschildren == False:
            prevLetter, prevNode = stack.pop()
            #make the reference to the node None
            index = charToIndex(prevLetter)
            prevNode.children[index] = None
            #update prevNode's has children in case you deleted the last child
            prevNode.updateChildren()
            #make the prevNode the current
            current = prevNode
            
def charToIndex(ch):
    return ord(ch)-ord('a')

def main():
    #input keys a through z lower case
    keys = ["top", "tops", "topside"]

    #when the functions return, it will return either False(0) or True(1)
    output = ["Not present in Trie", "Present in Trie"]

    #Create a Trie object
    trie = Trie()

    #Make a trie with the keys
    for word in keys:
        trie.insert(word)
    
    print("Looking for a word not added to the trie")
    print("The : {}\n".format(output[trie.search("the")] ))
   

    print("Looking for a word that is in the trie")
    print("topside : {} \n".format(output[trie.search("topside")] ))

    print("Looking for a word that is in the trie")
    print("top : {}\n".format(output[trie.search("top")] ))

    print("Looking for a word that is in the trie")
    print("tops : {}\n".format(output[trie.search("tops")] ))
    print()

    print("Looking for a word not added to the trie")
    print("topper : {}\n".format(output[trie.search("topper")] )) 
    print()

    print("Attempting to Remove a word not added to the trie")
    trie.remove("cops")
    print("cops : {}\n".format(output[trie.search("cops")] ))

    print("Inserting a new word")
    trie.insert("taper")
    print("taper : {}\n".format(output[trie.search("taper")] ))

    print("Inserting a new word")
    trie.insert("tape")
    print("tape : {}\n".format(output[trie.search("tape")] ))

    print("Removing the word taper \n")
    trie.remove("taper")
    print("taper : {}\n".format(output[trie.search("taper")]))

    print("Searching to make sure tape still is in the tree after removing taper \n") 
    print("tape : {}\n".format(output[trie.search("tape")] ))

    print("Inserting the word taper again") 
    trie.insert("taper")
    print("taper : {}\n".format(output[trie.search("taper")] ))

    print("Inserting the word tapering")
    trie.insert("tapering")
    print("tapering : {}\n".format(output[trie.search("tapering")] ))

    print("Removing the word taper again after adding another word to it \n")
    trie.remove("taper")
    print("taper : {}\n".format(output[trie.search("taper")] ))

    print("Searching to make sure tapering still is in the tree after removing taper \n")
    print("tapering : {}\n".format(output[trie.search("tapering")] ))

    print("Inserting the word tapestry")
    trie.insert("tapestry")
    print("tapestry : {}\n".format(output[trie.search("tapestry")] ))

    print("Removing the word tape \n") 
    trie.remove("tape")
    print("tape : {}\n".format(output[trie.search("tape")] ))

    print("Checking to make sure tapering and tapestry are still in the try \n")
    print("tapering : {}".format(output[trie.search("tapering")] ))
    print("tapestry : {}".format(output[trie.search("tapestry")] ))

    print("Removing the word topside \n") 
    trie.remove("topside")
    print("topside : {}\n".format(output[trie.search("topside")] ))

    print("Looking to make sure tops is still in the trie")
    print("tops : {}\n".format(output[trie.search("tops")] ))
    print()

if __name__ == "__main__":
    main()
```
