# Lab 6B Singly Linked List

---

*Note: please use validation in your code

### **Exercise 1**

Implement a **swap_node** method to the singly linked list. (Change data or change next attribute)

```python
def swap_nodes(self, key1, key2):
    pass
```

### **Exercise 2**

Implement a **reverse** method to the singly linked list that will reverse the list.

### **Exercise 3**

Modify **delete** to find the data you want to delete rather than an index. Modify **delete** to take in either an index or data.

### **Exercise 4**

Implement a **count_occurrences** method that keeps track of how many times an item appears in a list

---
