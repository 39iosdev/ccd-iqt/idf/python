## Object Oriented Programming

### **Introduction:**
This lesson you will walk through Object Oriented Programming, or OOP for short, which bundles attributes and methods into individual objects. Here you will learn how to create and use objects using Object Oriented Programming principles.

### **Topics Covered:**

* [**Tenets of OOP**](tenets_oop.html)
* [**Class Structure**](class_structure.html)
* [**Creating an Instance**](instance_creation.html)
* [**Getters/Setters**](getters_setters.html)
* [**Inheritance**](parent_child_classes.html)
* [**Polymorphism**](overloading_overriding.html)
* [**Composition**](composition.html)
* [**Modules**](modules.html)
* [**Packages**](packages.html)
* [**Exceptions**](exceptions.html)
* [**Design Principles**](design_principles.html)
* [**OOP Terminology Review**](oop_terminology.html)


#### To access the Object Oriented Programming slides please click [here](slides)

