
### Inheritance

# Structure of a Class

To understand object oriented programming, a good place to start is with the concept of inheritance and to do that, we need to learn how to build a class.  A brief example is provided below:

```python
class Person(object):
    """ A sample of what a class looks like."""
    first_name = "James"
    last_name = "Bond"

    def print_name(cls):
        print(f"My name is {last_name}, {first_name} {last_name}")
```

* To create our class, we use the keyword **class**
* This is followed by the class name, which starts with a capital letter to comply with PEP8 standards.
* We'll cover inheritance a bit later in the lesson, but we extend explicitly from object.
* A docstring is then provided next, to gives a brief explanation of our class.
* Next, we have some **class variables** (first_name, last_name), which will be shared among all instances of class Person.
* After the class variables, we have a class method print_name and again all instances of class Person will have access to it.

<br />

* To use our class, we have to instantiate an object of our class Person.

```python
james_bond = Person()
james_bond.print_name()
Person.first_name = "Daniel"
Person.last_name = "Craig"
james_bond.print_name()
```
* After the james_bond object is been created, it holds a copy of the Person class, along with access to the class variables and methods
* To access these variables and methods, we use dot notation
* On line 3 and 4, we directly access the class variables of Person and modify them.  These changes will applied to all instances of Person, as long as that object has not changed the class variable first.
* If you change a class variable using the specific object, a new instance variable is made for that object.
```python
>>> james_bond = Person()
>>> james_bond.first_name
James
>>> Person.first_name = "Daniel"
>>> james_bond.first_name
Daniel
>>> james_bond.first_name = "James"
>>> james_bond.first_name
James
>>> Person.first_name = "Daniel"
>>> james_bond.first_name
James
```

These elements are just the basic outlines of a class.  To really make use of classes, we need to go over the concepts of `__init__()` and `self`.

### Reference: 
  * [PyDocs on Classes](https://docs.python.org/3.8/tutorial/classes.html)
  * [Python Textbook Docs](https://python-textbok.readthedocs.io/en/1.0/Classes.html)