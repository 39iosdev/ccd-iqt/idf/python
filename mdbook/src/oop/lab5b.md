# Lab 5B

<img src="../assets/ironman.jpg" width="500">

## Instructions

* Using the SuperHero Class from the previous lab, update with the following additions:
  * Create a generic Person class
  * Redesign your SuperHero class to inherit from the Person class
  * Refactor code where needed
  * Utilize proper encapsulation
  * Include user input, with input validation
  * Utilize methods for getters and setters
  * Utilize a `__init__()` function
  * Create a few instances of your class. Populate it with data utilizing an init and/or getters and setters

## Requirements

* Adhere to [PEP8](https://www.python.org/dev/peps/pep-0008/)  (comments, formatting, style, etc)
* Make sure to include your student docstring at the top (name, lab, date)
* Follow instructions above

## Bonus

* Split your classes into separate files and import them properly
* Expand this program into a game or larger program. Some possible ideas:
  * Hero vs Villain
  * Battle Royale
  * Guess that Hero
---