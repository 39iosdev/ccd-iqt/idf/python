# Lab 5C

<img src="../assets/supra.jpg" width="500">

## Note about Composition vs Inheritance

* Remember, composition is a **has a** relationship vs a **is a** relationship. A **what it can do** vs **what it is**. 
  * A dog can eat, run, bark, etc.
  * A cat can eat, run, meow, etc.
  * **VERSUS**
  * A pickup truck has 2 axles, an engine, brakes, windows, etc.
  * A semi-truck has 3 axles, an engine, brakes, windows, etc.
  * A sedan has two row seating

## Instructions

* Create some vehicle types (start with just a few), pickup truck, semi truck, sedan, sports car, exotic, van, crossover, SUV, etc 
  * Create the list of vehicle components, some as classes (get creative here, think of things some vehicles have that others don't):
    * Axles (how many, what type)
    * Seating (#passengers, upholstery)
    * Engine
      * Gas (MPG, tank capacity)
      * Electric (charge consumption)
      * Hybrid
    * Transmission
    * Towing capacity
    * Sport ability
* Create instances of different types of vehicles:
  * Pickup Truck: Ford F150, Chevy Siverado, etc
  * Semi Truck: International, Peterbilt, Volvo
  * Sedan: Toyota Corolla, Honda Civic
* Make use of proper user class OOP Principles
* Make use of input validation, file usage, packages, user modules

## Requirements

* Adhere to [PEP8](https://www.python.org/dev/peps/pep-0008/)  (comments, formatting, style, etc)
* Make sure to include your student docstring at the top (name, lab, date)
* Follow instructions above

## Bonus Features

* Allow users to create objects of cars, selecting from a menu of selections to narrow down what type of class to make. 
* Save the user vehicles into a file, and allow the users to view, delete, add, or modify them
* Convert it to a racing game
---