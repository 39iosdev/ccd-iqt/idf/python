
### Inheritance | Polymorphism

# Method Overloading

Previously, when we talked about the concept of Polymorphism, two ways of implementing it were mentioned: method overloading and method overriding.  We'll briefly go over the concept of method overloading here, but Python doesn't really have a need to make use of it (this will be explain why in a moment).  Method overloading involves using the same name for a function, but having a different set of arguments for each version of the function.  In Java, to print out to the console, we would use `System.out.println()` (don't worry, this is only to explain the concept of method overloading). Each different variable type has a version of `.println()`, so if we wanted to print out a string, the `System.out.println(string)` version of the function would be called.  Alternatively, to print out an int, the `System.out.println(int)` version would be called, and these functions ***are not*** strictly the same function.

In Python, however, the type of argument passed to the `print()` function is handling within the function itself; there is no need for a separate function for each type, and with the use of default values for arguments, no need for different versions of the function for different sets of parameters.  There are ways to emulate the behavior of method overloading within the language ([Python Method Overloading](https://www.geeksforgeeks.org/python-method-overloading/) is a possible starting point); however, for the purpose of the course, we'll not going to focus on this facet of Polymorphism.

## Method Overriding

The facet we *will* focus on is the concept of method overriding.  Whereas the name of a function is shared between different versions of function to handle different types and sets of parameters for method overloading, method overriding relates to how methods work between a parent and child class.  As mentioned in the previous page, when a child class inherits from a parent class, it can inherit both variables *and* methods, defined in the parent class.  Both `__str__()` and `salary()` are defined in the Teacher class and are inherited by the PhysicsTeacher class.  In both cases, the PhysicsTeacher overrides the functionality of the method provided by the parent Teacher class, and gives its own implementation.  When an object calls one of its methods, if the class inherits from a parent class, there is a process to figuring out *which* version of method should be called, if there are multiple version of that method.  Using the Teacher and PhysicsTeacher classes from before:
```python
class Teacher(object):
    def __init__(self, name, college):
        self.name = name
        self.college = college

    def __str__(self):
        return f"{self.name} teaches at {self.college}"

    def salary(self):
        print(f"{self.name} is paid $63,645.")
    
    def college_info(self):
        print((f"{self.name} works at {self.college}"))

class PhysicsTeacher(Teacher):
    def __init__(self, name, college, specialty):
        Teacher.__init__(self, name, college)
        self.subject = "Physics"
        self.specialty = specialty

    def __str__(self):
        return super().__str__() + f", teaching the subject {self.subject} with a focus on {self.specialty}"

    def salary(self):
        salary_value = ""
        while not salary_value:
            salary_value = input(f"What is the salary for {self.name}?")
            try:
                self.salary_value = int(salary_value)
            except ValueError:
                print("That is not a valid value for the salary.  Please try again.")
                salary_value = ""

jerry = PhysicsTeacher("Jerry", "State", "Asteroids")
print(jerry)         # Example 1
jerry.salary()       # Example 2
jerry.college_info() # Example 3
```

Here, we've setup an object, jerry, as an instance of the PhysicsTeacher class.  In example 1, the `__str__()` for the PhysicsTeacher class will be looked for first, and because it has one defined, that `__str__()` gets called.  Because the method then uses the `super()` call, we go up to the Teacher class, and call its `__str__()` method.  That string gets returned, concatenated with the string in the PhysicsTeacher `__str__()` method, and then printed.  So, in our first example, we immediately find the method we need.

In the second example, `salary()`, again, PhysicsTeacher has an implementation of its own of `salary`.  This time, we don't need to access parent class Teacher's `salary()`, as the child class doesn't call its parent classes method.

For the third example, PhysicsTeacher doesn't have its own `college_info()` method, so the method call will try going up the chain, and checks the parent class, Teacher.  Teacher does have the method, and so that method is the one invoked.

For the most part, within Python, for aspects of Polymorphism, method overriding is what you'll be dealing with.  There are also further complexities, namely, what if your child class inherits from multiple parents or what if you have multiple levels of inheritance (a good starting point is here [Python Method Overriding](https://www.geeksforgeeks.org/method-overriding-in-python/))

### Reference: 
  * [PyDocs on Classes](https://docs.python.org/3.8/tutorial/classes.html)
  * [Python Textbook Docs](https://python-textbok.readthedocs.io/en/1.0/Classes.html)
  * [Composition and Inheritance](https://www.thedigitalcatonline.com/blog/2014/08/20/python-3-oop-part-3-delegation-composition-and-inheritance/)
