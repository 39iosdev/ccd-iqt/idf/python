
# Tenets of Object Oriented Programming

So far in Python, the course has covered how to use various data types, how to control the flow of code execution and how functions can be built to tie code together, that can be reused.  

When programming started, as a field, programs were very much built with the mindset of, use code as it is currently being executed, i.e. I need a variable to do this next set of operations, I declare it now.  This, however, lead to a lot of code that was repeated, over and over, in a program.  

To alleviate this, many programming languages moved to a paradigm based around grouping code blocks that work as one into a reusable block.  Going by various names (functions, procedures, methods, subroutine, etc.), the concept ultimately is that code that needs to be repeated should be in a central location and should have some sort of name, to be called.

A paradigm that was developed to take this grouping of code a bit further is object oriented programming (OOP).  In addition to reusing blocks of code, OOP focuses on structuring an application around declared data types (objects) and behaviors associated with those objects (methods).  This paradigm allows for a broad design, that is specific for the needs of the developed application.  To accomplish this, object oriented programming has four pillars:

## Encapsulation
---

This pillar is one that the course has covered already, but now we'll apply directly to OOP.  By developing our own functions, with their own set of variables, we've built code blocks that can be reused in different programs easily.  In addition, once we go over the concept of modules and packages later in this lesson, future developers that use our code won't need to see the full code block, just what is required as arguments, what the function is called and what the function returns.  This sub-concept of ***data hiding***, allows for easy portability, provided good documentation is also provided.

## Abstraction
---

Related to **encapsulation**, abstraction makes only high level public (a term we'll discuss when we get to the **inheritance** lesson) methods available to developer for use in programs.  While hiding code is a result of encapsulating our code, abstraction starts with the goal that anyone who using our code will only see what is publicly available.  The complex inner workings are privy to only the team that does the direct development of that code, or those who really need (or want) to understand what the code does.  Abstraction and encapsulation work together as two sides of the same coin.

## Polymorphism
---

Many different objects can share the same behavior, but implement that behavior in different fashions.  We call this sharing of behaviors **polymorphism**, and this can be executed in one of two ways, method overriding and method overloading, which we'll cover that when we get to that topic.

## Inheritance
---

The last pillar, and a pretty big thrust of object oriented programming, is inheritance.  This concept centers around developing parent classes that are more generic in nature and then building child classes that extend the parent classes and flesh out specific features.  Being a fairly important part of OOP, we'll be covering this topic first.