
### Inheritance

# Creating an Instance

To effectively use a Class, we need to learn how declare instances with initial values and how to access the values unique to each object of a Class.

## `self`

To be able to access the values assigned to an individual instance of a class, Python uses the keyword `self`.  A class may have variables that all instances of the class might influence (for example, a class might have a counter to keep track of how many instances exist within the application), but typically, as a developer, we'll be more interested in the values assigned to a particular instance.
```python
class Person(object):
    first_name = "James"
    last_name = "Bond"

    def change_name(self, fname):
        self.first_name = fname
```

In the example above, the class has two class variable for each name, but when we call the change_name() function, the first_name related to the specific object is changed, not the first_name related to the class itself.
```python
>>> daniel = Person()
>>> daniel.first_name
James
>>> daniel.change_name("Daniel")
>>> daniel.first_name
Daniel
```
When the method is called, the function call implicitly passes the self value to the method, based on the object that is being called on.  One of the most issues when using classes and the methods tied to the class, is forgetting self as a parameter, if you want to access the values associated with the object.

We'll be using the `self` keyword to modify values related to each object, first using the `__init__()` method to setup the object.

<br />

## `__init__()`

Typically, when we're creating objects of a class, that object will need some initial values stored and this process uses a **constructor**.  In Python, we use the `__init__()` function as the constructor of objects.
```python
class Person(object):
    def __init__(self, fname, lname):
        self.first_name = fname
        self.last_name = lname

daniel = Person("Daniel", "Craig")
print(f"My name is {daniel.last_name}, {daniel.first_name} {daniel.last_name}")
```

In the example, `self` refers to the specific instance or object of Person, fname and lname are the two arguments passed when the Person object is created.  When the Person object is created, we don't have to pass `self` as an argument as well, since that is implicitly passed to the `__init__()` method.  When we access the variables in the print function, we are calling the variables specific to the instance/object, so these are **instance variables**.

### Reference: 
  * [PyDocs on Classes](https://docs.python.org/3.8/tutorial/classes.html)
  * [Python Textbook Docs](https://python-textbok.readthedocs.io/en/1.0/Classes.html)