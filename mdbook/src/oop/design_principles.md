
# Design Principles of OOP

Remember that in object oriented programming, code is designed to be focused on the data used, instead of centered on functions.  There are some design principles that we can use to help us with the design of our applications, and these can be summed up in the acronym **SOLID**.

* **S - Single Responsibility Principle**
  * "A class should have one, and only one, reason to change." -Robert C. Martin
  * When building our classes, we acknowledge that requirements can (and will likely) change as we develop our application.  By narrowing the focus of our classes, when a change is requested and the list of responsibilities for our class is small, implementing the change will be less complicated, have less side-effects, and need less work.

* **O - Open/Closed Principle**
  * "Software entities (classes, modules, functions, etc.) should be open for extension, but closed for modification." -Bertrand Meyer
  * Essentially, when we write our classes, we should write them in a way that doesn't require a complete rewrite of the class if we add new functionality later.  If we don't follow this principle, if we make a major modification to a class, any dependent class will also need to be looked at.  If we simply extended the class, dependent code can continue to operate as normal.

* **L - Liskov Substitution Principle**
  * "Let Φ(x) be a property provable about objects x of type T. Then Φ(y) should be true for objects y of type S where S is a subtype of T." -Barbara Liskov
  * Outside of the mathematical definition, this principle outlines that objects of the type of a superclass (the parent class) should be interchangeable with objects of the type of a subclass (the child class) without the application breaking.

* **I - Interface Segregation Principle**
  * "Clients should not be forced to depend upon interfaces that they do not use." -Robert C. Martin
  * Similar to the Single Responsibility Principle, when designing classes that will be depended upon by other classes, the superclass should (as much as possible) not have methods that its subclasses will not use.  By doing so, again, we simplify our code and reduce the chance that any dependencies will cause issue with our code.

* **D - Dependency Inversion Principle**
  * "High-level modules should not depend on low-level modules. Both should depend on abstractions. AND Abstractions should not depend on details. Details should depend on abstractions." -Robert C. Martin
  * At its core, each component of your code, when it is interacting with another component, should be doing so through an abstracted medium.  This is done, again, to reduce dependencies and simplify where changes in code might have an effect.

A good initial set of resources to expand upon these basic definitions would be the wikipedia entry for [SOLID](https://en.wikipedia.org/wiki/SOLID) and also Stackify's articles on each concept, starting with [Single Responsibility Principle](https://stackify.com/solid-design-principles/).

# Miscellaneous Object Oriented Concepts

* When functions are inside of a class, these functions are called `methods`, with both having the same functionality.
* Keeping your functions small and simple is a good first rule when working with classes.
* Remember that your `__init__()` is there to handle the initial setup of your object.  Try not to do too much in it, it makes the `__init__()` harder to use.
* Remember to make use of [PEP8](https://www.python.org/dev/peps/pep-0008/) standards when naming your functions, variables, classes, etc.
* Consistency in the way you order your parameter list for your functions, as a baseline, can greatly assist with usage and debugging of your code.
* Vertical spacing can assist with code readability.  Not everyone has same amount of horizontal spacing for their development machine, but all machines can keep vertical scrolling (and vertical scrolling is typically easier to do than horizontal spacing).
* Commenting should be done.  Additionally, describing *why* the code does what it does is more important than *how*.  The code itself should describe how it performs (unless it does something necessarily complex), your comments should be a guide post as to why you implemented the code this way.
* There is a balance, though, to commenting.  Keep in mind having a fellow programmer (if you're on a team) take a look at your code is always helpful.  The comments will also need to be maintained (for instance, if the function is dramatically changed, the comments will need to be, too).  If you only have the needed amount of comments, that is less text that needs to be maintained.
