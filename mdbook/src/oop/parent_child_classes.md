
### Inheritance 

# Parent and Child Classes

When we defined inheritance, we mentioned the concept of there being parent and child classes.  The idea being, we might need to develop a generic class that can be extended in different ways.  For example, using the animal kingdom, humans share certain attributes with other primates, so we could consolidate those common traits into a generic Primate class.  Each of the different species of primate would then expand upon the default traits of parent Primate class.  Also like the taxonomy tree, we can continue up each level, creating a new common parent class, which can then be extended by the child classes below it.

<img src="../assets/animal_tree.jpg" width="500">

If we use the image above as an example, Ursus is a parent class to both arctos and horribilus, which are child classes to Ursus. Below, we'll go over how this is implemented with Python.
```python
class Teacher(object):
    def __init__(self, name, college):
        self.name = name
        self.college = college

    def __str__(self):
        return f"{self.name} teaches at {self.college}"

    def salary(self):
        print(f"{self.name} is paid $63,645.")

class PhysicsTeacher(Teacher):
    def __init__(self, name, college, specialty):
        Teacher.__init__(self, name, college)
        self.subject = "Physics"
        self.specialty = specialty

    def __str__(self):
        return super().__str__() + f", teaching the subject {self.subject} with a focus on {self.specialty}"

    def salary(self):
        salary_value = ""
        while not salary_value:
            salary_value = input(f"What is the salary for {self.name}?")
            try:
                self.salary_value = int(salary_value)
            except ValueError:
                print("That is not a valid value for the salary.  Please try again.")
                salary_value = ""
```

In this example, Teacher is our parent class, with PhysicsTeacher as a child class.  The PhysicTeacher will *inherit* from Teacher the use of the name and college variables, will modify the `__str__()` method and will override the salary method.  When inheritance is used in larger program, far more variables and methods would be inherited, but for brevity, this example is short.  Items to note from the example:

* The parent class Teacher lists some variables and methods that will be available for all its child classes.  When we are designing our classes and using inheritance, we want to consolidate all the common elements as high up on the parent-child chain as we can.  This reduces repeated code (one of the reasons we're using inheritance in the first place).
* Within the `__init__()` method for our PhysicTeacher class, the name and college variables are passed as arguments to the Teacher's `__init__()` method.  This is one way for us to access the parent class's resources, so that the child class can pass items up the chain.  With using `Teacher.__init__()`, we have to pass `self` as the first argument, so that we working with the specific object's variables and such.
* When we call PhysicTeacher's `__str__()` method, instead of calling `Teacher.__str__()`, we use the `super().__str__()` method call.  With only one parent class, these two methods are equivalent.  With the `super()` method call, the `self` keyword is implicitly passed, so no need to have it as one of the arguments.
  * Within Python, it is possible for a child class to inherit from multiple parent classes.  If the child class does so, it cannot make use of `super()`.  When a child class has multiple inheritances, it will use the format `class Child(Parent1, Parent2, etc.....)` for its first line.
* With the `salary` method, the PhysicTeacher class has completely overwritten it.  Sometimes it is not possible to reuse the Parent class's implementation of a method, but the name needs to be reused.  In this case, no call is made to the Parent's version of the method.  We'll cover this a little further when we go over the Polymorphism concept of method overriding.

### Reference: 
  * [PyDocs on Classes](https://docs.python.org/3.8/tutorial/classes.html)
  * [Python Textbook Docs](https://python-textbok.readthedocs.io/en/1.0/Classes.html)

**Continue to Performance Lab:** 5B
