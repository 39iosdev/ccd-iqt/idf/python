# Lab 5A

## Instructions

* Create a very simple super hero class. Some attributes you will need:
  * Hero Name
  * Real Name
  * Power(s)
  * Color(s)
* Make sure you utilize an `__init__()` function
* Ensure variables are correct type (class vs instance variables)
* Utilize methods within your class:
  * Start to format your class using getters and setters
* Create an instance of your class. Populate it with data, utilizing an `__init__()` and/or getters and setters

## Requirements

* Adhere to [PEP8](https://www.python.org/dev/peps/pep-0008/)  (comments, formatting, style, etc)
* Make sure to include your student docstring at the top (name, lab, date)
* Follow instructions above
---