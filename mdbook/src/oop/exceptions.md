
## Exceptions

When we run code that has the possibility that to product an error (a user inputs a string, when the code expects an int, for example), it is often useful, and at times required so that the application doesn't crash, to capture that error and process it in some fashion.  Structure of the code block consists of:
* **try**:
  * This section will hold the section of code that might produce the error or code that might rely on the previous code succeeding.  To optimize your code, it might be a good idea to limit the code in the try block to only what needs to be tested.
* **except**:
  * Here, if an error/exception was raised in the try block, then an except block will be looked for that can handle that specific type of error.  Best practice with using try/excepts is to think about the actual errors that might be produced by your code and handle it accordingly.  If your code could produce several different errors, you can include multiple except statements, with specific handling code for each error.
* **finally**:
  * Once the error(s) is/are taken care of, a finally block can be used to do some closing actions, especially if your try blocks account for multiple different errors.  There may need to be a generic line of code executed after any error handling is done, and this can be done in the finally block.  Per the python documentation, 'If a finally clause is present, the finally clause will execute as the last task before the try statement completes. The finally clause runs whether or not the try statement produces an exception.'
* **else**:
  * Lastly, just like with if statements, while and for loops, you can make use of an else block, after your try block.  An else block will execute only if an error **was not** raised.
* **raise**:
  * We can also raise our own Exceptions by using the `raise` keyword.  `Raise` behaves similar to `return`, except that an Exception gets returned, based on the type of exception provided after the `raise` keyword.

**Example 1**

```python
try:
    <statements>
except:
    <statements>
except <name> as <data>:
    <statements>
finally:
    <statements>
else:
    <statements>
```

**Example 2:**

```python
"""
Checks a certain range of numbers to see if they can divide into a user specified num
"""
# Program main, runs at start of program
def launch():
    num = input('What number would you like to check?')
    amount = input('How many numbers do you want to check?')

    if isInt(num) == False or isInt(amount) == False:
        print("You must enter an integer")
        launch() 
    elif int(amount) < 0 or int(num) < 0:
        print("You must enter a number greater than 0")
        launch() 
    else:
        divisible_by(int(num), int(amount))

# Checks num divisible numbers up to amount or itself
def divisible_by(num, amount):
    i = 1.0
    while (num / i >= 1 and amount > 0):
        if num % i == 0:
            print('{} is divisible by {}'.format(int(num), int(i)))
            amount -= 1
        i += 1

# EXCEPTION HANDLING
def isInt(x):
    try:
        int(x) ###
        return True
    except ValueError:
        return False

launch()
```
<br />

A list of errors that are built into Python by default can be found here [Python Error Types](https://docs.python.org/3/library/exceptions.html).  A nice chart with the errors can be found here [Python Errors and Built-in Exceptions](https://www.programiz.com/python-programming/exceptions).

It is also possible to build your own custom errors/exceptions.  An example is provided below.

```python
 
class CustomError(Exception):
    def _init_(self, msg):
        self.msg = msg
         
    def _str_(self):
        return "your error is {}".format(self.msg)
         
    def doStuff(danger):
        if danger == True:
            raise CustomError("Whoa don't do that!")
            print("Success") #What happens here?
 
try:
    doStuff(True)
    
except CustomError as stuff:
    print(stuff)
    
```
