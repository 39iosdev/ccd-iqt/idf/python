
### Encapsulation

# Composition

Another way that we can organize the usage of classes, is through decomposing the different objects our program might use.  Instead of trying to build some sort of hierarchy, so that all of the classes that we'll use are in a tree structure, we separate the classes into distinct groups.  For example, a desktop computer has several distinct objects (motherboard, processor, graphics device, possibly optical drive, etc).  Each of these objects could have their own set of variables and methods, but we don't necessarily have to have any of classes inherit from any of the others.
```python
class Processor():
    def __init__(self, name, speed, cores):
        self.name = name
        self.speed = speed
        self.cores = cores
    ...

class Motherboard():
    def __init__(self, name, ports):
        self.name = name
        self.ports = ports
    ...

class GraphicsCard():
    def __init__(self, name, memory, features):
        self.name = name
        self.memory = memory
        self.features = features
    ...

class Computer():
    def __init__(self, processor, motherboard, graphics):
        self.processor = processor
        self.motherboard = motherboard
        self.graphics = graphics
    ...

i9 = Processor("I9-11900KF", 5.3, 8)
rog_strix = Motherboard("ROG Strix Z590-E Gaming WiFi", ["HDMI", [8, "USB-A"], [2, "USB-C"]])
gf_3080 = GraphicsCard("MSI Gaming GeForce RTX 3080 OC Ventus", 1740, ["RTX", "DLSS"])
my_computer = Computer(i9, rog_strix, gf_3080)
```

In the example above, we have a basic outline of what a Computer class could be composed of.  In this case, 3 classes, that will do separate tasks and contain different sets of data, together will represent a Computer object.  For brevity, only some of the variables were included, but if we were build this out, a Computer class would likely have a set of optional variables (many computer no longer have optical drives, but all computers need some sort of power supply).  The Computer class would also have methods (turning the computer on, anyone?).  Each of the composite classes would also likely have some additional variables and methods.

The key difference between the relationship of classes in inheritance versus in composition relates to being either a ***is a*** relationship or a ***has a*** relationship.  In inheritance, the child class ***is a*** type of the parent class.  In composition, a class ***has a*** group of composite classes.  A takeaway about these two approaches of building your classes is their use doesn't have to be an either or situation.  Using our Computer example from before, the Processor and GraphicsCard classes could share a lot of attributes (both of them have a name, speed, etc.), and so building a parent class to both would not be out of the question.  The two approaches are quite complementary to each other.

### Reference: [Composition and Inheritance](https://www.thedigitalcatonline.com/blog/2014/08/20/python-3-oop-part-3-delegation-composition-and-inheritance/)

**Continue to Performance Lab:** 5C  