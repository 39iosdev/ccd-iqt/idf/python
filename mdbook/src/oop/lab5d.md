# Lab 5D

## Instructions

* Using the calculator program created from Lab 4C, split up the functionality into modules and utilize packaging. Some things you could split up:
  * The user menu into it's own module on a higher level package
  * Operations into one module, lower level
  * Algorithms into one module, lower level

## Requirements

* Adhere to [PEP8](https://www.python.org/dev/peps/pep-0008/)  (comments, formatting, style, etc)
* Make sure to include your student docstring at the top (name, lab, date)
* Follow instructions above
---