
### Encapsulation | Abstraction

# Packages

Packages are a way to structure Python's module namespace. Essentially a package is just a collection of modules. A module named `__init__.py` is required to treat the directory as a package; allowing the package to be imported the same way a module can be imported. This file can contain initialization code, but it's often best to leave it blank.

## Package Structure

```python
/                       # Top directory
    operations/         # Package
        __init__.py     # special file
        pemdas.py       # PEMDAS module 
        shapes.py       # operations related to shapes module 
        logarithm.py    # logarithm module
    menu/               # additional package 
        __init__.py 
        uilook.py       # UI used for the calculator
```

### Importing From a Package

Using the package above, we have two ways of importing from the package.

```python
import menu.uilook

menu.uilook.do_something()
```

**OR**

```python
from menu import uilook as ui

ui.do_something()
```

Notice the calls to the module. They do behave differently. You can also import the entire package as well.

[More on Packages](https://realpython.com/python-modules-packages/#python-packages)

## Useful Commands

There are two useful commands that can help tell us information about objects within the current namespace:

* **dir()**
  * Returns list of names in current local scope or **with argument,** attempts to return a list of valid attributes for object
* **help()**
  * Invokes built-in help system

```python
# dir
>>> print(dir())
['__builtins__', '__doc__', '__name__', '__package__']
>>> import(struct)
>>> print(dir())
['__builtins__', '__doc__', '__name__', '__package__', 'struct']
>>> print(dir(struct))
['Struct', '__builtins__', '__doc__', '__file__', '__name__', '__package__', '_clearcache', 'calcsize', 'error', 'pack', 'pack_into', 'unpack', 'unpack_from']

# help
print(help(struct))
print(help())

```
**Continue to Performance Lab:** 5D
