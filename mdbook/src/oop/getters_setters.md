
### Inheritance | Encapsulation

# Access Modifiers

We've learned how to setup an object with some initial information, using the `self` keyword and `__init__()` method; however, what if we want to change values after the initial creation?  Before we go over the methodology for changing instance variables, we need to understand access modifiers.  In object oriented programming, to enable encapsulation, we control access to the variables and methods of a class to only those entities that need the access.

The lowest level of access in OOP is declared with the modifier *private*.  If a variable is private, then only the class that holds the variable ***should*** (we'll get to that in a moment) access the variable.  Private attributes are designated with two underscores in front of the name, so __variable_name.

The next level of access is called *protected*.  If a variable is protected, then only the class that holds the variable and its children classes (we'll also cover this shortly) ***should*** access the variable.  Protected attributes are designated with one underscore in front of the name, so _variable_name.

The highest level of access in most OOP languages and the one that allows most any entity to modify values is the keyword *public*.  If an attribute is declared public, then that attribute can be accessed anyone, anytime.  Public attributes have no modifier in front of the name, and now you may guessed how Python actually treats all attributes in the language.  In reality, all attributes in Python are public.  The underscores for private and protected are simply *guidelines* to developers that the attribute in question ***should*** be treated as such.
```python
>>> dir(str)
['__add__', '__class__', '__contains__',....,'upper', 'zfill']
```
In the above example, the beginning methods have two underscores at the front, indicating that they should be treated as private to that class, while the non-underscored methods are public (the str class has plenty of other methods, these have been omitted for brevity).  If you, as a developer, understand the purpose of the `"private"` attributes of a class, Python doesn't stand in your way to prevent you from using it.

With those access modifiers defined, we can talk about changing the instance variables.

<br />

## Getters and Setters

With the understanding of using access modifiers, in object oriented programming, we don't normally allow for free and open use of attributes of a class.  Methods are set up to allow for an interface between class attributes and users, and we call these methods either getters (*if we're getting the attribute*) or setters (*if we're setting/modifying the attribute*).  In most object oriented programming languages, the format for getters/setters is pretty straightforward:
```python
def get_value(self):
    return self.value

def set_value(self, value):
    self.value = value
```
Remember that we use the keyword `self` to represent the specific object that we're either getting a value from or setting the value of.

Within Python, however, the language offers a couple of methodologies that allows the user to use the ***object.value*** format for accessing object attributes, but still go through the getter/setter methods.

The first way is using the property class.
```python
class Person(object):
    def __init__(self, name):
        self.name = name

    # getter
    def get_name(self):
        return self.name

    # setter
    def set_name(self, name):
        self.name = name

    name = property(get_name, set_name)
```
With that last line, our Person class can now have its name attribute accessed through a getter/setter, but in the format of `object.name`.

A way that we can expand upon this and not even have to use the set/get titles for the methods is to use ***decorators***.
```python
class Person(object):
    def __init__(self, name):
        self.name = name

    @property
    def name(self):
        return self.name

    @name.setter
    def name(self, name):
        self.name = name
```
Instead of having separate names for the setters and getters, and having the additional line for setting up the property object at the bottom, we use the @property decorator to indicate the getter and @attribute.setter to indicate the setter.  This method will also allow the user to use the object.value format when wanting to access the object attribute.  (There is a third attribute method, deleter, that can be used in this same fashion, but deleters are uncommon and beyond the scope of this course).

**Continue to Performance Lab:** 5A