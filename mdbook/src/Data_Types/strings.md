
## Strings

## What is a sequence object?

A sequence object is a container of items accessed via index. Text strings are technically sequence objects.

### Sequence Object Types:

* String
* Lists
* Tuples
* Range Objects

_Each of the above support built in functions and slicing._

## Sequence Objects: Strings

**Strings are immutable!** They need to be reassigned. There are two independent types of strings:

* **ASCII:** Default strings for Python 2
* **Unicode (UTF-8):** Default strings for Python 3

To declare a string, use one of the following. There is no Pythonic way aside from keeping it as simple and clean as possible.

* 'single quotes'
* "double quotes"
* "\"escaped\" quotes"
* """triple quoted-multiline"""
* r"\"raw\" strings"

<img src='../assets/strings_new.png' width='900'>


### String Prefixes

* u or U for Unicode​
* r or R for raw string literal (ignores escape characters)​
* b or B for byte, is ignored in Python 2 since str is bytes.​
* A 'u' or 'b' prefix may be followed by a 'r' prefix.​

Yes, this stuff is old school and not very Pythonic... but it can't be helped.

### Difference between ASCII and Unicode (utf-8)

**ASCII** defines 128 characters, mapped 0-127. **Unicode** on the other hand defines less than 2^21 characters, mapped 0-2^21. It is worth noting that not all Unicode numbers are assigned yet. Unicode numbers 0-128 have the same meaning as ASCII values; but do not fit into one 8-bit block of memory like ASCII values do. Thus Unicode in Python 3 utilizes **utf-8** which allows for the use of multiple 8-bit blocks of memory.

### Byte String vs Data String

Byte Strings are simply just a sequence of bytes. In Python 2, **bytes** is an alias of str. In other words, they are used interchangeably. In Python 3 on the other hand, str is it's own type... utilizing Unicode \(utf-8\). Whereas the _bytes_ type in Python 3 is still a bytes object in ASCII; an array of integers.

​

### ASCII Strings

```python
>>> x = 'I am a string'.encode('ASCII')​
>>> type(x)​
<class 'bytes'>​

>>> print(hex(x[0]))​
0x49 # ASCII code for capital I​


>>> x = b'I am a string'
>>> type(x)
<class 'bytes'>
```

### Unicode Strings

```python
>>> x = 'I am a unicode string'
>>> type(x)​
<class 'str'>​
​
# class str is unicode in Python 3 natively​
# You can use '...' quotes "..." quotes or """...""" quotes​
```

### Slicing

Slicing allows you to grab a substring of a string. Python's indexing structure starts at 0. The only exception is when grabbing an element using a negative index. There is an example further below.

**Grabbing a specific element**

```python
>>> x = "hello world"​
>>> print(x[4]) # grabs 4th index
o
```

**Grab a range of elements**

```python
>>> x = "hello world"
>>> print(x[4:9]) # grabs 4th index up until, but not including the 9th index. 
o wor
```

**Grabbing backwards from the last element**

```python
>>> x = "hello world"
>>> print(x[-3]) # grabs the 3rd to the last ELEMENT
r
```

* Slicing with negative values start at -1, not 0​. So if we have a string (x = "test"... t= -4, e = -3 s = -2 t = -1)... Thus a -2 slice will grab the 's'.
* When slicing a range, you grab everything **UP TO** (not including) the second defined index number.

### More String Manipulation

* Try the following 

```python
>>> my_string = "Hello World!"
>>> print(my_string[0]​)
H​
>>> print(my_string[0:5])​
Hello​
>>> print(my_string[6:]​)
World!​

>>> print(my_string[-6:]) # ???​
>>> print(my_string[::2]) # ???​
>>> print(my_string  * 2) # ???​
>>> print(my_string + my_string) # ???​
```

What did you get? Try different operations to manipulate strings!

### String Special Operators

| Operator | Description |
| :---: | :---: |
| + | Concatenation |
| * | Repetition |
| [] | Slice |
| [  :  ] | Range Slice |
| [  :  :  ] | Start:End:Step |
| in | Membership - returns true if a character is in the string |
| not in | Membership - returns true if a character is not in the string |
| % | String Formatting |

### User Input

There are a few ways to capture user input in Python. The most common are **raw_input()** in Python 2 and **input()** in Python 3. It is important that you use raw_input() in Python 2, as input() has security vulnerabilities. In Python 3, input() takes user input as a string, no matter the input. On the other hand, input() in Python 2 will take user input as the type presented. This can lead to users implementing false bools and such.


```python
>>> name = input("What is your name? ")
What is your name? <user input>
>>> name
<user input>
```

As you more than likely guessed, the user input is assigned to the variable _name_ and stored as a string. From here, you can treat the variable name as any other regular string.

### Useful String Methods

* **string.upper()** : Outputs string as uppercase​
* **string.lower()** : Outputs string as lowercase​
* **len(string)** : Outputs string length​
* **'symbol'.join(list)** : Most commonly used to join list items, outputs string.​
* **string.split(symbol)** : Most commonly used to split a string up into a list of items, outputs list, using the symbol as a delimiter.​
* **string.ljust(length, fillchar)** : left justifies of text in a specified length/block of characters
* **string.rjust(length, fillchar)** : right justifies of text in a specified length/block of characters
* **string.center(length, fillchar)** : centers text in a specified length/block of characters


```python
>>> my_string = "Hello World!”"
>>> a = my_string.upper()​
>>> print(a)​
'HELLO WORLD!'​
>>> a = my_string.lower()​
>>> print(a) ​
'hello world!'​
>>> a = len(my_string)​
>>> print(a)​
12​
>>> a = my_string.split(" ")​
['Hello', 'World!']​
>>> print(my_string)​
'Hello World!'​
>>> my_string = "+".join(a)​
>>> print(my_string​)
'Hello+World!'
```

### Changing a Character

```python
>>> my_string = "Hello World!"​
>>> my_string[1]​
'e'
>>> my_string[1] = 'E'

>>> my_string.replace('e', 'E')
```

What's the outcome? How does **replace()** work?

### Join

**join()** combines sequential string elements by a specified string delimiter. If no delimiter is specified join will insert white space by default.

Syntax:

```python
str.join(sequence)
```

Example:

```python
s = "-"
seq = ("a", "b", "c") # This is sequence of strings.
print(s.join( seq ))

#Output: a-b-c
```

### Split

**split()** will breakup a string and add the data to a string array using a defined delimiter.

Syntax:

```python
str.split(str="", num=string.count(str))

#str - any delimiter, by default it is space.
#num - number of lines minus one.
```

Example:

```python
>>> x = 'blue,red,green'
>>> x.split(",")
['blue', 'red', 'green']
>>> a, b ,c = x.split(",")
>>> a
'blue'
>>> b
'red'
>>> c
'green'
```

### Additional Standard Library Functionality

* string.startswith()
* string.endswith​()
* string.find()
* string.rfind​()
* string.count​()
* string.isalnum​()
* string.isalpha()
* string.isnumeric​()
* string.strip​()
* string.capitalize()​
* string.title​()
* string.swapcase()​
  
---  
 **Continue to Performance Labs:** 2D and 2E
 
