# Lab 2I

## Instructions

* Create a program that takes input of a group of students' names and grades
* Sort the students based on highest to lowest grade. 
* Calculate and print to the console the sorted list and the average for the class.
* Make use of Dictionaries

## Requirements

* Adhere to [PEP8](https://www.python.org/dev/peps/pep-0008/)  (comments, formatting, style, etc)
* Make sure to include your student docstring at the top (name, lab, date)
* Follow instructions above

## Bonus

* Create a dictionary with the following key/value pairs: ("apples", 20), ("oranges", 25), ("bananas", 10)
* Change the value of the "apples" key from 20 to be a nested dictionary with the following values: ("Fuji", 12), ("Green", 8)
* Add the following key/value pair to the dictionary: ("Grapefruit", 6)
* Using the above dictionary, return the value for the "Fuji" key
* Using the provided unsorted dictionary, return a sorted dictionary (sorting by key):
```python
unSortedDict = {'apples': 20, 'oranges': 25, 'bananas': 10}
```
* Combine the sets below into a new set:
```python
xSet = {3,4,5,6,7,8}
ySet = {1,2,6,5,9}
```
* Show the difference between xSet and ySet, without changing the set values.
* Show the symmetric difference between the two sets, while updating xSet
* Using the sys library below, show the size of xSet
```python
import sys
```
---