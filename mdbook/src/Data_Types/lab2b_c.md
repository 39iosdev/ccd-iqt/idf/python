# Lab 2B 

## Instructions

* Using the provided file in the additional files section, modify it to align with the TODO comments

## Additional Files

* [lab2b.py](./performance_labs/lab2B_C/lab2b.py)

## Bonus Material

* Shorten the code in the file

<br />
<br />

# Lab 2C

## Instructions

* Write a program that calculates the total of an item, with taxes.
* Keep in mind that you have not learned Python formatting for print or user input. 
* Simple/ugly printing is allowed here. 
* Hard code the user input

## Lab Requirements (for both Labs)

* Adhere to [PEP8](https://www.python.org/dev/peps/pep-0008/)  (comments, formatting, style, etc)
* Make sure to include your student docstring at the top (name, lab, date)
* Follow instructions above
---