
## Bytes and Bytearray

**Bytes()**

It's worth re-mentioning that Python 2 and Python 3 have differences when it comes to bytes and strings. Python 2 strings are bytes naturally, whereas Python 3 is unicode and needs to be defined as bytes when you want to use bytes type. Here are a couple ways to turn Python 3 strings and such, into bytes. This functionality is backwards compatible with Python 2.

```python
# Method 1 (shortest)
>>> x = b'hello world'
>>> print(x[1])
101 # ASCII dec value

# Method 2 (clean)
>>> x = 'hello world'
>>> y = bytes(x, 'ascii')
>>> print(y)
b'hello world'
>>> print(y[1])
101 # ASCII dec value
```

**This can go both ways. We can convert these integer representations back into Unicode or ASCII strings.**

```python
# Python 3 back to native unicode string
>>> x = ord('e')
>>> x
101
>>> y = chr(x)
>>> y
'e'
# y is now a unicode string... but how do we turn it into a byte/ascii string?
>>> y = bytes(y, 'ascii')
>>> y
b'e'
```

## Bytearray()

**Bytearray()** is a mutable sequence of integers in range of 0 &lt;= x &lt; 256; available in Python 2.6 and later. Byte Arrays are useful when you need to modify individual bytes in a sequence. Since bytearray() takes in a byte/ASCII string... there is a difference in how we must implement this function between Python 2 and 3.

**Python 3**

Python 3 strings on the other hand need to be converted before you can pass them as an argument into bytearray().

```python
>>> b = bytearray(b"I am a string")​
>>> b​
bytearray(b'I am a string')​
>>> b[2]​
97 # decimal for 'a' char​
>>> b[2] = 85​
b​
bytearray(b'I Um a string')​
```  

---
