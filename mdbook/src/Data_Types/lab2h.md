# Lab 2H

## Instructions

* Create three tuple variables, using the different methods (parentheses, commas, and the tuple() function)
* Combine the three tuples into a new tuple variable and print the results to the console
* Remove a section/'slice' of your newly created tuple and print the results to the console
* Use the range() function to create and print to the console all numbers from 7 to 23, inclusive
* Use the range() function to print to the console all even numbers from 10 to 50, inclusive
* Create a bytearray variable that is at least 20 characters in length
* Using the memoryview() function, create a new variable pointing to the previously created bytearray variable

## Requirements

* Adhere to [PEP8](https://www.python.org/dev/peps/pep-0008/)  (comments, formatting, style, etc)
* Make sure to include your student docstring at the top (name, lab, date)
* Follow instructions above

---