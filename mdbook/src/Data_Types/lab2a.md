# Lab 2A

## Instructions

Using the Python interpreter, find the type of each of the following variables.
 
* 10​
* 10.5​
* "10"​
* "Hello!"​
* ""​
* ''
* True​
* 0​
* type​
* object​
* b"10101101"
* 0b10101101​
* [1,2,3]​
* (1,2,3)​
* {1,2,3}​
* {'one':1}​
* 5j​

## Requirements

* Adhere to [PEP8](https://www.python.org/dev/peps/pep-0008/)  (comments, formatting, style, etc)
* Make sure to include your student docstring at the top (name, lab, date)
* Follow instructions above

## Bonus

* Feel free to experiment with other variables and such.

---