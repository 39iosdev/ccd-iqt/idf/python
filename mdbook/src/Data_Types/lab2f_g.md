# Lab 2F

## Instructions

* Write a program that will be able to check if two words (strings) are anagrams.
  * An **anagram** is a word or phrase formed by rearranging the letters of a different word or phrase
* Only one user input is expected (since we have not gone over looping yet)


<br />
<br />
<br />

# Lab 2G

## Instructions

* Using the provided file in the additional files section, convert the string to a list, and then perform actions on the list, per the TODOs in the file.

## Additional Files

* [Lab2G](./performance_labs/lab2G/lab2g.py)

## Lab Requirements (for both Labs)

* Adhere to [PEP8](https://www.python.org/dev/peps/pep-0008/)  (comments, formatting, style, etc)
* Make sure to include your student docstring at the top (name, lab, date)
* Follow instructions above

<br />
<br />
<br />

## Bonus

* Extend the Lab 2F to be able to compare two sentences.
* Using the built-in map() function, iterate through the provided list below by calling the defined addition(x) function. Store the list in a new variable and print the results to the console.

```python
def addition(x):
    return x + x

number_list = [1,2,3,4,5,6,7,8,9]
```
* Use the original number_list from above and the map() function to process the number_list by multiplying each index of the list by itself and print the results to the console.
* Using the filter() function, print the values of the original number_list from above that are less than 4.
* Using the filter() function, print the values of the original number_list from above that are greater than 2 **AND** less than 7.
* Using the imported reduce() function, add the values of the original number_list from above together and print the result to the console.

```python 
from functools import reduce
```
---