# Lab 2D

## Instructions

* Write a program that asks for a string from the user, reverses that string, and then outputs the result to the console in all uppercase letters.

<br />
<br />

# Lab 2E

## Instructions

* Write a program that asks for a string from the user, then count the number of words in that sentence.  Output the result to the console.

## Lab Requirements (for both Labs)

* Adhere to [PEP8](https://www.python.org/dev/peps/pep-0008/)  (comments, formatting, style, etc)
* Make sure to include your student docstring at the top (name, lab, date)
* Follow instructions above

<br />
<br />

## Bonus

1. Get a user to input their first name, last name, and age
    * CHALLENGE: Accept the name as one string, and parse the input for first and last name
    * Modify the input to ensure the first and last name are properly capitalized, with no spaces in either string
    * Output the information to the console in the format of "Last Name, First Name (Age)"

<br />

2. Prompt for user input of an integer that is less than 256
    * Since we have not gone over error checking, assume that the value meets this criteria
    * Using a single print function call, display the input in hexadecimal, octal, and binary (using any format)
    * Using a binary operator, "flip" the 1st and 2nd bit of the user input, store this result into a new variable, and then print both the original input and the new result to the console, in binary format.

<br />

3. Prompt user to input a string and an integer.  Print the string to the console the number of times indicated by the integer.

<br />

4. Create 4 variables of different types (refer to [DataTypes](./Data_Types/variables.html))
    * Print the 4 variables to the console on 3 lines, using all three of the alignment functions. Each line should have more than 20 characters.
---