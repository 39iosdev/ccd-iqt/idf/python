# Lab 4A

## Instructions

* Write the function ```show_students()``` that takes two parameters (students, message) and prints out each name individually in the students list and then prints the message
  * Use your created function to print the students in alphabetical order
  * Use your created function to print the students in reverse alphabetical order
* Write a function that takes in a person's name and prints out a greeting
  * The greeting must be at least three lines, and the person's name must be in each line
    ```text
    Hello, <name>
    Welcome to IDF <name>
    Good luck <name>!
    ```
* Write a function that takes in a first name and a last name then prints out a nicely formatted full name in a sentence
  * Your sentence could be as simple as, "Hello, full_name."
  * Also print name in (last name, first name)

## Requirements

* Adhere to [PEP8](https://www.python.org/dev/peps/pep-0008/)  (comments, formatting, style, etc)
* Make sure to include your student docstring at the top (name, lab, date)
* Follow instructions above
---