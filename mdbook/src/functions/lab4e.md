# Lab 4E

## Instructions

* Create a function that recursively prints all the words in the_list:
```python
the_list = ['all', 'the', 'words', 'in', 'the', 'list']
```
* Create a text file with the following text:
```text
This is your first line of text
By Grapthar's hammer, you shall be avenged!
Never give-up, never surrender.
Look! I have one job on this ship. It's stupid, but I'm going to do it. Okay?
```
* Using the open function, create a file object (in read mode). Pass the file object as an argument to a newly created function that uses recursion to print each line in the file.

```python
number_list = [4, 15, 6, 344, 7, 423, 1, 10, -5]
```
* Create a function that recursively goes through the number_list and prints/returns the largest value
* Create a function that recursively goes through the number_list and prints/returns the sum of all numbers

## Requirements

* Adhere to [PEP8](https://www.python.org/dev/peps/pep-0008/)  (comments, formatting, style, etc)
* Make sure to include your student docstring at the top (name, lab, date)
* Follow instructions above
---