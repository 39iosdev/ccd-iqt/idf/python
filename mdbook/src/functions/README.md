## Functions

### **Introduction:**
This lesson takes a look into the creation and use of Python functions.

### **Topics Covered:**

* [**Scope**](scope.html#scope)
* [**User Functions**](user_functions.html#user-functions)
* [**Parameters and Arguments**](user_functions.html#parameters-and-arguments)
* [**Command Line Arguments**](user_functions.html#cmdline-arguments)
* [**Returning**](user_functions.html#returning)
* [**Pass By Reference**](user_functions.html#pass-by-reference)
* [**Lambda Functions**](lambda_functions.html#lambda-functions)
* [**List Comprehension**](list_comprehension.html#list-comprehension)
* [**Closures**](closures_iterators_generators.html#closures-iterators--generators)
* [**Iterators**](closures_iterators_generators.html#iterators)
* [**Generators**](closures_iterators_generators.html#generators)

#### To access the Functions slides please click [here](slides)

