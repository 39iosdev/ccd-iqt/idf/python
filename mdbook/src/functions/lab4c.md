# Lab 4C

<img src="../assets/doge.jpg" width="300">

## Instructions

* Create a fully functional calculator using functions (you can use lambdas for map()/reduce()/filter() functions).
* Create a user menu as well as a "screen" where the numbers and operations take place. 
* The calculator **needs** to have the following functionality:
  * Addition
  * Subtraction
  * Division
  * Multiplication
  * Exponents
  * At least two math algorithms (One can be your Fibonacci)

## Requirements

* Adhere to [PEP8](https://www.python.org/dev/peps/pep-0008/)  (comments, formatting, style, etc)
* Make sure to include your student docstring at the top (name, lab, date)
* Follow instructions above

## Bonus Features

* Some additional features that you can add to your calculator:
  * Allowing for input of more than two numbers
  * Continuous operations (5 + 5 + 2 - 1 / 2 for example)
  * Additional operations
  * Additional math algorithms
* When you get done with recursion, come back to Lab 4C to make a Fibonacci recursive function.
---