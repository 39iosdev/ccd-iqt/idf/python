# Lab 4D

## Instructions

* Using the following code and list: 

```python
fruits = ["apple", "apricot", "avocado", "banana", "blackberry", "blueberry", "cantaloupe", "grapefruit", "cherry", "kiwi", "mango"]
new_list = []

for x in fruits:
    if "a" in x:
        new_list.append(x)

print(new_list)
```
  * Create a list that doesn't include fruit that contain an 'e'.
  * Create a list with all fruit names in uppercase.
  * Create a list of fruit not spelled with an 'e', in uppercase.


## Requirements

* Adhere to [PEP8](https://www.python.org/dev/peps/pep-0008/)  (comments, formatting, style, etc)
* Make sure to include your student docstring at the top (name, lab, date)
* Follow instructions above

## Bonus

* Using dictionary comprehension, return/print the names of those older than 28
```python
my_dict = {'Nick': 37, 'Tom': 28, 'Karl': 33, 'Bill': 55}
```
* Using dictionary comprehension and the previous dictionary above, add 7 to everyone's age that is over 28 and print the results
* Using user input, ask for a string and integer, pass them as arguments to a function, add the name and age to the previous dictionary, and then output the result to the console.
---