# Lab 4B

## Instructions

* Using *args (remember * is the distinguishing factor and 'args' is just a name), 
  create a function that takes any number of integers and prints their sum
  out. Use multiple examples of different length arguments
  
* Using **kwargs (Where ** is the distinguishing factor) dictionary arguments
  Create a function that takes any number of key/value pairs and creates a
  list that stores the keys and another list that stores the values then
  print both lists to verify the output

* Using both *args and **kwargs, create a function that will take any number of
   single arguments and any number of key/value pairs and return the sum off all
   the numbers
```
functionName(1,2,3,4)
functionName(1,2,3,4,5,6,a=1,b=2,c=3,d=4)
functionName(4,5,6,b=2,d=4)
functionName(b=2,d=4)
```

## Requirements

* Adhere to [PEP8](https://www.python.org/dev/peps/pep-0008/)  (comments, formatting, style, etc)
* Make sure to include your student docstring at the top (name, lab, date)
* Follow instructions above
---