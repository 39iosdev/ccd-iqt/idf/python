
---

# Lab {#}

## Instructions

* Details related to the lab
* Notes about how the lab can be approached
* Edge cases to take into account
* Possible solutions that are not being looked for

## Requirements

* PEP8 note 'Adhere to [PEP8](https://www.python.org/dev/peps/pep-0008/)  (comments, formatting, style, etc)'
* Student information docstring 'Make sure to include your student docstring at the top(name, lab, date)'
* Line about 'Follow instructions above'

## Additional Files

* If there are files to be used by the students, list here

## Bonus Material

* If the lab is going to offer additional content to be completed, list here

---