# Lab 3A

<img src="../assets/madlibs.png" width="400">

## Background

"Mad Libs is a phrasal template word game where one player prompts others for a list of words to substitute for blanks in a story, before reading the – often comical or nonsensical – story aloud. The game is frequently played as a party game or as a pastime."
-[Wiki](https://en.wikipedia.org/wiki/Mad_Libs#:~:text=Mad%20Libs%20is%20a%20phrasal,game%20or%20as%20a%20pastime)

## Instructions

* Create your own mad libs game asking the user for input to fill in the blanks. 
* Output to the console using the .format() method.
* Create a handful of phrases that require different numbers of inputs
* Prompt the user for input(s), telling them if the word or phrase needs to be a verb, noun, etc. 
* Output the user inputs into the given phrase
* Humor is encouraged

## Requirements

* Adhere to [PEP8](https://www.python.org/dev/peps/pep-0008/)  (comments, formatting, style, etc)
* Make sure to include your student docstring at the top (name, lab, date)
* Follow instructions above

## Templates Provided

* Below are links to some Mad Libs templates.  You can copy the text from the page and insert your inputs in the indicated "Word Not Submitted" fields.
  * [Be Kind](https://www.madtakes.com/libs/188.html)
  * [Romeo and Juliet](https://www.madtakes.com/libs/186.html)
  * [Rush Hour 3](https://www.madtakes.com/libs/183.html)
  * For other templates, [Mad Takes](https://www.madtakes.com/)

## Bonus

* Additional features that can be added:
  * Changing input method:
    * (MODERATE CHALLENGE) Give the user a handful of choices per input to choose from.  You will need to create a bank of verbs, nouns, etc for this.
    * (HARD CHALLENGE) Give the user cards, that consist of options (actions, verbs, nouns, etc) based off the actual game. Allowing them to draw, etc following the rules. Allow them to only input those cards.
  * Implement basic user input checking:
    * Check that the type of the field matches the input
    * Ensure symbols aren't used if they are not needed
    * Implement a method for the user to try again if their input is incorrect
  * Use formatting to create a UI within the terminal. Space out certain UI elements such as title of program, choices, menu decoration, etc. 
  * Use [f-strings](https://docs.python.org/3/reference/lexical_analysis.html#f-strings) instead of .format().

---