
## Operators

### Relational / Comparison Operators

* **x == y**
  * if x equals y, return True
* **x != y**
  * if x is not y, return True
* **x &gt; y**
  * if x is greater than y, return True
* **x &lt; y**
  * if x is less than y, return True
* **x &gt;= y**
  * if x is greater or equal to y, return True
* **x &lt;= y**
  * if x is less or equal to y, return True

**Example:**

```python
a=7
b=4

print('a > b is',a>b)
print('a < b is',a<b)
print('a == b is',a==b)
print('a != b is',a!=b)
print('a >= b is',a>=b)
print('a <= b is',a<=b)


#Output
a > b is True
a < b is False
a == b is False
a != b is True
a >= b is True
a <= b is False
```

## Membership Operators

* **in**
  * Evaluates to True if it finds a variable in the checked sequence
* **not in**
  * Evaluates to True if it does not find a variable in the checked sequence

**Examples:**

```python
test_list = [1, 2, 3, 4, 5]
print(1 in test_list)
print(10 not in test_list)
print(7 in test_list)
print(3 not in test_list)

list1=[1,2,3,4,5]
list2=[6,7,8,9]
for item in list1:
    if item in list2:
        print("overlapping")      
else:
    print("not overlapping")

# Output: not overlapping
```

## Identity Operators

* **is**
  * Evaluates to True if variables on either side of the operator point to the same object 
* **is not**
  * Evaluates to True if the variables on either side of the operator does not point to the same object

**Examples:**

```python
x1 = 5
y1 = 5
x2 = 257
y2 = 257
x3 = 'Hello'
y3 = 'Hello'
x4 = [1,2,3]
y4 = [1,2,3]

# Output: False
print(x1 is not y1)

# Output: False
print(x2 is y2)

# Output: True
print(x3 is y3)

# Output: False
print(x4 is y4)

x = 5
if (type(x) is int):
    print ("true")
else:
    print ("false")

# Output: true
```

## Boolean Operators

* **and**
  * Evaluates True if expressions on both sides of the operator are True. 
* **or**
  * Evaluates True if expressions on either side of the operator are True.

**Examples:**

```python
# Example using bitwise operators
a=7
b=4

# Result: a and b is 4
print('a and b is',a and b)

# Result: a or b is 7
print('a or b is',a or b)

# Result: not a is False
print('not a is',not a)

#Output
# a and b is 4
# a or b is 7
# not a is False
```

## Assignment Operators

| Operator | Example | Similar |
| :---: | :---: | :---: |
| = | x = 8 | x = 8 |
| += | x += 8 | x = x + 8 |
| -= | x -= 8 | x = x - 8 |
| *= | x *= 8 | x = x * 8 |
| /= | x /= 8 | x = x/8 |
| %= | x %= 8 | x = x%8 |
| \**= | x \**= 8 | x = x**8 |
| &= | x &= 8 | x = x & 8 |
| \|= | x \|= 8 | x = x\|8 |
| ^= | x ^= 8 | x = x ^ 8 |
| &lt;&lt;= | x &lt;&lt;= 8 | x = x &lt;&lt; 8 |
| &gt;&gt;= | x &gt;&gt;= 8 | x = x &gt;&gt; 8 |

---
