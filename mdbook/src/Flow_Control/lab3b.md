# Lab 3B

## Instructions

* Using the provided student code, you will need to decrypt the provided encrypted file.
* The file has been encrypted with a simple XOR, using the value 27 as the key
* Remember to think outside the box, what the XOR operation is, what type of data it acts on, etc.
* [XOR Cipher](https://www.geeksforgeeks.org/xor-cipher/) provides further assistance, should you need it 

## Requirements

* Adhere to [PEP8](https://www.python.org/dev/peps/pep-0008/)  (comments, formatting, style, etc)
* Make sure to include your student docstring at the top (name, lab, date)
* Follow instructions above

## Additional Files

* [Student Code](./performance_labs/lab3B/c4115L-TM-lab3b-studentCode.py)
* [Encrypted File](./performance_labs/lab3B/file.txt)

## Bonus

* Some further exercises to help with opening/processing files:
  * Write a series of integers to a file named numbers.txt.  Then read that file, and display the numbers in the file to the console.
  * Write a program that reads the numbers stored in a file, and calculate the sum and average of the numbers.  Assume the file only contains whole numbers.
  * Write a program that asks the user for the name of a file.  Display the contents of the first five lines of the file to the console.  If the file contains less than 5 lines, all of the file's contents should be displayed.
  * For the next 2 exercises, use
  ```python
  import random
  random.randint(min, max)
  ```
    * Build a program that writes a series of random numbers between 1 and 100 (inclusive) to a file.  The program should let the user specify how many random numbers to write to the file.
    * Build another program that reads the random numbers from the file created in the last exercise.  Display the numbers, the sum of the numbers, and the amount of numbers read.
---