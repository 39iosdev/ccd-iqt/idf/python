# Lab 3C

<img src="../assets/funhouse.jpg" width="600">

## Instructions

* Create a text-based game where the user is navigating through a "Fun" House. 
* Prompt the user to make a decision 
  * Using **if/elif/else** statements, give them different outcomes based on their answers. 
  * Begin with an introduction to your fun house, and prompt the user to choose between at least three different options. 

## Requirements

* Adhere to [PEP8](https://www.python.org/dev/peps/pep-0008/)  (comments, formatting, style, etc)
* Make sure to include your student docstring at the top (name, lab, date)
* Follow instructions above

## Bonus Features

* Additional features that you can add:
  * Make use of nested if/elif/else statements to make the game more complex.
  * Create a file that holds all of your prompts
  * Use formatting to create UI elements and/or fancy prompts

**Example:**

```python
print "Welcome to Fun House! Choose door 1, 2, or 3..."

input = raw_input("> ")

if input == "1":
    #code
    print"1"
elif input == "2":
    #code
    print "2"
elif input == "3":
    #code
    print "3"
else:
    print "Go home you're drunk."
```
<br />
<br />
<br />

## Bonus Exercises

* Write a program that prompts the user to enter a number from 1 to 10. Display the Roman numeral version of that number. If the number is
outside the range, the program should display an error message.
* Using the formula weight = mass * 9.81, write a program that asks the user to enter an object’s mass, and then calculates its weight. If the object weighs more than 1,000 newtons, display a message indicating that it is too heavy. If the object weighs less than 10 newtons, display a message indicating that it is too light.
* Build a program that prompts the user to enter the names of two primary colors to mix. If the user enters anything other than "red","blue", or "yellow", the program should display an error message.  Otherwise, the program should display one of the following messages, based on the input:
```text
When you mix red and blue, you get purple.
When you mix red and yellow, you get orange.
When you mix blue and yellow, you get green.
```
---