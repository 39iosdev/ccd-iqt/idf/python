# Lab 3F

## Instructions

* Iterate through a while loop, printing **“Fizz”** for any number divisible by 3 and **“Buzz”** for any number divisible by 5. If the number is divisible by both, print **“FizzBuzz”**. Just print the number for all other numbers.  Once the counter reaches 100, break out of the while loop (not using the loop condition).
* Make sure to use the break and continue keywords.

## Requirements

* Adhere to [PEP8](https://www.python.org/dev/peps/pep-0008/)  (comments, formatting, style, etc)
* Make sure to include your student docstring at the top (name, lab, date)
* Follow instructions above

## Additional Features

* Create two versions:
  * One version which is as short as possible
  * Another version which is as drawn out and creative as possible

**Example Output:**

```text
​1​
2​
Fizz​
4​
Buzz​
Fizz​
7​
…​
14​
FizzBuzz​
…​
```
<br />
<br />
<br />

## Bonus Exercises

* Using the rainfall exercise from the bonus for Lab 3E, add the following modifications: if by the fifth month, the total of rainfall has not reached 25 for year, break from the inner loop, tell the user that year had a low amount of rainfall and set that year's rainfall to 35.
<br />

* Using the temperature exercise from the bonus for Lab 3E, add the following modification: display the table from -100 to 100 Celsius and if the degree amount in Fahrenheit is equal to the degree amount in Celsius, print a message saying that (instead of printing the numbers) and use the continue keyword for the loop.
<br />

* Write a program with a while loop that asks the user to enter a series of numbers, one at a time.  The series can include negative numbers, but those should be ignored (this should use the continue keyword).  Once an exit character has been input ```(|, 'end', or `)```, the loop should break (using the break keyword).  Display the sum of the positive numbers entered.
<br />

* Write a program that uses nested loops to draw this pattern (make use of the continue keyword):
```
##

#  #

#    #

#      #
```
---