
## I/O: Files

Python offers a robust and insanely easy to use toolset to interact with files.

First, a file must be opened before it can be modified... this includes creating new files. We use the **open()** function to achieve this. The first argument passed is the file name, whether it exists or not. The second argument is the operation in which we would like to perform(for example read, write, append).

```python
file = open("file.txt", 'r')
```

## open() Operation Arguments

### read

**r** -- Opens file for read only. File pointer is placed at start of file.

**rb** -- Opens a file for reading only in binary format. File pointer is placed at the start of the file.

**r+** -- Opens a file for both reading and writing. File pointer is placed at the start of the file.

**rb+** -- Opens a file for both reading and writing in binary format. File pointer is placed at the start of the file.

### write

**w** -- Opens a file for writing only. Overwrites the file if it exists. If the file does not exist, it creates a new one. File pointer is placed at start of file.

**wb** -- Opens a file for writing only in binary format. Overwrites the file if it exists. If it does not exist, it creates a new one. File pointer is placed at start of file.

**w+** -- Opens a file for writing and reading. Overwrites the existing file if it exists. If it does not exist, it creates a new one. File pointer is placed at start of file.

**wb+** -- Opens a file for writing and reading in binary format. Overwrites the existing file if it exists. If not, it creates a new one. File pointer is placed at start of file.

### append

**a** -- Opens a file for appending. File pointer is at end of file if it exists. If the file does not exist, it creates a new one.

**ab** -- Opens a file for appending in binary format. File pointer is at end of file if it exists. If not, it creates a new one.

**a+** -- Opens a file for both appending and reading. If file exists, file pointer placed at end of file. If not, it creates a new one.

**ab+** -- Opens a file for both appending and reading in binary format. Follows above file pointer and write rules.

## Advantages of Using Binary Formatting

The advantages of using binary formatting primarily apply to Windows. Unlike Linux, where "everything is a file", Windows treats binaries and files differently. Thus, reading binary in text mode in Windows will more than likely result in corrupted data. Passing a 'b' variant will mitigate this issue.

## File Operations

Once the file is open, we can begin reading, adding or modifying the file's contents. Below are some of the methods to make that happen.

* **f.write(str)**
  * Writes the content of the string to the the file, and returns the number of characters written.
* **f.writelines(lines)**
  * Writes the content of the list of lines to the file.  These will not have line separators added.
* **f.read(sz)**
  * Read sz amount of bytes from the file and returns them.
* **f.readline()**
  * Reads characters until either a newline character or the end of file has been reached and returns that result as a single string
* **f.seek(offset)**
  * Changes the file pointer to the given offset
* **f.tell()**
  * Returns the file pointer's current position
* **f.truncate(sz)**
  * Resizes the file to sz bytes.  If the file is to grow in size, additional bytes are zero-filled.
* **f.mode()**
  * Returns the access mode used to open a file
* **f.name()**
  * Returns the filename that was provided to the file pointer
* **f.close()**
  * Close file pointer used
  * Just like other programming languages, we need to close the stream
  * **ALWAYS CLOSE THE FILE AS SOON AS YOU'RE FINISHED USING IT!!**

```python
f = open('file_name', 'a')
data = f.read()
print(data)

file.close()
``` 

To assist a developer to remember to close a file pointer after use, we can make use of the **with** keyword statement.  If we use the **with** keyword, once the code block is complete, the file will be properly closed, even if an exception occurred.  It is recommended that if you interact with files, make use of the **with** keyword!

```python
with open('file_name', 'r') as f:
    data = f.read()
print(data)
```
---

**Continue to Performance Lab:** 3B  
