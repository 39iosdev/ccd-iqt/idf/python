# Lab 3E

## Instructions

* Write a file that prints out the first 100 numbers in the Fibonacci sequence iteratively
* Reference for further information on [Fibonacci](https://en.wikipedia.org/wiki/Fibonacci) 

<img src="../assets/13import.png" width="300">
<br />
<img src="../assets/14import.png" width="350">

## Requirements

* Adhere to [PEP8](https://www.python.org/dev/peps/pep-0008/)  (comments, formatting, style, etc)
* Make sure to include your student docstring at the top (name, lab, date)
* Follow instructions above

## Additional Features

* Ask user for range of numbers to calculate
* Create the recursive version once you learn the recursion topic in the Functions module

<br />
<br />

## Bonus Exercises

* Write a program that uses nested loops to collect data and calculate the average rainfall over a period of years. The program should first ask for the number of years. The outer loop will iterate once for each year. The inner loop will iterate twelve times, once for each month. Each iteration of the inner loop will ask the user for the inches of rainfall for that month. After all iterations, the program should display the number of months, the total inches of rainfall, and the average rainfall per month for the entire period.
<br />

* Write a program that displays a table of the Celsius temperatures 0 through 20 and their Fahrenheit equivalents. The formula for converting a temperature from Celsius to Fahrenheit is  
F = (9/5)C + 32, where F is the Fahrenheit temperature and C is the Celsius temperature. Your program must use a loop to display the table.
<br />

* Write a program that calculates the amount of money a person would earn over a period of time if his or her salary is one penny the first day, two pennies the second day, and continues to double each day. The program should ask the user for the number of days. Display a table showing what the salary was for each day, and then show the total pay at the end of the period. The output should be displayed in a dollar amount, not the number of pennies.
<br />

* Write a program with a loop that asks the user to enter a series of positive numbers. The user should enter a negative number to signal the end of the series. After all the positive numbers have been entered, the program should display their sum.
<br />

* Write a program that uses nested loops to draw this pattern:
```
*******
******
*****
****
***
**
*
```

* Write a program that uses nested loops to draw this pattern:
```
##
# #
#  #
#   #
#    #
#     #
```
---