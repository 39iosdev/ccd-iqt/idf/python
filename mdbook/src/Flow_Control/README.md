## Flow Control

### **Introduction:**
This lesson will walk you through the flow control, which is the order your python scripts operate. Python uses typical programming flow control statements. 

### **Topics Covered:**

* [**Operators**](operators.html#operators)
* [**I/O Print**](io_print.html#io-print)
* [**I/O Files**](io_files.html#io-files)
* [**If, Elif, Else**](if_elif_else.html#if-elif-else)
* [**While Loops**](while_loops.html#while-loops)
* [**For Loops**](for_loops.html#for-loops)
* [**Break and Continue**](break_continue.html#break-and-continue)


#### To access the Flow Control slides please click [here](slides)

