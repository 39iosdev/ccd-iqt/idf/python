
## I/O Print

Python **print()** is very powerful... taking styling from C string formatting and adding some of it's own features; allowing for deeper functionality. Pull up PyDocs and reference Py3 print(), %, and formatting.

**Note:** Python print() function automatically creates a newline. We can get around this if needed.

**Python 3 Basic:**

```python
print("hello")
print(1+1)
print('Hello' + 'World')
print('Hello' , 'World')

# Printing without newline
print("First statement ", end=" ")
print("{}".format("Second statement"), end="")
print("Third statement")
# Output: 'First statement  Second statementThird statement'
```

## % Formatting

**%** formatting follows C style and is being phased out. With short strings, the code still looks very readable.  However, once multiple variables are being formatted, the code starts to look very unwieldy. It is highly recommended to use **.format()**, but have practice with both!

```python
​# Basic Positional Formatting
>>> print("%s World!" % "Hello")
'Hello World!'

# Basic Positional Formatting
>>> print("%s %s!" % ("Hello", "World"))
'Hello World!'

# Padding 10 left
>>> print('%10s' % ('test',))
'      test'

# Negative Padding (10 right)
>>> print('%-10s %s' % ('test', 'next word'))
'test       next word'

# Truncating Long Strings
>>> print('%.5s' % ('what in the world',))
'what '

# Truncating and padding
>>> print('%10.5s' % ('what in the world',))
'     what'

# Numbers
>>> print('%d' % (50,))
50

# Floats
>>> print('%f' % (3.123513423532432))
3.123513

# Padding numbers
>>> print('%4d' % (50,))
  50

# Padding Floating Points/precision
>>> print('%06.2f' % (3.141592653589793,))
003.14

# Name Placeholders
>>> things = {'car': 'BMW E30', 'motorcycle': 'Harley FXDX'}
>>> print('My fav car %(car)s and motorcycle %(motorcycle)s' % things)
'My fav car BMW E30 and motorcycle Harley FXDX'
```

## .format()

Format() is a newer formatting functionality. PEP8 highly encourages the use of .format() whenever possible. .format() includes most of the previous functionality with a ton of added features as well. Below are just SOME of the built in .format() components. Keep in mind, you can create custom functionality with .format(). Take a look at the docs and below and experiment!

```python
​# Basic Positional Formatting
>>> print('{} {}!'.format('Hello', 'World'))
'Hello World!'

# Basic Positional Formatting
>>> print("{!s} {!s}".format("Hello", "World"))
'Hello World!'

# Actual Positional Formatting
# EXCLUSIVE
>>> print('{1} {0}!'.format('World', 'Hello'))
'Hello World!'

# Padding 10 left
>>> print('{:>10}'.format('test'))
'      test'

# Negative Padding 10 right
>>> print('{:<10}'.format('test'))
'test      '

# Changing Padding Character
# EXCLUSIVE
>>> print('{:_>10}'.format('test'))
'______test'

# Center Align
# EXCLUSIVE
>>> print('{:_^10}'.format('test'))
'___test___'

# Truncating Long Strings
>>> print('{:.5}'.format('what in the world'))
'what '

# Truncating and padding
>>> print('{:10.5}'.format('what in the world'))
'     what'

# Numbers
>>> print('{:d}'.format(50))
50

# Floats
>>> print('{:f}'.format(3.123513423532432))
3.123513

# Padding numbers
>>> print('{:4d}'.format(50))
  50

# Padding Floating Points/precision
>>> print('{:06.2f}'.format(3.141592653589793))
003.14

# Name Placeholders
>>> things = {'car': 'BMW E30', 'motorcycle': 'Harley FXDX'}
>>> print('My favorite car is a {car} and motorcycle {motorcycle}'.format(**things))
'My fav car BMW E30 and motorcycle Harley FXDX'


# Date and Time
# EXCLUSIVE
>>> from datetime import datetime
>>> print('{:%Y-%m-%d %H:%M}'.format(datetime(2017, 10, 17, 10, 45)))
2017-10-17 10:45


#Variables
>>> nBalloons = 8
>>> print("Sammy has {} balloons today!".format(nBalloons))

Output
Sammy has 8 balloons today!


>>> sammy = "Sammy has {} balloons today!"
>>> nBalloons = 8
>>> print(sammy.format(nBalloons))

Output
Sammy has 8 balloons today!
```

| Format Symbol | Conversion |
| :---: | :---: |
| %c | character |
| %s | string conversion via str() prior to formatting |
| %i | signed decimal integer |
| %d | signed decimal integer |
| %u | unsigned decimal integer |
| %o | octal integer |
| %x | hexadecimal integer (lowercase letters) |
| %e | exponent notation |
| %f | floating point real number |  

---

## f-strings

As of Python 3.6, there is now an additional method to format string output.  Called "formatted string literals", f-strings allow formatting to occur on variables in the place of the string where they should reside, instead of separating the string and the variables.

## Example:

```python
>>> name = Bob
>>> age = 25
>>> f"Hello, {name}. You are {age}."
'Hello, Bob. You are 25.'
```

f-strings start with the letter f or F, and allow for arbitrary expressions to exist between the curly braces.  So, not just variables, but mathematical expressions, functions, any sort of Python code can be executed within the curly brace.

```python
f"{name.lower()} is funny."
f"{2 * 37}"
```

To modify the code presented within the curly braces, just as you would with % formatting or .format(), use a : after the code to indicate whatever formatting options are needed.  Because you might have code within the curly braces that needs to use escape characters, you might need to use additionally curly braces to appropriately escape. For more references on f-strings, see [Formatted String Literals](https://docs.python.org/3/tutorial/inputoutput.html#formatted-string-literals)

**Continue to Performance Lab:** 3A  
